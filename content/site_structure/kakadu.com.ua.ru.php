<?php

/* 
 * Главная страница
 */

$Site->afterContent = true;

$query = "
        select 
                tb_parthner.id,
                tb_parthner.name_".LANGUAGE_CURRENT." as name,                 
                tb_parthner.link,
                tb_parthner.image,
                tb_parthner.is_organizator
        from site_parthner tb_parthner            
        where tb_parthner.active = 1
        order by tb_parthner.priority
";
$data = $DB->query($query);

$prth = $org = array();
foreach($data as $row) {
    $row['image'] = Uploads::getIsFile('site_parthner', 'image', $row['id'], $row['image']);
    if ($row['is_organizator'] == 1){
        $org[] = $row;
    } else {
        $prth[] = $row;
    }
}

if (count($org)){
    $TmplDesign->set('show_prth_org', true);
    $TmplDesign->iterateArray('/organizator/', null, $org);
}
if (count($prth)){
    $TmplDesign->set('show_prth', true);
    $TmplDesign->iterateArray('/parthner/', null, $prth);
}