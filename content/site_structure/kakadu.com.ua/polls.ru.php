<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!Auth::isAdmin()) {
    if (!AwardsSeason::isPoll()){
        header("Location: /");
        exit();
    }
}

$user_id = Auth::getUserId();

$work_id = $Site->item_object_id;

$Awards = new Awards();
$count_works = $Awards->getCountUserWorks($user_id);

$TmplContent->setGlobal('user_id', $user_id);
        
if (empty($count_works) && !Auth::isAdmin()) {
    $TmplContent->set('no_work', true);
} else {
    if (!empty($work_id)){
        $work = $Awards->getWork($work_id, false);
        if (empty($work)){
            $Site->cross404();
            exit;
        }

        if (!empty($work['file_video'])){
            $code = explode('?v=', $work['file_video']);
            if (!isset($code[1])){
                $backs = explode('/', $work['file_video']);
                $code = array_pop($backs);
            } else {
                $code = explode('&',$code[1]);
                $code = $code[0];
            }
            $work['file_video'] = $code;// Uploads::fancyVideo('Посмотреть', $code);
        }

        if (!empty($work['file_link']) && !preg_match('/^http/', $work['file_link'])) {
            $work['file_link'] = 'http://' . $work['file_link'];        
        }
        if (!empty($work['file_link'])) {
            $work['file_link_txt'] = $work['file_link'];           
        }
        
        $user_id = Auth::getUserId();
        $work['already_poll'] = $DB->result("select IF(poll = 1, 'Нравится', 'Не нравится') from kakadu_works_poll where work_id='$work_id' and user_id='$user_id' ");
        $TmplContent->set('work', $work);

        $next = $Awards->getNearbyWorks($work['id'], false);
        $TmplContent->set('next', $next);

        $prev = $Awards->getNearbyWorks($work['id'], true);
        $TmplContent->set('prev', $prev);

    } else {
        $works = $Awards->getWorksForPoll();
        $TmplContent->iterateArray('/works/', null, $works);
    }
}