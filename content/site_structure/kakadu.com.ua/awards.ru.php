<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$season_id = AwardsSeason::getID();
$awards = $DB->query("select *, name_".LANGUAGE_CURRENT." AS name, titles_".LANGUAGE_CURRENT." as title "
        . " FROM `kakadu_awards` WHERE active = 1 AND season_id='$season_id' ORDER BY priority");

foreach($awards as $i=>$row){
    $awards[$i]['i'] = $i+1;
}
$TmplContent->iterateArray('/awards/', null, $awards);
$TmplContent->set('show_awards', true);