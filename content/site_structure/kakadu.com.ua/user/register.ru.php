<?php

/* 
 * регистрации
 */

if (Auth::isLoggedIn() || !AwardsSeason::isTake()) {
    header("Location:/");
    exit;
}

 if (isset($_SESSION['ActionError']) && isset($_SESSION['ActionReturn']['error'])){
     while( list(, $error) = each($_SESSION['ActionReturn']['error']) ){        
        $TmplContent->iterate('/error/', null, array('name'=> $error) );
     }
}

$TmplDesign->set('show_tabmenu', true);
$TmplDesign->set('tab_class', 'regist');
$TmplDesign->set('page_class', 'regist-page');
$TmplDesign->iterateArray('/tabmenu/', null, array(
    array('url'=>'#tab1', 'name'=>'Организация', 'class'=>'active'),
    array('url'=>'#tab2', 'name'=>'Частное лицо'),
));

$TmplContent->set('captcha_html', Captcha::createHtml());
$TmplContent->set('show_soc_button', 	USER_ENTER_SOC);
        
