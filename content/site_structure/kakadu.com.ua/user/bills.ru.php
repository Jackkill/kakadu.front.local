<?php

/*  
 * Счета
 */

if (!AwardsSeason::isLogining() ){
    header("Location: /".LANGUAGE_URL);
    exit;
}

$Awards = new Awards();

$bills = $Awards->getUserBills();
foreach($bills as $i=>$row) {
    
    $works = $DB->fetch_column("select tb_work.`name` from kakadu_works tb_work "
                . " inner join kakadu_bills_works as tb_bill on tb_bill.work_id=tb_work.id "
                . " where tb_bill.bill_id='{$row['id']}'");
    $bills[$i]['works'] = implode(",<br/>", $works);            
}
$TmplContent->iterateArray('/bills/', null, $bills);
$TmplContent->set('bills_count', count($bills));

$works = $Awards->getUserQueueWorks();

$TmplContent->iterateArray('/works/', null, $works);
$TmplContent->set('work_need_bills', count($works));

$сworks = $Awards->getUserWorks();
//if ( AwardsSeason::isTake() ){
    $TmplContent->set('count_works', count($сworks));
//}