<?php

/* 
 * Измениние Персональные данные
 * @package Pilot
 * @subpackage Auth
 * @author Naumenko A.
 * @copyright c-format, 2014
 */


if (!AwardsSeason::isLogining() ){
    header("Location: /".LANGUAGE_URL);
    exit;
}

$user = Auth::getInfo();
$user += Auth::getDataInfo($user['id']);    
$TmplContent->set('user', $user);

if(isset($_SESSION['ActionReturn']['success']) ){
    header("Location: /".LANGUAGE_URL."user/info/");
//    foreach($_SESSION['ActionReturn']['success'] as $error){
//        $TmplContent->set('success', $error );
//    }
}