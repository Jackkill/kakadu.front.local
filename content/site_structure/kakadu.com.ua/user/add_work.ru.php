<?php

/** 
 * Добавление работы
 */

if (!AwardsSeason::isTake() ){
    header("Location: /".LANGUAGE_URL);
    exit;
}

if (!Auth::isLoggedIn()){
    header("Location: /".LANGUAGE_URL ."user/register/");
    exit;
}

$group_id = globalVar($_GET['group_id'], 0);
$work_id = globalVar($_GET['work_id'], 0);


$Awards = new Awards();

$work = array();

if ($work_id) {
    //Редактирование работи
    $work = $Awards->getWork($work_id, true);
    if ($work) {
        $TmplDesign->set('headline', 'Редактирование работы');
        $TmplContent->set('work', $work);
        if (!empty($work['commands'])){
            $TmplContent->iterateArray('/commands/', null, $work['commands']);
        }
        
        $group_id = $work['group_id'];
    }
}

//урл Категории по умолчанию
$url_categories = Site::getPageUrl( 75903 );

//info пользователя
$user = Auth::getInfo();
$TmplContent->set('user', $user);

//категории
$categories = $Awards->categories();

if (isset($categories[$group_id])) {
    $categories[$group_id]['selected'] = true; 
}
$TmplContent->iterateArray('/categories/', null, $categories);

//Файли для загрузки
if (empty($group_id)){
    reset($categories);
    list($group_id, $row) = each($categories);
}

$files = $Awards->getFileToDownload($group_id, $work);

$Template = new Template('kakadu/file_to_download');
$Template->iterateArray('/files/', null, $files);
$Template->setGlobal('url_categories', $url_categories);
$TmplContent->set('file_to_download', $Template->display());

//года
$year = date("Y");
$sel_year = (isset($work['years_konkurs']) && !empty($work['years_konkurs'])) ? $work['years_konkurs'] : '';
$TmplContent->set('years', TemplateUDF::html_options(array('options' => array(($year-1)=>($year-1), $year=>$year), 'selected'=>array($sel_year))));
