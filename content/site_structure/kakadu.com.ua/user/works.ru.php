<?php

/* 
 * Список всех работ пользователя
 */


if (!AwardsSeason::isLogining() ){
    header("Location: /".LANGUAGE_URL);
    exit;
}

if(isset($_SESSION['ActionReturn']['error']) ){
    foreach($_SESSION['ActionReturn']['error'] as $error){
        $TmplContent->iterate('/error/', null, array('name'=>$error) );
    }
}
 
$TmplContent->setGlobal('end_taking', false);
 
$Awards = new Awards();
$works = $Awards->getUserWorks();

if (!AwardsSeason::isTake() ){
    $TmplContent->setGlobal('end_taking', true);
}

$TmplContent->iterateArray('/works/', null, $works);
$TmplContent->set('count_works', count($works));


$TmplContent->setGlobal('text_confirm', cmsMessage::get('SITE', 'MSG_SITE_WORK_DELETED'));