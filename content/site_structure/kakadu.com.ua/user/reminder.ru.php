<?php

/* 
 * Напоминание пароля
 */


if (!AwardsSeason::isLogining() ){
    header("Location: /".LANGUAGE_URL);
    exit;
}

if (Auth::isLoggedIn()) {  
    header("Location: /".LANGUAGE_URL);
    exit;
}
    
    if(isset($_SESSION['ActionReturn']['success']) ){
    foreach($_SESSION['ActionReturn']['success'] as $error){
        $TmplContent->set('success', $error );
    }
    }
    if(isset($_SESSION['ActionReturn']['error']) ){
        foreach($_SESSION['ActionReturn']['error'] as $error){
            $TmplContent->iterate('/error/', null, array('name'=>$error) );
        }
    }
   
    if (isset($_SESSION['ActionError']['email'])) {
	$TmplContent->set('email', $_SESSION['ActionError']['email']);
    }

    $TmplContent->set('captcha_html', Captcha::createHtml());