<?php
/**
 * Подтверждение пароля
 */

$code = globalVar($_GET['code'], '');
$complete = globalVar($_GET['complete'], '');

if (Auth::isLoggedIn()){
    header("Location: /".LANGUAGE_URL."user/works/");
    exit;
}

if(isset($_SESSION['ActionReturn']['error']) ){
        $Site->showContent = false;
        foreach($_SESSION['ActionReturn']['error'] as $error){
            $TmplContent->iterate('/error/', null, array('name'=>$error) );
        }
    }
else {  
    
    if (!empty($complete)){
        $TmplContent->set('show_form', true);
    } else {
        header("Location: /".LANGUAGE_URL."action/user/confirm/?code=$code&_error_path=/".LANGUAGE_URL."user/confirm/?code=$code&_return_path=/".LANGUAGE_URL."user/confirm/?complete=is");
        exit;
    }
}
