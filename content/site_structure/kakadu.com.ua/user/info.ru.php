<?php

/*
 * Кабинет пользователя
 */
header("Location: /".LANGUAGE_URL.'user/works/');
    exit;
if (!Auth::isLoggedIn()) { exit; }


if (!AwardsSeason::isLogining() ){
    header("Location: /".LANGUAGE_URL);
    exit;
}

$user = Auth::getInfo();
$user += Auth::getDataInfo($user['id']);

$TmplContent->set('user', $user);