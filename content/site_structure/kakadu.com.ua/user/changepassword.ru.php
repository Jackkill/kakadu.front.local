<?php

/* 
 * Изменение пароля
 */


if (!AwardsSeason::isLogining() ){
    header("Location: /".LANGUAGE_URL);
    exit;
}

if(isset($_SESSION['ActionReturn']['error']) ){
    foreach($_SESSION['ActionReturn']['error'] as $error){
        $TmplContent->iterate('/error/', null, array('name'=>$error) );
    }
}

if(isset($_SESSION['ActionReturn']['success']) ){
    foreach($_SESSION['ActionReturn']['success'] as $error){
        $TmplContent->set('success', $error );
    }
}
