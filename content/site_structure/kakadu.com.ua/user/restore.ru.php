<?php
/** 
 * Восстановление пароля пользователя
 * @package Pilot 
 * @subpackage User 
 * @author Eugen Golubenko <eugen@delta-x.com.ua> 
 * @copyright Delta-X, ltd. 2008
 */ 
if (Auth::isLoggedIn()) {  
    header("Location: /".LANGUAGE_URL);
    exit;
}

    
$auth_code = globalVar($_GET['auth_code'], '');
$user_id = globalVar($_GET['user_id'], '');

if(isset($_SESSION['ActionReturn']['error']) ){
        foreach($_SESSION['ActionReturn']['error'] as $error){
            $TmplContent->iterate('/error/', null, array('name'=>$error) );
        }
    }
if(isset($_SESSION['ActionReturn']['success']) ){
    foreach($_SESSION['ActionReturn']['success'] as $error){
        $TmplContent->set('success', $error );
    }
}
    
$TmplContent->set('auth_code', $auth_code);
$TmplContent->set('user_id', $user_id);

$TmplContent->set('captcha_html', Captcha::createHtml());

?>