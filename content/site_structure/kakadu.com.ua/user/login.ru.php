<?php

/**
 * Страница авторизации пользователей
 */

if (!AwardsSeason::isLogining() ){
    header("Location: /".LANGUAGE_URL);
    exit;
}

/**
 * Пользователь авторизирован
 */
if (Auth::isLoggedIn()) {
    header("Location: /".LANGUAGE_URL."user/");
    exit;
}


/**
 * Форма авторизации
 */
$form_login = Auth::displayLoginForm(false, false );
$TmplContent->set("form_login", $form_login );

//print_r($_SESSION['ActionReturn']['error']);
if(isset($_SESSION['ActionError'])){ 
	reset($_SESSION['ActionReturn']['error']);
	while(list(, $message) = each($_SESSION['ActionReturn']['error'])){
		$row['message'] = $message;
		$TmplContent->iterate("error", null, $row);
	}
	
}

?>