<?php

/* 
 * Страница категорий / номинаций
 */



$user_id = Auth::getUserId();

$isTake = AwardsSeason::isTake();

$Awards = new Awards();
$categories = $Awards->categories();

$index=1;
foreach($categories as $i=>$row){
    $categories[$i]['link'] = ($isTake) ? ( ($user_id>0 ) ? '/'.LANGUAGE_URL.'user/add_work/?group_id='.$row['id'] : '/'.LANGUAGE_URL.'user/register/' ) : '#';
    $categories[$i]['devider'] = ($index % 3 == 0) ? true : false;
    $categories[$i]['description'] = nl2br($row['description']);
    $categories[$i]['icon'] = Uploads::getIsFile('kakadu_group', 'icon', $row['id'], $row['icon']);
    $index++;
}
$TmplContent->iterateArray('/categories/', null, $categories);