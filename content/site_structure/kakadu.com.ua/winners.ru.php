<?php

/* 
 * Победители прошлых лет
 * и нового сезона, если идет награждение
 */

$season = globalVar($_REQUEST['season'], '');
$season_id = 0;

if ( empty($season) ) {
     
    //обновление сезона и статуса
    $Season = new AwardsSeason(true);
    
    $current_season_id = AwardsSeason::getID();
    
    if (IS_DEVELOPER || AwardsSeason::isAwards() ){
        //по умолчание текущие победители
        $season_id = $current_season_id;
        
    } elseif (date("Y") > 2015 ) {
        $season_id = $DB->result("SELECT MAX(tb_season.id) "
                . " FROM `kakadu_season` as tb_season "
                . " INNER JOIN `kakadu_works` AS tb_work ON tb_work.season_id = tb_season.id "
                . " WHERE tb_season.status = 'awards' and tb_work.award_id > 0 and tb_work.is_paid ");
    } 
} else {
        $season_id = $DB->result("SELECT tb_season.id "
                . " FROM `kakadu_season` as tb_season "
                . " INNER JOIN `kakadu_works` AS tb_work ON tb_work.season_id = tb_season.id "
                . " WHERE tb_season.uniq_name = '{$season}' and (tb_season.status='none' or tb_season.status='awards') ");
        
}

$TmplDesign->set('show_tabmenu', true);
$TmplDesign->set('tab_class', 'tabs');
$TmplDesign->set('page_class', 'winner-page');
$TmplDesign->set('headline', '');
    

if (!empty($season_id)) {
    $data = $DB->query("SELECT "
                . " tb_work.id, tb_work.name as product, tb_work.participant as author, tb_work.alias as work_alias, tb_group.alias,"
                . " tb_group.name_".LANGUAGE_CURRENT." as category, tb_group.name_".LANGUAGE_CURRENT." as name,"
                . " tb_award.name_".LANGUAGE_CURRENT." as win_type, tb_group.id as category_new_id, "
                . " tb_work.file_video as video, tb_work.file_image as image, tb_work.file_audio as audio, tb_work.file_link as link "
            . " FROM `kakadu_works` as tb_work "
            . " INNER JOIN `kakadu_awards` as tb_award ON tb_award.id=tb_work.award_id"
            . " INNER JOIN `kakadu_group` as tb_group ON tb_group.id=tb_work.group_id"
            . " WHERE tb_work.`season_id`='$season_id' and tb_work.active='1' and tb_work.for_poll='1' "
            . " ORDER BY tb_group.priority, tb_award.priority, tb_work.id");
    if ( empty($data) ) {
        //$Site->cross404();
    }
    $group_id = 0; $i = 0;
    
    foreach( $data as $work ){
        if ($group_id != $work['category_new_id']) {
            $work['tab'] = 'tab' . $i;
            $fp = $TmplContent->iterate('/category/', null, $work);
            
            $work['url'] = '#' . $work['tab'];
            $TmplDesign->iterate('/tabmenu/', null, $work);
    
            $i++;
            $group_id = $work['category_new_id'];
        }
        $work['name'] = $work['product'];
        
        if (!empty($work['link']) && !preg_match('/^http/', $work['link'])) {
            $work['link'] = 'http://' . $work['link'];        
        }
        
        $fp2 = $TmplContent->iterate('/category/works/', $fp, $work);
        
        //foreach($videos as $field) {
            if (!empty($work['video'])){
                $code = explode('?v=', $work['video']);
                if (!isset($code[1])){
                    $backs = explode('/', $work['video']);
                    $code = array_pop($backs);
                } else {
                    $code = explode('&',$code[1]);
                    $code = $code[0];
                }
                
                $TmplContent->iterate('/category/works/videos/', $fp2, array('code'=>$code));
            }
        //}
        //foreach($images as $field => $val) {
            if (!empty($work['image'])){
                $file = Uploads::getIsFile('kakadu_works', 'file_image', $work['id'], $work['image']);
                if ( !empty($file) ) {
                    $TmplContent->iterate('/category/works/images/', $fp2, array('file' => $file));
                }
            }
            
            if (!empty($work['image'])){
                $file = Uploads::getIsFile('kakadu_works', 'file_audio', $work['id'], $work['audio']);
                if ( !empty($file) ) {
                    $TmplContent->iterate('/category/works/audios/', $fp2, array('file' => $file));
                }
            }
        //}
        
        
         
    }
    
    $season = $DB->result("SELECT season FROM `kakadu_season` WHERE id='$season_id' ");
} else {
    if (empty($season)) {
        $season = '2015';
    }
    
    $data = $DB->query("SELECT * FROM `kakadu_winners_old` WHERE `year`='$season' and active='1' ORDER BY category_new_id, priority");
    if ( empty($data) ) {
        $Site->cross404();
    }
    
    $groups = $DB->query("SELECT category_new_id as id, category as name, alias "
            . " FROM `kakadu_winners_old` "
            . " WHERE `year`='$season' and active='1' GROUP BY category_new_id ORDER BY category_new_id");
    
   
    foreach($groups as $i=>$row ){
        $row['url'] = '#tab' . $i;
        $TmplDesign->iterate('/tabmenu/', null, $row);
    }
    
    $videos = array('video', 'video1');
    $images = array('image' => 'work', 'image1' => 'work1', 'image2' => 'work2', 'image3' => 'work3');
    
    $group_id = -1; $i = 0;
    foreach( $data as $work ){
        if ($group_id != $work['category_new_id']) {
            $work['tab'] = 'tab' . $i;
            $fp = $TmplContent->iterate('/category/', null, $work);
            $i++;
            $group_id = $work['category_new_id'];
        }
        $fp2 = $TmplContent->iterate('/category/works/', $fp, $work);
        
        foreach($videos as $field) {
            if (!empty($work[$field])){
                $v = explode('?v=', $work[$field]);
                if (isset($v[1])) $TmplContent->iterate('/category/works/videos/', $fp2, array('code'=>$v[1]));
            }
        }
        foreach($images as $field => $val) {
            if (!empty($work[$field])){
                $extension = Uploads::getFileExtension($work[$field]);
                $file = 'uploads/assets_images/winner/'.$work['id'].'/'.$val.'_originals.'.$extension;
               
                if ( file_exists(SITE_ROOT . $file) ) {
                    if ($extension == 'mp3') {
                        $TmplContent->iterate('/category/works/audios/', $fp2, array('file'=>'/' . $file));
                    } else {
                        $TmplContent->iterate('/category/works/images/', $fp2, array('file'=>'/' . $file));
                    }
                } else {x($file);}
            }
        }
    }
}

/** можна засунути в кеш */
$season_new = $DB->query("select tb_season.uniq_name, tb_season.season as year "
                    . " FROM `kakadu_season` tb_season "
                    . " INNER JOIN `kakadu_works` AS tb_work ON tb_work.season_id = tb_season.id "
                    . " WHERE tb_work.award_id > 0 AND (tb_season.active='0' OR tb_season.status = 'awards') "
                    . " GROUP BY tb_season.id ORDER BY tb_season.`id` DESC");

$season_old = $DB->query("select DISTINCT(`year`) from `kakadu_winners_old` WHERE active='1' ORDER BY `year` DESC");

$season_menu = array_merge($season_new, $season_old);
//x($season_menu);

if (count($season_menu)){
    $TmplDesign->set('show_leftside', true);
    $season_menu[0]['first'] = true;
    //x($season_menu);
    $TmplDesign->iterateArray('/leftMenu/', null, $season_menu);
    $TmplDesign->setGlobal('url_year', $season);
}