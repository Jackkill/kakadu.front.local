<?php
/**
 * Награды
 * 
 * @package DeltaCMS 
 * @subpackage Kakadu
 * @author Naumenko A.
 * @copyright c-format, 2016
 */


    $season_id = globalVar($_REQUEST['season_id'], SEASON_ID);
    
    $Awards = new Awards();
    $season_row = $Awards->getInfo($season_id);
    
    function cms_filter($row) {
        return $row;
    }
    $query = "
            select 
                tb_group.id,          
                tb_group.name_".LANGUAGE_CURRENT." as name,
                tb_group.titles_".LANGUAGE_CURRENT." as titles,
                tb_group.point,
                tb_group.active,
                tb_group.priority
            from kakadu_awards as tb_group  
            where tb_group.season_id='$season_id'
            order by tb_group.priority
    ";
    $cmsTable = new cmsShowView($DB, $query);    
    $cmsTable->setParam('prefilter', 'cms_filter');
    $cmsTable->setParam('delete', false);
    
    if (empty($season_row) || $season_row['active'] == 0) {
        $cmsTable->setParam('add', false);
        $cmsTable->setParam('edit', false);
    }
    $cmsTable->addColumn('name', '20%', 'left');    
    $cmsTable->addColumn('point', '20%', 'center');    
    $cmsTable->addColumn('titles', '20%', 'center');
    $cmsTable->addColumn('active', '10%', 'center');
    $cmsTable->setColumnParam('active', 'editable', true);
    
    if (!SEASON_ID){
        echo '<div class="message_align"><table border="0" cellpadding="0" cellspacing="0">';
        echo '<tbody><tr><td>Для добавления новых категорий создайте <b>Новый сезон</b></td></tr></tbody>';
        echo '</table></div>';
    }
    
    $cms_table = $cmsTable->display();
    echo Awards::admin_season_menu($season_id, 'award/', $cms_table);
    
    unset($cmsTable);