<?php

/* 
 * Работи учасника
 * @package DeltaCMS
 * @subpackage kakadu
 * @copyright c-format, 2016
 */

    $user_id = globalVar($_REQUEST['user_id'], 0);
   
    //Данные пользователя
    $user = $DB->query_row("select * from auth_user where id = '$user_id'");
    
    if (empty($user)){
        header("Location: /admin/kakadu/users/");
        exit();
    }
    
    $TmplContent->set('user', $user);
    
    $user_data = $DB->query_row("select * from auth_user_data where user_id = '$user_id'");
    if ($user_data){
        $image_file = Uploads::getIsFile('auth_user_data', 'svid_file', $user_data['id'], $user_data['svid_file']);
        $user_data['svid_file'] = Uploads::targetImage($image_file, 'Свидетельство', 200);
    
        $TmplContent->set('data', $user_data);
    }
    
    //Работы пользователя
    $Awards = new Awards();
    $season_row = $Awards->getInfo($season_id);
    
     $status = cmsTable::getEnumFields('kakadu_works', 'status');
     
    function cms_filter($row) {
        global $status, $DB;
        $row['user_id'] -=  intval(USER_DIFF_POINTS);
        $row['name'] = "<a href='/admin/kakadu/works/?id=".$row['id']."'>" . $row['name'] . '</a>';
        $row['status'] = "<span ". (($row['status'] == 'new') ? " style='color:green' " : " style='color:grey' ") . "> " . $status[$row['status']] . "</span>" ;
        if ($row['is_paid'] == 1){
            $row['status_paid'] = "<span style='color:green'>Оплачено</span>";
        } else {
            $is_wait = $DB->result("select bill_id from kakadu_bills_works where work_id = '{$row['id']}'");
            $row['status_paid'] = ($is_wait) ? "<span style='color:blue'>Ожидает оплаты</span>" : "<span style='color:red'>Не запрошен счет</span>";
        }
        return $row;
    }
    $query = "
            select 
                tb_work.*,
                DATE_FORMAT(tb_work.create_dtime, '%d.%m.%Y') as date,
                tb_user.name as company,
                tb_work.alias,
                tb_work.is_paid,
                tb_group.name_".LANGUAGE_CURRENT." as category,
                tb_award.name_".LANGUAGE_CURRENT." as award
            from kakadu_works as tb_work 
            left join auth_user as tb_user on tb_user.id=tb_work.user_id
            left join kakadu_group as tb_group on tb_group.id=tb_work.group_id
            left join kakadu_awards as tb_award on tb_award.id=tb_work.award_id
            where tb_work.season_id='$season_id' ".where_clause('tb_work.user_id', $user_id)."
            order by tb_work.create_dtime desc
    ";
    $cmsTable = new cmsShowView($DB, $query);    
    $cmsTable->setParam('prefilter', 'cms_filter');
    $cmsTable->setParam('show_parent_link', true);
    $cmsTable->setParam('parent_link', '/admin/kakadu/users/?');
    $cmsTable->setParam('title', 'Работы участника ' .$user['name'] );
    $cmsTable->setParam('delete', false);
    $cmsTable->setParam('add', false);
    
    if (empty($season_row) || $season_row['active'] == 0) {
        $cmsTable->setParam('edit', false);
    }
    //$cmsTable->setParam('parent_link', '/admin/site/infoblock/?');      
    $cmsTable->addColumn('user_id', '10%', 'center', 'User ID'); 
    $cmsTable->addColumn('id', '10%', 'center', 'Work ID'); 
    $cmsTable->addColumn('date', '10%', 'left', 'Дата подачи');    
    $cmsTable->addColumn('company', '10%', 'left', 'Компания');
    $cmsTable->addColumn('alias', '10%');
    $cmsTable->addColumn('name', '15%', 'left', 'Название работы');    
    $cmsTable->addColumn('category', '10%', 'center', 'Категория');    
    $cmsTable->addColumn('status', '10%');
    $cmsTable->setColumnParam('status', 'order', 'tb_work.status');
    $cmsTable->addColumn('status_paid', '10%', 'center', 'Статус оплаты');
    $cmsTable->setColumnParam('status_paid', 'order', 'tb_work.is_paid');
    $cmsTable->addColumn('award', '20%', 'center', 'Номинация');
    $cmsTable->addColumn('active', '10%', 'center');
    $cmsTable->setColumnParam('active', 'editable', true);

    
    $cms_table = $cmsTable->display();
    $TmplContent->set('cms_works', $cms_table);
    
    unset($cmsTable);

//счета пользователя
        function cms_filter_bill($row) {
            global $DB;
            $works = $DB->fetch_column("select CONCAT('<b>', tb_work.`alias`, '.</b> ', tb_work.`name`) from kakadu_works tb_work "
                    . " inner join kakadu_bills_works as tb_bill on tb_bill.work_id=tb_work.id "
                    . " where tb_bill.bill_id='{$row['id']}'");
            $row['works'] =  '<small>'. implode("<br/> ", $works) . '</small>';
            $row['count_works'] = count($works);
            $row['paid'] = ($row['is_paid'] == 0) ? '<span style="color:red">'.$row['paid'].'</span>' : $row['paid'];
            return $row;
        }
    
        $query = "
            select 
                tb_bill.*,
                DATE_FORMAT(tb_bill.date_bill, '%d.%m.%Y') as date,
                IF(tb_bill.is_paid = 1, 'Оплачен', 'Не оплачен') as paid
            from kakadu_bills as tb_bill  
            where tb_bill.season_id='$season_id' AND tb_bill.user_id='$user_id'
            order by tb_bill.date_bill DESC
        ";
        $cmsTable = new cmsShowView($DB, $query);    
        $cmsTable->setParam('prefilter', 'cms_filter_bill');
        $cmsTable->setParam('show_filter', false);
        if (empty($season_row) || $season_row['active'] == 0) {
            $cmsTable->setParam('add', false);
            $cmsTable->setParam('edit', false);
        }
        $cmsTable->addColumn('date', '10%', 'center', 'Дата счета');  
        $cmsTable->addColumn('id', '5%', 'left', 'ID счета');          
        $cmsTable->addColumn('paid', '10%', 'center', 'Статус');
        $cmsTable->addColumn('price', '10%', 'center', 'Сумма, грн');
        $cmsTable->addColumn('count_works', '5%', 'center', 'К-ство работ');
        $cmsTable->addColumn('works', '20%', 'left', 'Работы');
        
        $TmplContent->set('cms_bills', $cmsTable->display());
    
        unset($cmsTable);
   