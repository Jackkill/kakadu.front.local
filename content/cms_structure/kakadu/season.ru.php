<?php
/**
 * Сезоны
 * 
 * @package DeltaCMS 
 * @subpackage Kakadu
 * @author Naumenko A.
 * @copyright c-format, 2016
 */

    $status = cmsTable::getEnumFields('kakadu_season', 'status');
 
    function cms_filter($row) {
        global $status;
        $style = ($row['status'] == 'none') ? 'grey' : 'green';
        $row['status'] = "<span style='color:$style'>" .$status[$row['status']] . '</span>';
        
        return $row;
    }
    $query = "
            select 
                tb_season.id,          
                IF(tb_season.active='0', CONCAT('<font style=\"color:gray\">', tb_season.season, '</font>'), tb_season.season) AS name,                                
                CONCAT(DATE_FORMAT(`start_take`, '%d.%m.%Y'), ' - ', DATE_FORMAT(`end_take`, '%d.%m.%Y')) as date_take,
                CONCAT(DATE_FORMAT(`start_poll`, '%d.%m.%Y'), ' - ', DATE_FORMAT(`end_poll`, '%d.%m.%Y')) as date_poll,
                DATE_FORMAT(`start_awards`, '%d.%m.%Y %H:%i') as start_awards,
                IF(tb_season.active = 0, 'none', tb_season.`status`) as status,
                tb_season.active
            from kakadu_season as tb_season         
            order by tb_season.season DESC
    ";
    $cmsTable = new cmsShowView($DB, $query);    
    $cmsTable->setParam('prefilter', 'cms_filter');
    $cmsTable->setParam('delete', false);
    $cmsTable->setParam('parent_link', '/admin/site/infoblock/?');      
   
    $cmsTable->addColumn('name', '20%', 'left', 'Сезон');    
    $cmsTable->addColumn('date_take', '20%', 'center', 'Прием работ');    
    $cmsTable->addColumn('date_poll', '20%', 'center', 'Голосование');
    $cmsTable->addColumn('start_awards', '20%');
    $cmsTable->addColumn('status', '10%', 'center');
    // $cmsTable->addColumn('active', '5%', 'center');
    echo $cmsTable->display();
    unset($cmsTable);
    