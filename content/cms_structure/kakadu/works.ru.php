<?php
/**
 * Поданные работы - проекты
 * 
 * @package DeltaCMS 
 * @subpackage Kakadu
 * @author Naumenko A.
 * @copyright c-format, 2016
 */

$id = globalVar($_REQUEST['id'], 0);
$group_id = globalVar($_REQUEST['group_id'], 0);

$Awards = new Awards();

$season = globalVar($_REQUEST['season'], 0);
if (!$season) $season = $Awards->getCurrentYearAwards();
$season_row = $Awards->getInfoByYear($season);
    
$isAllowed = (!empty($season_row)) && ($season_row['active'] == 1);
    
//if (IS_DEVELOPER) {
//    
//
//    
//} else {
//    $season_id = globalVar($_REQUEST['season_id'], SEASON_ID);
//    $season_row = $Awards->getInfo($season_id);
//}

//$Awards->rebuildAlias();
/*
if (IS_DEVELOPER) {
    $ids = $DB->fetch_column("select id from kakadu_works where 1");
    foreach ($ids as $id) {
        $poll = (int)$DB->result("select SUM(`poll`) from kakadu_works_poll where work_id = '$id'");
        
        $query = "update kakadu_works SET poll = '$poll' WHERE id='$id'";
        $DB->query($query);
    }
}*/

if (!empty($id)) {
        
    $TmplContent->set('page_work', true);
    
    $query = "
            select 
                tb_work.*,
                tb_work.award_id as award,
                DATE_FORMAT(tb_work.create_dtime, '%d.%m.%Y %H:%i') as create_dtime,
                DATE_FORMAT(tb_work.last_modified, '%d.%m.%Y %H:%i') as last_modified,
                IFNULL(tb_work.file_link, '') as file_link,
                IFNULL(tb_work.file_image, '') as file_image,
                IFNULL(tb_work.file_video, '') as file_video,
                IFNULL(tb_work.file_audio, '') as file_audio,
                kakadu_season.uniq_name as year,
                IF(tb_bill.bill_id is null, '', 'Запрошен счет') as bills,
                 CONCAT('<a href=\"/admin/kakadu/users/user_works/?user_id=', tb_user.id, '\" target=\"_blank\">', tb_user.name, '</a>') as user_id,
                IF (tb_work.for_poll = 1, 'Да', 'Нет') as for_poll,
                tb_work.alias,tb_work.for_poll as is_forpoll,
                IF (tb_work.is_edit = 1, 'Да', 'Нет') as is_edit,
                IF (tb_work.active = 1, 'Да', 'Нет') as active,
                IF (tb_work.is_paid = 1, 'Да', 'Нет') as is_paid,
                tb_group.name_".LANGUAGE_CURRENT." as group_id,
                IFNULL(tb_award.name_".LANGUAGE_CURRENT.", '') as award_id
            from kakadu_works as tb_work
            LEFT JOIN `kakadu_season` ON `kakadu_season`.id = tb_work.season_id
            left join auth_user as tb_user on tb_user.id=tb_work.user_id
            left join kakadu_group as tb_group on tb_group.id=tb_work.group_id
            left join kakadu_awards as tb_award on tb_award.id=tb_work.award_id
            left join kakadu_bills_works as tb_bill on tb_bill.work_id=tb_work.id
            where `kakadu_season`.`season` = '{$season}' " . where_clause('tb_work.id', $id)."
            order by tb_work.create_dtime desc
    ";
    
//    if (IS_DEVELOPER) {
//        
//    } else {
//        $query = "
//            select 
//                tb_work.*,
//                tb_work.award_id as award,
//                DATE_FORMAT(tb_work.create_dtime, '%d.%m.%Y %H:%i') as create_dtime,
//                DATE_FORMAT(tb_work.last_modified, '%d.%m.%Y %H:%i') as last_modified,
//                IFNULL(tb_work.file_link, '') as file_link,
//                IFNULL(tb_work.file_image, '') as file_image,
//                IFNULL(tb_work.file_video, '') as file_video,
//                IFNULL(tb_work.file_audio, '') as file_audio,
//                IF(tb_bill.bill_id is null, '', 'Запрошен счет') as bills,
//                 CONCAT('<a href=\"/admin/kakadu/users/user_works/?user_id=', tb_user.id, '\" target=\"_blank\">', tb_user.name, '</a>') as user_id,
//                IF (tb_work.for_poll = 1, 'Да', 'Нет') as for_poll,
//                tb_work.alias,tb_work.for_poll as is_forpoll,
//                IF (tb_work.is_edit = 1, 'Да', 'Нет') as is_edit,
//                IF (tb_work.active = 1, 'Да', 'Нет') as active,
//                IF (tb_work.is_paid = 1, 'Да', 'Нет') as is_paid,
//                tb_group.name_".LANGUAGE_CURRENT." as group_id,
//                IFNULL(tb_award.name_".LANGUAGE_CURRENT.", '') as award_id
//            from kakadu_works as tb_work
//            left join auth_user as tb_user on tb_user.id=tb_work.user_id
//            left join kakadu_group as tb_group on tb_group.id=tb_work.group_id
//            left join kakadu_awards as tb_award on tb_award.id=tb_work.award_id
//            left join kakadu_bills_works as tb_bill on tb_bill.work_id=tb_work.id
//            where tb_work.season_id='$season_id' ".where_clause('tb_work.id', $id)."
//            order by tb_work.create_dtime desc
//    ";
//    }
      
    
    $data = $DB->query_row($query);
    
     if (empty($data)) {
        header("Location: /admin/kakadu/works/");
        exit();
    }
    $TmplContent->set('isAllowed', $isAllowed);
    $TmplContent->set('work', $data);
    $TmplContent->set('id', $id);
    $TmplContent->set('checked', ($data['status'] == 'checked') ? 1 : 0);
    
    if (!empty($data['file_link'])){
        if (!preg_match('/^(http:)/', $data['file_link'])){
            $data['file_link'] = 'http://' . $data['file_link'];
        }
         $data['file_link'] = '<a href="'.$data['file_link'].'" target="_blank">'.$data['file_link'].'</a>' ;
    }
    
    $file = Uploads::getIsFile('kakadu_works', 'file_image', $data['id'], $data['file_image']);
    $data['file_image'] = (!empty($file)) ? '<a href="'.$file.'" target="_blank"><img src="'.$file.'" style="width:400px" alt=""></a>' : "";
    
    $file = Uploads::getIsFile('kakadu_works', 'file_audio', $data['id'], $data['file_audio']);
    $data['file_audio'] = Uploads::setAudio($file);
    
    if (!empty($data['file_video'])){
        $code = explode('?v=', $data['file_video']);
        if (!isset($code[1])){
            $backs = explode('/', $data['file_video']);
            $code = array_pop($backs);
        } else $code = $code[1];
        $data['file_video'] = Uploads::fancyVideo('Посмотреть', $code);
    }
    
    
    $data['is_paid'] .= ' ' .$data['bills']; 
    $data['status'] = ($data['status'] == 'new') ? 'Новая' : (($data['status'] == 'checked') ? 'Проверенная' : '');
    
    $fields = cmsTable::getFields('2898');
    $fields['id'] = array('title'=>"ID");
    
    $skip_fields = array('priority', 'season_id', 'uniq_name', 'poll');
    
    $i=0;
    $fp = $TmplContent->iterate('/work/', null, array('title'=>'Основная информация') );
    foreach($fields as $key=>$row){
        if (in_array($key, $skip_fields)) {
            continue;
        }
        if (isset($data[$key])) { 
            $TmplContent->iterate('/work/fields/', $fp, array('keysd'=>$row['title'], 'value'=>$data[$key])); 
        } elseif ($row['cms_type'] == 'devider') {
            $i++;
            $fp = $TmplContent->iterate('/work/', null, array('title'=>$row['title'], 'i'=>$i));
        }
    }
    
    $commands = $DB->query("SELECT name as keysd, position as value FROM `kakadu_works_commands` WHERE work_id='$id' ");
    $fp = $TmplContent->iterate('/work/', null, array('title'=>'Состав') );
    $TmplContent->iterateArray('/work/fields/', $fp, $commands);  
   
    $awards = $DB->fetch_column("select id, name_".LANGUAGE_CURRENT." from kakadu_awards where season_id='{$season_id}' AND active=1 ORDER BY priority ");
    $TmplContent->set('award',TemplateUDF::html_options(array('options'=>$awards, 'selected'=>$data['award'])) );
      
} else {
    
    $status = cmsTable::getEnumFields('kakadu_works', 'status');

   
    function cms_filter($row) {
        global $status;
        
        if ($row['year'] <= 2012) {
            $row['name'] = '<span class="edit_text"></span> ' . $row['edit'];
        } elseif ($row['year'] >= 2016) {
            $row['name'] = "<a href='./?season=" . $row['year'] . "&id=" . $row['id'] . "'>" . $row['name'] . '</a>';
        }
        
//        if (IS_DEVELOPER) {
//            
//            
//        } else {
//            $row['alias'] = "<b>". $row['alias'] . "</b>";
//            $row['name'] = "<a href='./?season_id=" . $row['season_id'] . "&id=".$row['id']."'>" . $row['name'] . '</a>';
//            $row['company'] = "<a href='/admin/kakadu/users/user_works/?user_id=".$row['user_id']."'> " . $row['company'] . "</a>";
//        $row['status'] = "<span ". (($row['status'] == 'new') ? " style='color:green' " : " style='color:grey' ") . "> " . $status[$row['status']] . "</span>" ;
//        
//        $row['category'] = "<b>".$row['group_alias']."</b> <small>(" . $row['category'] .")</small>";
//        
//        $row['status_paid'] = ($row['is_paid'] == 0) 
//                ? '<span style="color:red">'.$row['paid'] . (($row['bill_id'] > 0) ? ' / Запрошен счет' : '') . '</span>'
//                : $row['paid'];
//        
//        $row['user_id'] -= intval(USER_DIFF_POINTS);
//        }
        
        return $row;
    }
    
    $season = (int)$season;
        if ($season > 2015) {
            $query = "SELECT 
                        tb_work.*,
                        DATE_FORMAT(tb_work.create_dtime, '%d.%m.%Y') as date,
                        kakadu_season.uniq_name as year,
                        tb_user.name as company,
                        tb_work.alias,
                        tb_group.alias as group_alias,
                        tb_bill.bill_id,
                        tb_work.poll,
                        tb_work.for_poll,
                        IF (tb_work.is_paid = 1, 'Оплачен', 'Не оплачен') as paid,
                        tb_group.name_".LANGUAGE_CURRENT." as category,
                        tb_award.name_".LANGUAGE_CURRENT." as award
                    from kakadu_works as tb_work
                    LEFT JOIN `kakadu_season` ON `kakadu_season`.id = tb_work.season_id
                    left join auth_user as tb_user on tb_user.id=tb_work.user_id
                    left join kakadu_group as tb_group on tb_group.id=tb_work.group_id
                    left join kakadu_awards as tb_award on tb_award.id=tb_work.award_id
                    left join kakadu_bills_works as tb_bill on tb_bill.work_id=tb_work.id
                    where `kakadu_season`.`season` = '{$season}' " . where_clause('tb_work.group_id', $group_id)."
                    group by tb_work.id    
                    order by tb_work.create_dtime desc
            ";
            $cmsTable = new cmsShowView($DB, $query);    
            $cmsTable->setParam('prefilter', 'cms_filter');
            $cmsTable->filterSkipTable('auth_user');
            
            if (!$isAllowed) $cmsTable->setParam('edit', false);
            //if (IS_DEVELOPER) 
            	$TmplContent->set('show_btn_generate', TRUE);
            
            $cmsTable->addColumn('user_id', '10%', 'center', 'User ID'); 
            $cmsTable->addColumn('id', '5%', 'center', 'Work ID'); 
            $cmsTable->addColumn('date', '10%', 'left', 'Дата подачи');

            $cmsTable->addColumn('company', '15%', 'left', 'Компания');
            $cmsTable->setColumnParam('company', 'order', 'tb_user.name');

            $cmsTable->addColumn('alias', '5%');
            $cmsTable->addColumn('name', '20%');  
            $cmsTable->setColumnParam('alias', 'order', 'tb_work.alias');

            $cmsTable->addColumn('category', '10%', 'center', 'Категория');  
            $cmsTable->setColumnParam('category', 'order', 'tb_work.group_id');
            
            $cmsTable->addColumn('poll', '10%', 'center', 'К-ство баллов');
            $cmsTable->setColumnParam('poll', 'order', 'tb_work.poll');
                
            $cmsTable->addColumn('for_poll', '5%', 'center', 'Допущен к голосованию');
            $cmsTable->setColumnParam('for_poll', 'editable', true);

            $cmsTable->addColumn('status_paid', '10%', 'center', 'Статус оплаты');
            $cmsTable->addColumn('award', '20%', 'center', 'Номинация');
            
        } else {
            $query = "SELECT `kakadu_winners_old`.*,"
                    . " html_editor(id, 'kakadu_winners_old', 'text', name) as edit,"
                    . " DATE_FORMAT(`kakadu_winners_old`.`created_at`, '%d.%m.%Y') AS date"
                    . " FROM `kakadu_winners_old`"
                    . " WHERE `year` = '{$season}' AND `active` = 1";
            
            $cmsTable = new cmsShowView($DB, $query);
            
            $cmsTable->setParam('prefilter', 'cms_filter');
            
            if (empty($season_row) && $season <= 2012) $cmsTable->setParam('edit', false);
            
            $cmsTable->addColumn('id', '5%', 'center', 'Winner ID');
            $cmsTable->addColumn('name', '20%');
            $cmsTable->addColumn('date', '10%', 'left', 'Дата подачи');
            $cmsTable->addColumn('author', '15%', 'left', 'Автор');
            $cmsTable->setColumnParam('author', 'order', 'author');
            $cmsTable->addColumn('category', '10%', 'center', 'Категория');
            $cmsTable->addColumn('alias', '5%');
            $cmsTable->setColumnParam('alias', 'order', 'kakadu_winners_old.alias');
            
        }
        
        //	$cmsTable->setParam('delete', false);
        $cmsTable->setParam('add', false);
        
        $cmsTable->addColumn('active', '10%', 'center');
        $cmsTable->setColumnParam('active', 'editable', true);
        $TmplContent->set('cms_works', Awards::admin_season_menu_year($season, 'works/', $cmsTable->display()));
//        $cms_table = $cmsTable->display();
//        echo Awards::admin_season_menu_year($season, 'works/', $cmsTable->display());
        unset($cmsTable);
    
//    if (IS_DEVELOPER) {
//        
//       
//    } else {
//        $query = "
//            select 
//                tb_work.*,
//                DATE_FORMAT(tb_work.create_dtime, '%d.%m.%Y') as date,
//                tb_user.name as company,
//                tb_work.alias,
//                tb_group.alias as group_alias,
//                tb_bill.bill_id,
//                tb_work.poll,
//                tb_work.for_poll,
//                IF (tb_work.is_paid = 1, 'Оплачен', 'Не оплачен') as paid,
//                tb_group.name_".LANGUAGE_CURRENT." as category,
//                tb_award.name_".LANGUAGE_CURRENT." as award
//            from kakadu_works as tb_work 
//            left join auth_user as tb_user on tb_user.id=tb_work.user_id
//            left join kakadu_group as tb_group on tb_group.id=tb_work.group_id
//            left join kakadu_awards as tb_award on tb_award.id=tb_work.award_id
//            left join kakadu_bills_works as tb_bill on tb_bill.work_id=tb_work.id
//            where tb_work.season_id='$season_id' ".where_clause('tb_work.group_id', $group_id)."
//            group by tb_work.id    
//            order by tb_work.create_dtime desc
//    ";
//    $cmsTable = new cmsShowView($DB, $query);    
//    $cmsTable->setParam('prefilter', 'cms_filter');
//    $cmsTable->filterSkipTable('auth_user');
//    $cmsTable->setParam('delete', false);
//    $cmsTable->setParam('add', false);
//    
//    if (empty($season_row) || $season_row['active'] == 0) {
//        $cmsTable->setParam('edit', false);
//    }
//    //$cmsTable->setParam('parent_link', '/admin/site/infoblock/?');      
//    $cmsTable->addColumn('user_id', '10%', 'center', 'User ID'); 
//    $cmsTable->addColumn('id', '5%', 'center', 'Work ID'); 
//    $cmsTable->addColumn('date', '10%', 'left', 'Дата подачи');
//    
//    $cmsTable->addColumn('company', '15%', 'left', 'Компания');
//    $cmsTable->setColumnParam('company', 'order', 'tb_user.name');
//    
//    $cmsTable->addColumn('alias', '5%');
//    $cmsTable->addColumn('name', '20%');  
//    $cmsTable->setColumnParam('alias', 'order', 'tb_work.alias');
//    
//    $cmsTable->addColumn('category', '10%', 'center', 'Категория');  
//    $cmsTable->setColumnParam('category', 'order', 'tb_work.group_id');
    
   // if (AwardsSeason::isPoll() || AwardsSeason::isAwards() ){
//        $cmsTable->addColumn('poll', '10%', 'center', 'К-ство баллов');
//        $cmsTable->setColumnParam('poll', 'order', 'tb_work.poll');
   // } else {
//   $cmsTable->addColumn('status', '10%');
//        $cmsTable->setColumnParam('status', 'order', 'tb_work.status');
//    }
//    $cmsTable->addColumn('for_poll', '5%', 'center', 'Допущен к голосованию');
//    $cmsTable->setColumnParam('for_poll', 'editable', true);
    
//    $cmsTable->addColumn('status_paid', '10%', 'center', 'Статус оплаты');
//    $cmsTable->addColumn('award', '20%', 'center', 'Номинация');
//    $cmsTable->addColumn('active', '10%', 'center');
//    $cmsTable->setColumnParam('active', 'editable', true);
    /*
    if (!SEASON_ID){
        echo '<div class="message_align"><table border="0" cellpadding="0" cellspacing="0">';
        echo '<tbody><tr><td>Для добавления новых раб создайте <b>Новый сезон</b></td></tr></tbody>';
        echo '</table></div>';
    }*/
    
//    $cms_table = $cmsTable->display();
//    echo Awards::admin_season_menu($season_id, 'works/', $cms_table);
//    
//    unset($cmsTable);
//    }
    
    
}