<?php
/**
 * Отчеты
 * @package DeltaCMS
 * @subpackage kakadu
 * @copyright c-format, 2016
 */

$type = globalVar($_GET['type'], 'users');

$TmplContent->set('type', $type);
 
$season_id  = AwardsSeason::getID();
$Awards     = new Awards();
$season_row = $Awards->getInfo($season_id);


if ($type == 'users') {
   
    function row_filter($row) {
        if ($row['count_work'] == 0) {
            $row['count_work'] = '';
        }
        $row['user_id'] = $row['id']  - intval(USER_DIFF_POINTS);
        return $row;
    }

    $title = "Все учасники " . $season_row['season'];
    $_SESSION['xlsx_filename'] = $season_row['season'] .'_all_users';

    $query = "
        SELECT 
            tb_user.id,
            tb_user.email,
            tb_user.name as user_name,
            tb_data.person_reger,
            tb_data.city,
            tb_data.phone,
            (select count(id) from kakadu_works where user_id=tb_user.id) as count_work
        FROM auth_user AS tb_user
        INNER JOIN auth_user_data as tb_data on tb_data.user_id = tb_user.id
        WHERE tb_user.group_id = 0 AND tb_user.season_id = '$season_id'
        GROUP BY tb_user.id
        ORDER BY tb_user.id
    ";
    $cmsTable = new cmsShowView($DB, $query);

    $cmsTable->setParam('row_filter', 'row_filter');
    $cmsTable->setParam('title', $title);
    $cmsTable->setParam('edit', false);
    $cmsTable->setParam('add', false);
    $cmsTable->setParam('delete', false);
    $cmsTable->setParam('show_filter', false);
    $cmsTable->setParam('excel', true);

    $cmsTable->addColumn('user_id', '5%', 'center', 'ID');
    $cmsTable->addColumn('user_name', '20%', 'left', 'Наименование пользователя');
    $cmsTable->addColumn('count_work', '10%', 'center', 'К-ство работ');
    $cmsTable->addColumn('city', '10%', 'left', 'Город');
    $cmsTable->addColumn('person_reger', '20%', 'left', 'Контактное лицо');
    $cmsTable->addColumn('email', '15%', 'left', 'E-mail');
    $cmsTable->addColumn('phone', '15%', 'left', 'Телефон');
    $TmplContent->set('cms_table', $cmsTable->display());
    unset($cmsTable);
    
} elseif ($type == 'users_cities') {
    $query = "
        SELECT 
            tb_data.city,
            COUNT(tb_user.id) as count_user
        FROM auth_user_data as tb_data
        INNER JOIN auth_user AS tb_user on tb_data.user_id = tb_user.id
        WHERE tb_user.group_id = 0 AND tb_user.season_id = '$season_id'  AND tb_data.city <> ''
        GROUP BY tb_data.city
        ORDER BY tb_data.city 
    ";
    $cmsTable = new cmsShowView($DB, $query);
    $title = "Города - Учасники " . $season_row['season'];
    $_SESSION['xlsx_filename'] = $season_row['season'] .'_cities_users';

    //$cmsTable->setParam('row_filter', 'row_filter');
    $cmsTable->setParam('title', $title);
    $cmsTable->setParam('edit', false);
    $cmsTable->setParam('add', false);
    $cmsTable->setParam('delete', false);
    $cmsTable->setParam('show_filter', false);
    $cmsTable->setParam('excel', true);

    $cmsTable->addColumn('city', '40%', 'left', 'Город');
    $cmsTable->addColumn('count_user', '40%', 'left', 'К-ство учасников');
    $TmplContent->set('cms_table', $cmsTable->display());
    unset($cmsTable);
    
}  elseif ($type == 'cities_works') {

    $query = "
        SELECT 
            tb_data.city,
            COUNT(tb_works.id) as count_works
        FROM auth_user_data as tb_data
        INNER JOIN auth_user AS tb_user on tb_data.user_id = tb_user.id
        INNER JOIN kakadu_works AS tb_works on tb_works.user_id = tb_user.id
        WHERE tb_user.group_id = 0 AND tb_user.season_id = '$season_id'  AND tb_data.city <> ''
        GROUP BY tb_data.city
        ORDER BY tb_data.city 
    ";
    $cmsTable = new cmsShowView($DB, $query);
    $title = "Города - Работы " . $season_row['season'];
    $_SESSION['xlsx_filename'] = $season_row['season'] .'_cities_works';

    //$cmsTable->setParam('row_filter', 'row_filter');
    $cmsTable->setParam('title', $title);
    $cmsTable->setParam('edit', false);
    $cmsTable->setParam('add', false);
    $cmsTable->setParam('delete', false);
    $cmsTable->setParam('show_filter', false);
    $cmsTable->setParam('excel', true);

    $cmsTable->addColumn('city', '40%', 'left', 'Город');
    $cmsTable->addColumn('count_works', '40%', 'left', 'К-ство работ');
    $TmplContent->set('cms_table', $cmsTable->display());
    unset($cmsTable);
    
}  elseif ($type == 'category_works') {

    function row_filter($row){
        global $DB, $season_id;
        
        $queries = "SELECT count(id) FROM kakadu_works "
                . " WHERE season_id = '$season_id' "
                . " AND group_id in (SELECT id FROM kakadu_group_relation WHERE parent='{$row['id']}') ";
        $row['count_works'] = $DB->result($queries);        
        return $row;
    }
    
    $query = "
        SELECT 
            tb_group.id,
            CONCAT(tb_group.alias, '. ', tb_group.name_".LANGUAGE_CURRENT.") as name
        FROM kakadu_group AS tb_group
        WHERE tb_group.group_id = 0 AND tb_group.season_id = '$season_id'
        ORDER BY tb_group.priority 
    ";
    $cmsTable = new cmsShowView($DB, $query);
    
    $title = "Номинации - Работы " . $season_row['season'];
    $_SESSION['xlsx_filename'] = $season_row['season'] .'_works_count_report';

    $cmsTable->setParam('row_filter', 'row_filter');
    $cmsTable->setParam('title', $title);
    $cmsTable->setParam('edit', false);
    $cmsTable->setParam('add', false);
    $cmsTable->setParam('delete', false);
    $cmsTable->setParam('priority', false);
    $cmsTable->setParam('show_filter', false);
    $cmsTable->setParam('excel', true);

    $cmsTable->addColumn('name', '40%', 'left', 'Номинация');
    $cmsTable->addColumn('count_works', '40%', 'left', 'К-ство работ');
    $TmplContent->set('cms_table', $cmsTable->display());
    unset($cmsTable);
    
}  elseif ($type == 'category') {

    $group_id = globalVar($_GET['group_id'], 0);
    
    $categories = $Awards->categories(0);
    if (empty($group_id)) {
        $group_id = key($categories);
    }
    if (isset($categories[$group_id])) {
        $categories[$group_id]['selected'] = true;
    } else {
        Header("Location: /admin/kakadu/report/?type=category");
        exit();
    }
    $TmplContent->set('show_categories', count($categories));
    $TmplContent->iterateArray('/categories/', null, $categories);
    
    $query = "
        SELECT 
            tb_works.id, tb_works.advertiser,
            CONCAT(tb_works.alias, '. ', tb_works.name) as name,
            tb_user.name as user_name,
            CONCAT(tb_group.alias, '. ', tb_group.name_".LANGUAGE_CURRENT.") as group_name
        FROM kakadu_works as tb_works
        INNER JOIN auth_user AS tb_user on tb_user.id=tb_works.user_id
        LEFT JOIN kakadu_group as tb_group ON tb_group.id=tb_works.group_id
        WHERE tb_works.season_id = '$season_id' AND tb_works.group_id in (SELECT id FROM kakadu_group_relation WHERE parent = '$group_id')
        ORDER BY tb_group.priority, tb_works.id
    ";
    $cmsTable = new cmsShowView($DB, $query);
    
    $title = $categories[$group_id]['alias'] . '. ' . $categories[$group_id]['name'] . ' ' . $season_row['season'];
    $_SESSION['xlsx_filename'] = $categories[$group_id]['alias'] . '_report_' . $season_row['season'];

    //$cmsTable->setParam('row_filter', 'row_filter');
    $cmsTable->setParam('title', $title);
    $cmsTable->setParam('subtitle', 'group_name');
    $cmsTable->setParam('edit', false);
    $cmsTable->setParam('add', false);
    $cmsTable->setParam('delete', false);
    $cmsTable->setParam('priority', false);
    $cmsTable->setParam('show_filter', false);
    $cmsTable->setParam('excel', true);
    $cmsTable->addColumn('user_name', '30%', 'left', 'Учасник');
    $cmsTable->addColumn('name', '30%', 'left', 'Работа');
    $cmsTable->addColumn('advertiser', '30%', 'left');
    $TmplContent->set('cms_table', $cmsTable->display());
    unset($cmsTable); 
 
} elseif ($type == 'poll' ) {
    
    $categories = $Awards->categories(0);
    $TmplContent->iterateArray('/categories/', null, $categories);
    
}

