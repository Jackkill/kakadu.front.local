<?php
/**
 * Список учасников сезона
 * @package DeltaCMS
 * @subpackage kakadu
 * @copyright c-format, 2016
 */

$season_id = AwardsSeason::getID();

$count_all_works = $DB->result("select count(id) from kakadu_works where season_id = '$season_id' and for_poll=1");
function row_filter($row) {
        global $count_all_works, $DB, $season_id;
        
        $style = ($row['confirmed'] == 0) ? "style='color:red'" : '';
       
        //if ($row['count_work']>0){ 
            $row['user_name'] = "<a href='/admin/kakadu/users/user_works/?user_id=".$row['id']."' $style> " . $row['user_name'] . "</a>";
        //}
        
        $row['user_id'] = $row['id'] - intval(USER_DIFF_POINTS);
        $row['confirmed'] = ($row['confirmed'] == 0) ? 'Не активирован' : 'Активирован';
         
        $row['poll_work'] .= ' / ' . $count_all_works; 
        
        if ($row['count_work']>0){
            $status = array();
            $is_paid = $DB->result("select count(id) from kakadu_works where user_id='{$row['id']}' and season_id='$season_id' and is_paid = 1");
            if ($is_paid == $row['count_work']) {
                $status[] = 'Все оплачено';
            } else {
                if ($is_paid > 0) {
                    $status[] = 'Оплачено';
                }
                $count_wait = $DB->result("select count(tb_work.id) from kakadu_works tb_work"
                        . " inner join kakadu_bills_works as tb_bill on tb_bill.work_id=tb_work.id"
                        . " where tb_work.user_id='{$row['id']}' and tb_work.is_paid = 0 and tb_work.season_id='$season_id' ");
                        
                if ($count_wait > 0){
                    $status[] = 'Ожидается оплата';
                }
                if ( ($row['count_work'] - $count_wait) > 0 ) {
                    $status[] = 'Cчет не запрошен'; 
                }
            }
            $row['status'] = implode(' / ', $status );
        }
        //$row['user_name'] = "<span $style>{$row['user_name']}</span>";
        $row['confirmed'] = "<span $style>{$row['confirmed']}</span>";
        $row['person_reger'] = "<span $style>{$row['person_reger']}</span>";
        $row['email'] = "<span $style>{$row['email']}</span>";
        $row['count_work'] = "<span $style>{$row['count_work']}</span>";
        $row['status'] = "<span $style>{$row['status']}</span>";
        $row['poll_work'] = "<span $style>{$row['poll_work']}</span>";
	//$row['switch'] = "<a title='Перейти в аккаунт этого пользователя' class='aicons switch' href='/".LANGUAGE_URL."action/admin/auth/switch_user/?switch_id=". $row['id'] . "&_return_path=".urlencode('/')."'>&nbsp;</a>";
	return $row;
}

$query = "
	SELECT 
		tb_user.id,
		tb_user.login as login,
                tb_user.email,
                '' as status,
                tb_user.confirmed,
		tb_user.name as user_name,
                tb_data.person_reger,
                (select count(id) from kakadu_works where user_id=tb_user.id) as count_work,
                (select count(work_id) from kakadu_works_poll where user_id=tb_user.id) as poll_work
	FROM auth_user AS tb_user
        INNER JOIN auth_user_data as tb_data on tb_data.user_id = tb_user.id
        WHERE tb_user.group_id = 0 AND tb_user.season_id = '$season_id' 
	GROUP BY tb_user.id
	ORDER BY tb_user.registration_dtime DESC
";

$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('row_filter', 'row_filter');

$cmsTable->addColumn('user_id', '5%', 'center', 'ID');

//$cmsTable->addColumn('login', '30%');
//$cmsTable->setColumnParam('login', 'order', 'login');
$cmsTable->addColumn('user_name', '20%', 'left', 'Наименование пользователя / компании');
$cmsTable->setColumnParam('user_name', 'order', 'tb_user.name');

$cmsTable->addColumn('confirmed', '5%','center');
$cmsTable->addColumn('person_reger', '10%', 'left', 'Контактное лицо');
$cmsTable->addColumn('email', '10%');
$cmsTable->addColumn('count_work', '10%', 'center', 'К-ство работ');

$cmsTable->addColumn('status', '10%', 'center', 'Статус');

$cmsTable->addColumn('poll_work', '10%', 'center', 'Голосование');

echo $cmsTable->display();
unset($cmsTable);


?>