<?php
/**
 * Категории - Номинации
 * 
 * @package DeltaCMS 
 * @subpackage Kakadu
 * @author Naumenko A.
 * @copyright c-format, 2016
 */

    $options = cmsTable::getEnumFields('kakadu_group');

    $group_id = globalVar($_REQUEST['group_id'], 0);
    $season_id = globalVar($_REQUEST['season_id'], SEASON_ID);
    
    $Awards = new Awards();
    $season_row = $Awards->getInfo($season_id);
    
    function cms_filter($row) {
        global $options;
        
        $file = Uploads::getIsFile('kakadu_group', 'icon', $row['id'], $row['icon']);
        $row['icon'] = Uploads::fancyImage($file, $row['name'], 40);
        
        $files = array();
        if ($row['file_image'] != 'none') $files[] = "Изображение (" . $options['file_image'][$row['file_image']] . ")";
        if ($row['file_video'] != 'none') $files[] = "Видео (" . $options['file_video'][$row['file_video']] . ")";
        if ($row['file_audio'] != 'none') $files[] = "Аудио (" . $options['file_audio'][$row['file_audio']] . ")";
        if ($row['file_link'] != 'none')  $files[] = "Ссылка (" . $options['file_link'][$row['file_link']] . ")";
        
        $row['files'] = "<small>" .implode('<br/>', $files) . "</small>";
        return $row;
    }
    $query = "
            select 
                tb_group.id,          
                tb_group.name_".LANGUAGE_CURRENT." as name,
                tb_group.price,
                tb_group.alias,
                tb_group.icon,
                tb_group.active,
                tb_group.file_image, tb_group.file_audio, tb_group.file_video, tb_group.file_link,
                tb_group.priority
            from kakadu_group as tb_group  
            where tb_group.group_id = '$group_id' AND tb_group.season_id='$season_id'
            order by tb_group.priority
    ";
    $cmsTable = new cmsShowView($DB, $query);    
    $cmsTable->setParam('prefilter', 'cms_filter');
    
   
    if (empty($season_row) || $season_row['active'] == 0) {
        $cmsTable->setParam('add', false);
        $cmsTable->setParam('edit', false);
        $cmsTable->setParam('delete', false);
    }
    //$cmsTable->setParam('parent_link', '/admin/site/infoblock/?');      
   
    $cmsTable->addColumn('name', '20%', 'left', 'Сезон');    
    $cmsTable->addColumn('price', '10%', 'center', 'Стоимость');    
    $cmsTable->addColumn('alias', '10%', 'center', 'Алиас');
    $cmsTable->addColumn('files', '20%', 'center', 'Файлы');
    $cmsTable->addColumn('icon', '10%', 'center');
    $cmsTable->addColumn('active', '10%', 'center');
    $cmsTable->setColumnParam('active', 'editable', true);
    
    if (!SEASON_ID){
        echo '<div class="message_align"><table border="0" cellpadding="0" cellspacing="0">';
        echo '<tbody><tr><td>Для добавления новых категорий создайте <b>Новый сезон</b></td></tr></tbody>';
        echo '</table></div>';
    }
    
    $cms_table = $cmsTable->display();
    echo Awards::admin_season_menu($season_id, 'category/', $cms_table);
    
    unset($cmsTable);