<?php
/**
 * счета
 * 
 * @package DeltaCMS 
 * @subpackage Kakadu
 * @author Naumenko A.
 * @copyright c-format, 2016
 */


    $season_id = globalVar($_REQUEST['season_id'], SEASON_ID);
    
    $Awards = new Awards();
    $season_row = $Awards->getInfo($season_id);
    
    function cms_filter($row) {
        global $DB;
        $works = $DB->fetch_column("select CONCAT('<b>', tb_work.`alias`, '.</b> ', tb_work.`name`) from kakadu_works tb_work "
                . " inner join kakadu_bills_works as tb_bill on tb_bill.work_id=tb_work.id "
                . " where tb_bill.bill_id='{$row['id']}'");
        $row['works'] =  '<small>'. implode("<br/> ", $works) . '</small>';
        $row['count_works'] = count($works);
         $row['user_id'] -= intval(USER_DIFF_POINTS);
        $row['paid'] = ($row['is_paid'] == 0) ? '<span style="color:red">'.$row['paid'].'</span>' : $row['paid'];
        return $row;
    }
    $query = "
            select 
                tb_bill.*,
                DATE_FORMAT(tb_bill.date_bill, '%d.%m.%Y') as date,
                IF(tb_bill.is_paid = 1, 'Оплачен', 'Не оплачен') as paid,
                CONCAT('<a href=\"/admin/kakadu/users/user_works/?user_id=', tb_user.id, '\" target=\"_blank\">', tb_user.name, '</a>') as company,
                (select count(id) from kakadu_bills where user_id=tb_user.id) as count_bills
            from kakadu_bills as tb_bill  
            left join auth_user as tb_user on tb_user.id=tb_bill.user_id
            where tb_bill.season_id='$season_id'
            order by tb_bill.date_bill DESC
    ";
    $cmsTable = new cmsShowView($DB, $query);    
    $cmsTable->setParam('prefilter', 'cms_filter');
   // $cmsTable->setParam('delete', false);
    
    if (empty($season_row) || $season_row['active'] == 0) {
        $cmsTable->setParam('add', false);
        $cmsTable->setParam('edit', false);
    }
    $cmsTable->addColumn('date', '10%', 'center', 'Дата счета');  
    $cmsTable->addColumn('id', '5%', 'left', 'ID счета');  
    $cmsTable->addColumn('user_id', '5%', 'center', 'User ID');    
    $cmsTable->addColumn('company', '10%', 'center', 'Компания');   
    $cmsTable->setColumnParam('company', 'order', 'tb_user.name');
    $cmsTable->addColumn('count_bills', '10%', 'center', 'К-ство счетов пользователя');   
    $cmsTable->addColumn('paid', '10%', 'center', 'Статус');
    $cmsTable->addColumn('price', '10%', 'center', 'Сумма, грн');
    $cmsTable->addColumn('count_works', '5%', 'center', 'К-ство работ');
    $cmsTable->addColumn('works', '20%', 'left', 'Работы');
    $cms_table = $cmsTable->display();
    echo Awards::admin_season_menu($season_id, 'bills/', $cms_table);
    
    unset($cmsTable);