<?php

/**
 * Статистика по рассылки
 * @package Maillist
 * @subpackage Content_Admin
 * @author Rudenko Ilya <rudenko@delta-x.com.ua>
 * @copyright Delta-X, ltd. 2005
 */

$message_id = globalVar($_GET['message_id'], 0);
$task_id = globalVar($_GET['task_id'], 0);
$type = globalVar($_GET['type'], '');

if(empty($task_id)){
    function cms_filter($row) {
           global $DB;

           $status = array(
                'stop' => "Остановлена",
                'start' => "Запущена",
                'end' => "Завершена"
           );
           
           $percent = ($row['delivery'] > 0) ? round ( ($row['open_mail'] * 100) / $row['delivery']) : 0;
           if($percent > 0){
                    $row['open_mail'] = "<a href='./?type=open&task_id={$row['id']}' >". $row['open_mail']." ( ".$percent." %)</a>";                 
           }    
           $row['delivery'] = ($row['delivery'] > 0) ? "<a href='./?type=delivery&task_id={$row['id']}'>".$row['delivery'].'</a>' : $row['delivery'];
           $row['delivery_error'] = ($row['delivery_error'] > 0) 
                           ? "<a href='./?type=error&task_id={$row['id']}' style='color:red'>".$row['delivery_error'].'</a>' 
                           : $row['delivery'];

           $row['status'] = $status[ $row['status'] ];
          
           if( is_null($row['stop_date']) ){
               $row['stop_date'] = "<a href='/".LANGUAGE_URL."action/admin/maillist/clear_queue/?task_id={$row['id']}&_return_path=".CURRENT_URL_LINK."'>Отменить</a>";
           }
           return $row;
    }

    
    $query = "
	SELECT 
                tb_task.id,
		tb_task.status,               
                CONCAT('<small>',
                    DAY(tb_task.dtime), ' ', (CASE MONTH(tb_task.dtime) ".LANGUAGE_MONTH_GEN_SQL." END), ' ', YEAR(tb_task.dtime), '<br/>',
                    DATE_FORMAT(tb_task.dtime, '%H:%i'), '</small>'    
                ) as date,
                IF(tb_task.stop_dtime is not null,
                   CONCAT('<small>',
                        DAY(tb_task.stop_dtime), ' ', (CASE MONTH(tb_task.stop_dtime) ".LANGUAGE_MONTH_GEN_SQL." END), ' ', YEAR(tb_task.stop_dtime), '<br/>',
                        DATE_FORMAT(tb_task.stop_dtime, '%H:%i'), '</small>'    
                   ), tb_task.stop_dtime
                )  as stop_date,
                tb_message.subject,
                (SELECT count(*) FROM maillist_task_queue WHERE task_id=tb_task.id and `delivery`='ok') as delivery,
                (SELECT count(*) FROM maillist_task_queue WHERE task_id=tb_task.id and `delivery`='error') as delivery_error,
                (SELECT count(*) FROM maillist_task_queue WHERE task_id=tb_task.id and `open_mail`>'0') as open_mail
	FROM maillist_task AS tb_task
        INNER JOIN maillist_message as tb_message on tb_message.id=tb_task.message_id
	WHERE tb_task.message_id='".$message_id."'
	ORDER BY tb_task.dtime DESC
    ";
    $cmsTable = new cmsShowView($DB, $query);
    $cmsTable->setParam('prefilter', 'cms_filter');
    $cmsTable->setParam('show_parent_link', true);
    $cmsTable->setParam('parent_link', '/admin/maillist/messages/?');
    $cmsTable->setParam('title', 'Общая статистика');
    $cmsTable->setParam('add', false);
    $cmsTable->setParam('edit', false);
    $cmsTable->setParam('delete', false);
     $cmsTable->setParam('excel', false);

    //$cmsTable->addColumn('login', '30%', 'left', 'Пользователь');
    
    $cmsTable->addColumn('date', '10%', 'center', "Дата отправки");
    $cmsTable->addColumn('subject', '30%', 'left', "Тема");    
    $cmsTable->addColumn('delivery', '10%', 'center', "Доставлено");    
    $cmsTable->addColumn('delivery_error', '10%', 'center', "Не доставлено");    
    $cmsTable->addColumn('open_mail', '10%', 'center', "Открыто");    
    $cmsTable->addColumn('status', '10%', 'center');
    $cmsTable->addColumn('stop_date', '10%', 'center', "Дата завершения");
    
    echo $cmsTable->display();
    unset($cmsTable);
    
    echo "<div class=\"context_help\">
Очистка очереди приведёт к тому, что письма будут повторно доставлены всем адресатам
</div>";
}
else{
    $message_id = $DB->result("select message_id from maillist_task where id='$task_id' ");
    
    $where = ($type == 'open') ? " and `open_mail` > 0 " : " and tb_queue.delivery='ok' ";
    if($type == 'error'){
        $where = " and tb_queue.delivery='error' ";
    }
    $query = "
            SELECT 
                    tb_queue.email,		
                    tb_queue.delivery,
                    tb_queue.open_mail,
                    DATE_FORMAT(tb_queue.tstamp, '".LANGUAGE_DATETIME_SQL."') AS tstamp,
                    DATE_FORMAT(tb_queue.open_tstamp, '".LANGUAGE_DATETIME_SQL."') AS open_tstamp
            FROM maillist_task_queue AS tb_queue
            WHERE tb_queue.task_id='".$task_id."' $where
            ORDER BY tb_queue.email ASC
    ";
    $cmsTable = new cmsShowView($DB, $query);
    $cmsTable->setParam('add', false);
    $cmsTable->setParam('edit', false);
    $cmsTable->setParam('delete', false);

     $cmsTable->setParam('show_parent_link', true);
     $cmsTable->setParam('parent_link', '/admin/maillist/messages/stat/?message_id='.$message_id);

    //$cmsTable->addColumn('login', '30%', 'left', 'Пользователь');

    $cmsTable->addColumn('email', '30%', 'left', 'E-mail');
    //$cmsTable->addColumn('vars', '10%', 'center', 'Переменные');
    $cmsTable->addColumn('delivery', '10%', 'center');
    $cmsTable->addColumn('tstamp', '15%', 'center');
    $cmsTable->addColumn('open_mail', '10%', 'center', "Просмотры");
    $cmsTable->addColumn('open_tstamp', '15%', 'center', "Дата последнего открытия");
    echo $cmsTable->display();
    unset($cmsTable);


}
?>
