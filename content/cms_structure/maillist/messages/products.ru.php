<?php
/**
 * Формирование тела письма
 * @package DeltaCMS
 * @subpackage Maillist
 * @author Naumenko A.
 * @copyright ltd, c-format 2015
 */

$message_id = globalVar($_GET['message_id'], 0);


Maillist::generateContent($message_id);

$message = $DB->query_row("select * from maillist_message where id='$message_id'");
$TmplContent->set($message);

if( $message['template'] == 'products'){
    $tmpl_search = TemplateUDF::search_products(array('table_name'=>'maillist_message_product_relation'));
    $TmplContent->set('search_form', $tmpl_search);

    $TmplContent->set('utm_label', Maillist::getUtmLabel($message_id));

    $query = "
        SELECT 
                tb_mail.id,
                tb_mail.priority,
                tb_mail.utm,
                tb_shop.name_".LANGUAGE_CURRENT." as name,
                tb_shop.articul,
                tb_brand.name_".LANGUAGE_CURRENT." as brand,
                tb_version.articul as version, 		
                MIN(tb_version.newprice	) as newprice
        FROM maillist_message_product_relation as tb_mail	
        INNER JOIN  shop_product as tb_shop ON tb_shop.id=tb_mail.object_id and tb_shop.active='1'
        INNER JOIN  shop_group as tb_group ON tb_group.id=tb_shop.group_id and tb_group.active='1'
        INNER JOIN  shop_product_version as tb_version on tb_version.product_id=tb_shop.id
        LEFT JOIN  shop_brands as tb_brand on tb_brand.id=tb_shop.brand_id
        WHERE tb_mail.message_id = '$message_id' and tb_mail.table_name='shop_product' 
        group by tb_shop.id
        ORDER BY tb_mail.priority
    ";
    $cmsTable = new cmsShowView($DB, $query);

    $cmsTable->setParam("add", false);
    //$cmsTable->setParam("edit", false);
    $cmsTable->setParam("excel", false);
    $cmsTable->setParam("show_filter", false);

    $cmsTable->addColumn('name', '30%', 'left', 'Товар');
    $cmsTable->addColumn('articul', '20%', 'left', 'Артикул товара');
    $cmsTable->addColumn('brand', '10%', 'left', 'Бренд');
    $cmsTable->addColumn('newprice', '10%', 'center', 'Цена');
    $cmsTable->addColumn('utm', '30%', 'left');
    $TmplContent->set('cms_products', $cmsTable->display());
    unset($cmsTable);
}
elseif( $message['template'] == 'news' ){
    //последнии новости    
    $NEWS = new News();
    $data = $NEWS->getHeadlines(20, 20, true, null, null, " and tb_message.id not in (select object_id FROM maillist_message_product_relation where message_id = '$message_id' and table_name='news_message') ");
    $TmplContent->set('count_news', count($data) );
    $TmplContent->iterateArray('/products/', null, $data );

    $TmplContent->set('utm_label', Maillist::getUtmLabel($message_id));
    
    $query = "
        SELECT 
                tb_mail.id,
                tb_mail.priority,
                tb_mail.utm,
                tb_news.name_".LANGUAGE_CURRENT." as name,
                tb_type.name_".LANGUAGE_CURRENT." as type
        FROM maillist_message_product_relation as tb_mail	
        INNER JOIN news_message as tb_news ON tb_news.id=tb_mail.object_id  and tb_news.active='1'	
        INNER JOIN news_type as tb_type ON tb_type.id=tb_news.type_id  and tb_type.active='1'	
        WHERE tb_mail.message_id = '$message_id' and tb_mail.table_name='news_message'      
        ORDER BY tb_mail.priority
    ";
    $cmsTable = new cmsShowView($DB, $query);

    $cmsTable->setParam("title", 'Прикрепленные новости');
    $cmsTable->setParam("add", false);
    $cmsTable->setParam("excel", false);
    $cmsTable->setParam("show_filter", false);

    $cmsTable->addColumn('name', '30%', 'left', 'Название');
    $cmsTable->addColumn('type', '10%', 'left', 'Раздел');    
    $cmsTable->addColumn('utm', '30%', 'left');
    $TmplContent->set('cms_products', $cmsTable->display());
    unset($cmsTable);
}
?>
