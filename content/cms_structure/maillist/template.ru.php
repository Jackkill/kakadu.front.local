<?php
/**
 * Шаблоны раcсылки
 * @package Maillist
 * @author Naumenko A.
 * @copyright c-format, 2014
 */


$template_id = globalVar($_GET['id'], 0);

if(empty($template_id)){
    function row_filter($row) {

            $filename = Uploads::getFile('maillist_templates', 'image', $row['id'], $row['image']);
            $row['image'] = (is_file($filename) && is_readable($filename)) ? "<img src='".Uploads::getURL($filename)."' width='250px'>" : "";
            return $row;
    }
    $query = "
            SELECT 
                id, image, uniq_name,
                priority,
                `name`,
                if(
                            simple_editor=1,
                            CONCAT('<a href=\"?id=', id, '\">', 'Редактировать', '</a>'),
                            text_editor(id, 'maillist_templates', 'html', 'Редактировать')
                    ) AS html
            FROM maillist_templates        
            ORDER BY priority ASC
    ";
    $cmsTable = new cmsShowView($DB, $query);
    $cmsTable->setParam('row_filter', 'row_filter');
    $cmsTable->setParam('excel', false);

    $cmsTable->addColumn('name', '30%');
    $cmsTable->addColumn('uniq_name', '15%');
    $cmsTable->addColumn('html', '30%', 'center', 'Шаблон');
    $cmsTable->addColumn('image', '10%', 'center');

    $TmplContent->set('cms_templates', $cmsTable->display());
    unset($cmsTable);
}
else{
    //редактирование шаблона
    $template = $DB->query_row("select * from maillist_templates where id='$template_id'");
    $TmplContent->set( $template );
    $TmplDesign->iterate('/onload/', null, array('function' => 'integrateCkEditor("content_template", "'.$template_id.'", "maillist_templates", "html", "ru", "'.$template_id.'"); '));
 
}

?>
