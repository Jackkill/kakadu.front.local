<?php

/**
 * Категории рассылок
 * @package Maillist
 * @author Naumenko A.
 * @copyright c-format, 2014
 */

$group_id = globalVar($_GET['group_id'], 0);

if( empty($group_id) ){
    $query = "
            SELECT
                    tb_group.id,
                    CONCAT ('<a href=\"?group_id=', tb_group.id, '\" >', tb_group.name_".LANGUAGE_CURRENT.", '</a>') as name,
                    tb_group.test, tb_group.private, tb_group.priority,
                    count(tb_email.id) as count_emails
            FROM maillist_group AS tb_group	
            LEFT JOIN maillist_group_email_relation AS tb_relation ON tb_relation.group_id=tb_group.id
            LEFT JOIN maillist_emails as tb_email ON tb_email.id=tb_relation.email_id        
            GROUP BY tb_group.id
            ORDER BY tb_group.priority
    ";
    $cmsTable = new cmsShowView($DB, $query);
    $cmsTable->setParam('show_filter', false);
    $cmsTable->setParam('excel', false);
    $cmsTable->addColumn('name', '40%');
    $cmsTable->addColumn('count_emails', '7%', 'center', 'К-ство подписчиков');
    $cmsTable->addColumn('test', '7%', 'center');
    $cmsTable->addColumn('private', '7%', null, 'Скрытая');
    $cmsTable->setColumnParam('test', 'editable', true);
    $cmsTable->setColumnParam('private', 'editable', true);
    echo $cmsTable->display();
    unset($cmsTable);
}
else{
    $group_name = $DB->result("select name_".LANGUAGE_CURRENT." as name FROM maillist_group WHERE id='$group_id'");
    
    $query = "
            SELECT
                    tb_email.id, 
                    tb_email.email,
                    tb_email.name,                    
                    DATE_FORMAT(tb_email.dtime, '%d.%m.%Y %H:%i') as date
            FROM maillist_emails as tb_email	
            LEFT JOIN maillist_group_email_relation AS tb_relation ON tb_relation.email_id=tb_email.id
            LEFT JOIN maillist_group AS tb_group ON tb_group.id=tb_relation.group_id
            WHERE tb_relation.group_id='$group_id'            
            ORDER BY tb_email.dtime ASC
    ";
    $cmsTable = new cmsShowView($DB, $query);    
    $cmsTable->setParam('title', $group_name . ' :: Подписчики категории');
    $cmsTable->setParam('show_filter', false);
    $cmsTable->setParam('show_parent_link', true);
    $cmsTable->setParam('parent_link', "/admin/maillist/groups/?");
    $cmsTable->addColumn('email', '15%');
    $cmsTable->addColumn('name', '15%');    
    $cmsTable->addColumn('date', '15%', 'center', 'Подписан');
    
    echo $cmsTable->display();
    unset($cmsTable);
}