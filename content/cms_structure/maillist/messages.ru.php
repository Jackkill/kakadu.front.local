<?php
/**
 * Список рассылаемых сообщений
 * @package Maillist
 * @subpackage Content_Admin
 * @author Naumenko A.
 * @copyright ltd, c-format 2015
 */

function cms_filter($row) {
	global $DB;
	
	$row['message_size'] = Maillist::getMessageSize($row['id']);
	$row['message_size'] = ($row['message_size'] > MAILLIST_ATTACHMENT_MAX_SIZE) ? 
		'<span class="aicons warning inline">&nbsp;</span> <font color=red>'.ceil($row['message_size']/1000).' Kb'.'</font>': 
		ceil($row['message_size']/1000).' Kb';
        $row['views'] = "<a href=\"/tools/maillist/view.php?id=".$row['id']."\" target=\"_blank\" title=\"Посмотреть\" class=\"aicons views\">&nbsp;</a>";
	/**
	 * Добавляем отправителя к теме сообщения
	 */
	$row['subject'] .= "<br><span class='comment'>".$row['reply_to']."</span>";
	
        
//	$row['_attach'] = ($row['attach'] > 0) ?
//		"<a href='./Attachment/?message_id=$row[id]'><img src=\"/img/maillist/attach_active.png\" border=0 alt=\"Вложения\"></a>":
//		"<a href='./Attachment/?message_id=$row[id]'><img src=\"/img/maillist/attach.png\" border=0 alt=\"Вложения\"></a>";
//	$row['_cron'] = ($row['cron'] > 0) ?
//		"<a href='./Cron/?message_id=$row[id]' class='aicons clock noadctive'>&nbsp;</a>":
//		"<a href='./Cron/?message_id=$row[id]' class='aicons clock'>&nbsp;</a>";
//        
        $row['statistics'] = ($row['statistics']) ? "<a href=\"./stat/?message_id=". $row['id']. "\" class=\"aicons inline statistics\">&nbsp;</a>" : "";
                
        //к-ство подписчиков
        $query = "
                select count(DISTINCT(tb_email.id))
                from maillist_emails as tb_email
                inner join maillist_group_email_relation as tb_relation on tb_relation.email_id=tb_email.id
                inner join maillist_message_group as tb_mail on tb_mail.group_id=tb_relation.group_id
                where tb_mail.message_id='{$row['id']}' and tb_email.block='0'
                
	";
	$row['recipient'] = $DB->result($query);
        
        //Отписавшиися
        $query = "
                select count(DISTINCT(tb_email.id))
                from maillist_emails as tb_email
                inner join maillist_group_email_relation as tb_relation on tb_relation.email_id=tb_email.id
                inner join maillist_message_group as tb_mail on tb_mail.group_id=tb_relation.group_id
                where tb_mail.message_id='{$row['id']}' and tb_email.block='1'
                
	";
	$row['recipient_block'] = $DB->result($query);
        
	return $row;
}

$query = "
	SELECT 
		tb_message.id,
		tb_message.reply_to,
		length( tb_message.html ) as message_size,
                tb_message.subject,                
                CONCAT('<a href=\"./products/?message_id=', tb_message.id, '\">[Редактировать]</a>') AS edit_message,                
		(select count(*) from maillist_task where message_id=tb_message.id) as statistics,		
		'' AS status,
		DATE_FORMAT(tb_message.create_dtime, '".LANGUAGE_DATETIME_SQL."') AS create_dtime
	FROM maillist_message AS tb_message
          WHERE tb_message.is_archive='0'
	ORDER BY tb_message.create_dtime DESC
";
$cmsTable = new cmsShowView($DB, $query, CMS_VIEW, 'maillist_message');
$cmsTable->setParam('prefilter', 'cms_filter');
$cmsTable->addEvent('test', '/action/admin/maillist/send_test', false, true, false, '', '', 'Отправить тестовое сообщение', '');
$cmsTable->addEvent('queue', '/action/admin/maillist/send_queue', false, true, false, '', '', 'Разослать сообщение', 'Произвести рассылку?');
//$cmsTable->addEvent('add', './create/', true, false, false, '/design/cms/img/event/table/new.gif', '/design/cms/img/event/table/new_over.gif', 'Добавить рассылку', null);
$cmsTable->setParam('excel', false);
$cmsTable->setParam('show_parent_link', false);

$cmsTable->addColumn('subject', '25%');
$cmsTable->addColumn('edit_message', '15%', 'center', 'Тело письма');
///$cmsTable->addColumn('content', '20%', 'left', 'Тело письма');

$cmsTable->addColumn('recipient', '7%', 'center', 'Подписчики');
$cmsTable->addColumn('recipient_block', '7%', 'center', 'Блокированные');

//$cmsTable->addColumn('_cron', '7%', 'center', 'Расписание');
$cmsTable->addColumn('message_size', '7%', 'center', 'Объем');
$cmsTable->addColumn('views', '5%', 'center', 'Посмотреть');
echo $cmsTable->display();
unset($cmsTable);


////////Архив
$query = "
	SELECT 
		tb_message.id,
		tb_message.reply_to,
		length( tb_message.html ) as message_size,
                html_editor(tb_message.id, 'maillist_message', 'html', 'Edit mail') AS content,   
		(select count(*) from maillist_task where message_id=tb_message.id) as statistics,		
		CONCAT(
			' (',
			(SELECT COUNT(*) FROM maillist_task_queue WHERE task_id=tb_message.task_id AND delivery='wait'),
			'/<span style=\"color:#00aeef;\">',
			(SELECT COUNT(*) FROM maillist_task_queue WHERE task_id=tb_message.task_id AND delivery='ok'),
			'</span>/<span style=\"color:#ef696c;\">',
			(SELECT COUNT(*) FROM maillist_task_queue WHERE task_id=tb_message.task_id AND delivery='error'),
			'</span>)'
		) AS status,
		tb_message.subject AS subject,
		DATE_FORMAT(tb_message.create_dtime, '".LANGUAGE_DATETIME_SQL."') AS create_dtime
	FROM maillist_message AS tb_message
        WHERE tb_message.is_archive='1'
	ORDER BY tb_message.create_dtime DESC
";
$cmsTable = new cmsShowView($DB, $query, CMS_VIEW, 'maillist_message');
$cmsTable->setParam('prefilter', 'cms_filter');
$cmsTable->setParam('title', 'Архив рассылок');
//$cmsTable->addEvent('test', '/action/admin/maillist/send_test', false, true, false, '', '', 'Отправить тестовое сообщение', '');
//$cmsTable->addEvent('queue', '/action/admin/maillist/send_queue', false, true, false, '', '', 'Разослать сообщение', 'Произвести рассылку?');
//$cmsTable->addEvent('add', './create/', true, false, false, '/design/cms/img/event/table/new.gif', '/design/cms/img/event/table/new_over.gif', 'Добавить рассылку', null);
$cmsTable->setParam('add', false);
$cmsTable->setParam('edit', false);
//$cmsTable->setParam('excel', false);
$cmsTable->setParam('show_parent_link', false);

$cmsTable->addColumn('subject', '25%');
//$cmsTable->addColumn('content', '20%', 'left', 'Тело письма');

$cmsTable->addColumn('recipient', '7%', 'center', 'Подписчики');
$cmsTable->addColumn('recipient_block', '7%', 'center', 'Блокированные');

//$cmsTable->addColumn('_cron', '7%', 'center', 'Расписание');
$cmsTable->addColumn('message_size', '7%', 'center', 'Объем');
$cmsTable->addColumn('status', '7%', 'left', 'Статус');
$cmsTable->addColumn('statistics', '7%', 'center', 'Статистика');
$cmsTable->addColumn('views', '5%', 'center', 'Посмотреть');
echo $cmsTable->display();
unset($cmsTable);
?>