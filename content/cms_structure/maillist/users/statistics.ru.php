<?php

/**
 * Статистика по подписчику
 * @package Maillist
 * @author Naumenko A.
 * @copyright c-format, 2014
 */

$email_id = globalVar($_GET['user_id'], 0);

$data = $DB->query_row("select email, name FROM maillist_emails where id='$email_id'");

if(!empty($data)){
$query = "
	SELECT 
                tb_task.id,
		tb_task.delivery,               
                CONCAT('<small>',
                    DAY(tb_task.tstamp), ' ', (CASE MONTH(tb_task.tstamp) ".LANGUAGE_MONTH_GEN_SQL." END), ' ', YEAR(tb_task.tstamp), '<br/>',
                    DATE_FORMAT(tb_task.tstamp, '%H:%i'), '</small>'    
                ) as date,
                IF(tb_task.open_tstamp is not null,
                   CONCAT('<small>',
                        DAY(tb_task.open_tstamp), ' ', (CASE MONTH(tb_task.open_tstamp) ".LANGUAGE_MONTH_GEN_SQL." END), ' ', YEAR(tb_task.open_tstamp), '<br/>',
                        DATE_FORMAT(tb_task.open_tstamp, '%H:%i'), '</small>'    
                   ), tb_task.open_tstamp
                )  as open_date,
                tb_task.open_mail,
                tb_message.subject
	FROM maillist_task_queue AS tb_task
        INNER JOIN maillist_message as tb_message on tb_message.id=tb_task.message_id
	WHERE tb_task.email='".$data['email']."'
	ORDER BY tb_task.tstamp DESC
    ";
    $cmsTable = new cmsShowView($DB, $query);
    //$cmsTable->setParam('prefilter', 'cms_filter');
    $cmsTable->setParam('show_parent_link', true);
    $cmsTable->setParam('parent_link', '/admin/maillist/users/?');
    $cmsTable->setParam('title', 'Статистика по подписчику '. $data['name'].' &lt;'.$data['email'].'&gt; ');
    $cmsTable->setParam('add', false);
    $cmsTable->setParam('edit', false);
    $cmsTable->setParam('delete', false);
     $cmsTable->setParam ('excel', false);

    //$cmsTable->addColumn('login', '30%', 'left', 'Пользователь');
    
    $cmsTable->addColumn('date', '10%', 'center', "Дата отправки");
    $cmsTable->addColumn('subject', '30%', 'left', "Тема");    
    $cmsTable->addColumn('delivery', '10%', 'center', "Доставлено");    
    $cmsTable->addColumn('open_mail', '10%', 'center', "Открыто"); 
    $cmsTable->addColumn('open_date', '10%', 'center', "Дата открытия");
}    
    echo $cmsTable->display();
    unset($cmsTable);