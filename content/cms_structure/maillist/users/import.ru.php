<?php

/**
 * Импорт подписчиков
 * @package Maillist
 * @author Naumenko A.
 * @copyright c-format, 2014
 */

//группы
 $query = "
            SELECT
                    tb_group.id,
                    tb_group.name_".LANGUAGE_CURRENT." as name
            FROM maillist_group AS tb_group	            
            WHERE tb_group.test='0'
            ORDER BY tb_group.priority
    ";
 $groups = $DB->query($query);
 $TmplContent->iterateArray("/groups/", null, $groups);
 