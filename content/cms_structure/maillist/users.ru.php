<?php

/**
 * Список подписчиков
 * @package Maillist
 * @author Naumenko A.
 * @copyright c-format, 2014
 */

$query = " INSERT INTO maillist_emails "
            . " (`email`, `name`)
                (select tb_user.`email`, tb_user.`name` from `auth_user` tb_user
                 left join maillist_emails as tb_email on tb_email.email=tb_user.email WHERE tb_email.id is null ) 
            ";
$DB->query($query);


$type = globalVar($_GET['type'], '');

$where = '';
if($type == "nolist"){
    $where = " WHERE tb_relation.group_id is null and tb_email.block='0'";
}
elseif($type == "block"){
    $where = " WHERE tb_email.block = '1'";
}
$TmplContent->set('type', $type);

function cms_filter($row) {
	//$row['login'] = "<a href='./Category/?user_id=$row[id]'>$row[login]</a>";
	return $row;
}
$query = "
	SELECT
		tb_email.id, 
		IF( 
                    tb_queue.id is null, tb_email.email,
                    CONCAT('<a href=\"./statistics/?user_id=', tb_email.id, '\" title=\"Посмотреть статистику\">', tb_email.email, ' <i class=\"aicons inline statistics\">&nbsp;</i></a>') 
                )  as email,  
		tb_email.name,
		tb_email.message,
                DATE_FORMAT(tb_email.dtime, '%d.%m.%Y %H:%i') as date,
		IF(tb_email.block = '1', '<span style=\"color:red;\">Заблокирован</span>',
                    IFNULL(
			CONCAT('<span >', GROUP_CONCAT(DISTINCT tb_group.name_".LANGUAGE_CURRENT." ORDER BY tb_group.name_".LANGUAGE_CURRENT." SEPARATOR ', '), '</span>'),
			'<span style=\"color:grey;\">Не подписан</span>'
                    )    
		) AS group_name
	FROM maillist_emails as tb_email	
	LEFT JOIN maillist_group_email_relation AS tb_relation ON tb_relation.email_id=tb_email.id
	LEFT JOIN maillist_group AS tb_group ON tb_group.id=tb_relation.group_id
	LEFT JOIN maillist_task_queue AS tb_queue ON tb_queue.email=tb_email.email
        $where
	GROUP BY tb_email.id
	ORDER BY tb_email.dtime ASC
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('prefilter', 'cms_filter');
$cmsTable->setParam('title', 'Подписчики');

$cmsTable->addColumn('email', '15%');
$cmsTable->addColumn('name', '15%', 'center');
$cmsTable->addColumn('group_name', '40%', null, 'Рассылки');
if($type == "block"){
    $cmsTable->addColumn('message', '15%', 'left', 'Причина блокировки');
    $cmsTable->addColumn('date', '15%', 'center', 'Блокирован');
}
else{
    $cmsTable->addColumn('date', '15%', 'center', 'Подписан');
}    
$TmplContent->set('cms_table', $cmsTable->display());
unset($cmsTable);
