<?php

/**
 * Документация по классам програмы
 * @package Pilot
 * @subpackage SDK
 * @author Rudenko Ilya <rudenko@delta-x.ua>
 * @copyright Delta-X, ltd. 2010
 */


function funcfilter($row) {
	$row['name'] = "<a href=\"/Admin/CMS/Development/Doc/Function/?clsid=".$row['id']."\">".$row['name']."</a>";	
	return $row;
}


$query = "
	select *
	from sdk_doc_class
	order by subpackage asc
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('row_filter', 'funcfilter');
$cmsTable->setParam('add', false);
$cmsTable->setParam('delete', false);
$cmsTable->setParam('edit', false);
$cmsTable->setParam('excel', true);
$cmsTable->setParam('title', 'Список класcов системы');
$cmsTable->setParam('subtitle', 'subpackage');

$cmsTable->addColumn('name', '10%');
$cmsTable->addColumn('description', '30%');
//$cmsTable->addColumn('package', '5%', 'left', 'Проект');
$cmsTable->addColumn('subpackage', '5%', 'left', 'Модуль');
$cmsTable->addColumn('version', '5%', 'center', 'Версия');
$cmsTable->addColumn('author', '10%');
$cmsTable->addColumn('copyright', '10%', 'left', 'Копирайт');

$TmplContent->set('cms_table', $cmsTable->display());
unset($cmsTable);


/**
* Вывод событий
*/
function cms_prefilter($row) {
	
        $path = "/admin/".  strtolower($row['module_name'])."/$row[name].act.php";
        
	if (!is_file( ACTIONS_ROOT.$path )) {
            $row['name'] = '<font color="gray">'.$row['name'].'</font>';
	}
        else{
            $row['name'] = "<span title=\"$path\"><b>".$row['name']."</b></span>";
        }    
	return $row;
}
$query = "
	SELECT 
		tb_event.id, 
		tb_event.name,
                tb_module.name as module_name,
		tb_event.description_".LANGUAGE_CURRENT." AS description
	FROM cms_event AS tb_event
	LEFT JOIN cms_module as tb_module on tb_module.id=tb_event.module_id
	ORDER BY tb_module.name ASC
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('prefilter', 'cms_prefilter');
$cmsTable->setParam('add', false);
$cmsTable->setParam('delete', false);
$cmsTable->setParam('edit', false);
$cmsTable->setParam('show_filter', false);
$cmsTable->setParam('excel', true);
$cmsTable->setParam('subtitle', 'module_name');
$cmsTable->addColumn('name', '20%');
$cmsTable->addColumn('description', '60%');
$TmplContent->set('cms_actions', $cmsTable->display());

$TmplDesign->iterate('/onload/', null, array('function' => '$("#idTabs").tabs();'));