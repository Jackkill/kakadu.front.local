<?php
/**
 * Список аргументов даного классу
 * @package Pilot
 * @subpackage SDK
 * @author Rudenko Ilya <rudenko@delta-x.ua>
 * @copyright Delta-X, ltd. 2010
 */

$function_id = globalVar($_GET['fnkid'], 0);

$functionname = $DB->result("select name from sdk_doc_function where id = '".$function_id."'");
$classid = $DB->result("select class_id from sdk_doc_function where id = '".$function_id."'");

function argfilt($row) {
	$row['name'] = "<b>".$row['name']."</b>";
	return $row;
}


$query = "
	select *
		from sdk_doc_argument
	where function_id = '".$function_id."'	
	order by name asc
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('add', false);
$cmsTable->setParam('row_filter', 'argfilt');
$cmsTable->setParam('delete', false);
$cmsTable->setParam('edit', false);

$cmsTable->setParam('title', "Список аргументов метода: ".$functionname."");
$cmsTable->setParam('parent_link', "/Admin/CMS/Development/Doc/Function/?clsid=".$classid."");
$cmsTable->setParam('show_parent_link', true);
$cmsTable->addColumn('name', '30%');
$cmsTable->addColumn('type', '40%', 'left', 'Тип');
$cmsTable->addColumn('description', '30%', 'left', 'Описание');
//$cmsTable->addColumn('scope', '15%', 'left', 'Видимость');
echo $cmsTable->display();
unset($cmsTable);