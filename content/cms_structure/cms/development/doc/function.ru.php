<?php
/**
 * Документация по методам классов системы
 * @package Pilot
 * @subpackage SDK
 * @author Rudenko Ilya <rudenko@delta-x.ua>
 * @copyright Delta-X, ltd. 2010
 */

$class_id = globalVar($_GET['clsid'], 0);

$classname = $DB->result("select name from sdk_doc_class where id = '".$class_id."'");

function argfilter($row) {
        global $DB;
        
        $arg = $DB->fetch_column("
                select 
                    IF(type <> 'undefined', CONCAT('<small style=\"color:grey;\">', type, '</small> ', name), name) as name
                    from sdk_doc_argument
                where function_id = '{$row['id']}'	order by id
                ");
        
	$scope = str_replace(',', ' ', $row['scope']);
	
        if(count($arg)>0){
            $row['name'] = "<a href=\"/admin/cms/development/doc/function/arg/?fnkid=".$row['id']."\"> $scope"
                . " <span  style=\"color:black\"><b>".$row['name']."(</b>".implode(", ", $arg)."<b > )</b></span></a>";	
        }
        else {
            $row['name'] = $scope . " <b>".$row['name']."()</b>";	
        }
	return $row;
	
	
}


$query = "
	select *
		from sdk_doc_function
	where class_id = '".$class_id."'	
	order by name asc
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('row_filter', 'argfilter');
$cmsTable->setParam('add', false);
$cmsTable->setParam('delete', false);
$cmsTable->setParam('edit', false);
$cmsTable->setParam('excel', true);
$cmsTable->setParam('title', "Список методов класса: ".$classname."");
$cmsTable->setParam('parent_link', "../?");
$cmsTable->setParam('show_parent_link', true);
$cmsTable->addColumn('name', '40%');
$cmsTable->addColumn('description', '50%', 'left', 'Описание');
$cmsTable->addColumn('return', '10%', 'left', 'Возврат');
//$cmsTable->addColumn('scope', '15%', 'left', 'Видимость');
echo $cmsTable->display();
unset($cmsTable);