<?php
/**
 * Вывод списка сообщений по модулям
 * @package CMS
 * @subpackage Content_Admin
 * @author Naumenko A.
 * @copyright (c) 2014, c-format
 */

$languases = getLanguageList();

$query = "
	SELECT 
		tb_message.*
	FROM cms_language_message AS tb_message
        WHERE (tb_message.name_".LANGUAGE_SITE_DEFAULT." = '' or tb_message.name_".LANGUAGE_SITE_DEFAULT." IS NULL )
	ORDER BY tb_message.module, tb_message.uniq_name ASC
";
$count = $DB->query($query);

if(!empty($count)){
    $cmsTable = new cmsShowView($DB, $query);
    $cmsTable->setParam('title', 'Не заполненныне языковые константы');
    $cmsTable->setParam('subtitle', 'module');
    $cmsTable->addColumn('uniq_name', '20%');
    
    foreach($languases as $lang){
        $cmsTable->addColumn('name_'.$lang, '30%', 'left');
    }    
    
    $TmplContent->set('cms_table_empty', $cmsTable->display());
    unset($cmsTable);
}


$type = globalVar($_GET['type'], '');

$types = $DB->query("SELECT 
		DISTINCT(tb_message.module) as type
	FROM cms_language_message AS tb_message	        
	ORDER BY tb_message.module");
$TmplContent->iterateArray('/types/', null, $types);
$TmplContent->setGlobal('type', $type);


$query = "
	SELECT 
		tb_message.*
	FROM cms_language_message AS tb_message	
        WHERE (tb_message.name_".LANGUAGE_CURRENT." <> '' and tb_message.name_".LANGUAGE_CURRENT." IS NOT NULL )
                ".  where_clause('tb_message.module', $type)."
	ORDER BY tb_message.module, tb_message.uniq_name
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('subtitle', 'module');
$cmsTable->addColumn('uniq_name', '20%');

$languases = getLanguageList();
foreach($languases as $lang){
    $cmsTable->addColumn('name_'.$lang, '30%', 'left');
}    

$TmplContent->set('cms_table', $cmsTable->display());
unset($cmsTable);

