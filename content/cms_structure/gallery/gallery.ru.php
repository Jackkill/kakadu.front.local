<?php
/**
 * Фотогалерея. 
 * @package Gallery
 * @subpackage DeltaCMS
 * @author Naumenko A.
 * @copyright c-format, 2015
 */
$group_id = globalVar($_GET['group_id'], 0);

function cms_filter($row) {
	$row['name'] = "<a href='?group_id=$row[id]'>$row[name]</a>";
	return $row;
}

$query = "
	SELECT id, name_".LANGUAGE_CURRENT." AS name, 
             html_editor(id, 'gallery_group', 'content_".LANGUAGE_SITE_DEFAULT."', 'Описание') as content,
            priority, active
	FROM gallery_group
	WHERE group_id='$group_id'
	ORDER BY priority ASC
";
$cmsTable = new cmsShowView($DB, $query, 20);
$cmsTable->setParam('prefilter', 'cms_filter');
$cmsTable->addColumn('name', '60%');
$cmsTable->addColumn('content', '20%', "center");
$cmsTable->addColumn('active', '10%', "center");
$cmsTable->setColumnParam('active', 'editable', true);
echo $cmsTable->display();
unset($cmsTable);

/**
 * Class uiGallery
 */

$title = $DB->result("select name_".LANGUAGE_CURRENT." from gallery_group where id='$group_id'");
$title = (!empty($title)) ? 'Фотогаллерея: "'. $title. '"' : 'Фотогаллерея';

$gallery = new uiGallery('gallery_group', $group_id);
$gallery->setParam('field', 'photo');
$gallery->setParam('headline', $title);
echo $gallery->display();
unset($gallery);


?>