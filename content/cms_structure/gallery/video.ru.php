<?php
/**
 * Видео
 * @package Gallery
 * @subpackage DeltaCMS
 * @author Naumenko A.
 * @copyright c-format, 2015
 */

$group_id = globalVar($_GET['group_id'], 0);

if(empty($group_id)) {
    header("Location: /admin/gallery/video/?group_id=1");
    exit;
}

function cms_prefilter2($row) {        
        $row['count_video'] = "(" . $row['count_video'] . ")";
        return $row;
    }     
$query = "
        SELECT
                tb_group.id, 
                CONCAT('<a href=\'./?group_id=', tb_group.id, '\'>', tb_group.name_".LANGUAGE_CURRENT.", '</a>') AS name,
                html_editor(tb_group.id, 'gallery_video_group', 'content_".LANGUAGE_CURRENT."', 'Описание') as content,
                (
                        select count(distinct t_message.id) 
                        from gallery_video_group_relation as t_relation
                        inner join gallery_video as t_message on t_message.group_id=t_relation.id
                        where t_relation.parent=tb_group.id
                ) as count_video,
                tb_group.active, tb_group.priority
        FROM  gallery_video_group AS tb_group
        WHERE tb_group.group_id = '$group_id'           
        ORDER BY tb_group.priority
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('prefilter', 'cms_prefilter2');   
if ($group_id == 1) $cmsTable->setParam('show_parent_link', false);
$cmsTable->addColumn('name', '30%');
//$cmsTable->addColumn('img', '20%');    
$cmsTable->addColumn('content', '15%');    
$cmsTable->addColumn('count_video', '15%', 'center', 'К-ство видеофайлов');    
$cmsTable->addColumn('active', '5%');    
$cmsTable->setColumnParam('active', 'editable', true);  
echo  $cmsTable->display();
unset($cmsTable);

if(!empty($group_id)){
    
    function cms_prefilter($row) {        
        $file = Uploads::getIsFile('gallery_video', 'img', $row['id'], $row['img']); 
        
        $row['img']  = Uploads::fancyImage($file, $row['title'], 200);
        $row['code'] = Uploads::fancyVideo($row['code'] . ' <i class="aicons inline views"></i>', $row['code'], $row['type']);           
     
        if ($row['announcement']) $row['name'] .= "<br/><span class='comment'>{$row['announcement']}</span>";
        return $row;
    }        
   
    
    $query = "
            SELECT
                    id, 
                    name_".LANGUAGE_CURRENT." AS title,
                    code, type, img,
                    html_editor(id, 'gallery_video', 'content_".LANGUAGE_CURRENT."', name_".LANGUAGE_CURRENT.") as name,
                    announcement_".LANGUAGE_CURRENT." as   announcement,  
                    active,
                    priority
            FROM gallery_video
            WHERE group_table_name = 'gallery_video_group' and group_id = '$group_id'
            ORDER BY priority
    ";
    $cmsTable = new cmsShowView($DB, $query);
    $cmsTable->setParam('prefilter', 'cms_prefilter');      
    $cmsTable->setParam('show_parent_link', false);      
    $cmsTable->setParam('show_path', false);      
    $cmsTable->addColumn('name', '20%');
    $cmsTable->addColumn('code', '15%');
    $cmsTable->addColumn('type', '10%');
    $cmsTable->addColumn('img', '20%');
    $cmsTable->addColumn('active', '5%');
    $cmsTable->setColumnParam('active', 'editable', true);
    echo $cmsTable->display();
    unset($cmsTable);
}    
   
    