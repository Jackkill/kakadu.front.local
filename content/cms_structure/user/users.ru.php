<?php
/**
 * Список зарегистрированных на сайте пользователей
 * @package User
 * @subpackage Content_Admin
 * @author Rudenko Ilya <rudenko@delta-x.com.ua>
 * @copyright Delta-X, ltd. 2005
 */

$clients_show = globalVar($_GET['client'], 0);


$admin_groups = $DB->fetch_column("select id from auth_group where is_admin=1");
$title = ($clients_show == 1) ? "Пользователи сайта" : "Администраторы сайта";
$where = ($clients_show == 1) 
		? " and tb_user.group_id not in (".implode(',', $admin_groups).") "
		: " and tb_user.group_id in (".implode(',', $admin_groups).")";
$selected = ($clients_show == 1) ? "user" : "admin" ;


$TmplContent->set('selected', $selected);

function row_filter($row) {
	global $DB;
	
	if(!empty($row['group_name'])){
           $row['user_name'] .= "<br/><span style='font-size:10px;color:#666;'><b>Роль: </b>{$row['group_name']}</span>";
        }
            
	$row['switch'] = "<a title='Перейти в аккаунт этого пользователя' class='aicons switch' href='/".LANGUAGE_URL."action/admin/auth/switch_user/?switch_id=". $row['id'] . "&_return_path=".urlencode('/')."'>&nbsp;</a>";
	return $row;
}

$can_see_developers = "";
IF ( !IS_DEVELOPER ) $can_see_developers = " and tb_user.group_id<>'5'";

$query = "
	SELECT 
		tb_user.*,
		tb_user.login as login,		
		CONCAT(
			tb_user.name, ' (', tb_user.email, ')',
			'<br><span class=\"comment\">Группа: ', ifnull(tb_group.name, '<span style=\"color: #0E5FD8;\">не назначены</span>'), '<br>','</span>'
		) AS name,
		tb_user.name as user_name,
                tb_group.name as group_name,
		tb_site.url as site_id
	FROM auth_user AS tb_user
	LEFT JOIN auth_group AS tb_group ON tb_user.group_id = tb_group.id	
	LEFT JOIN site_structure_site AS tb_site ON tb_site.id=tb_user.site_id
        WHERE 1 $can_see_developers $where
	GROUP BY tb_user.id
	ORDER BY login
";
//x($query);
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('row_filter', 'row_filter');
$cmsTable->setParam('title', $title);

$cmsTable->addColumn('login', '30%');
$cmsTable->setColumnParam('login', 'order', 'login');
$cmsTable->addColumn('user_name', '20%', 'left', 'ФИО');
//if ($clients_show) $cmsTable->addColumn('discount_value', '10%', 'center', 'Скидка, %');
$cmsTable->setColumnParam('user_name', 'order', 'name');
//$cmsTable->addColumn('site_id', '5%');
$cmsTable->addColumn('checked', '5%');
$cmsTable->setColumnParam('checked', 'editable', true);
$cmsTable->setColumnParam('checked', 'editable', true);
//$cmsTable->setColumnParam('discount_value', 'editable', true);
$cmsTable->addColumn('switch', '5%', 'center', 'Вход');

$TmplContent->set('cms_table', $cmsTable->display());
unset($cmsTable);


?>