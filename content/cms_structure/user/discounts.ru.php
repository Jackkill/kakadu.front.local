<?php
/**
 * Скидочные группы
 * @package USER
 * @subpackage Content_Admin
 * @author Naumenko A.
 * @copyright c-format, 2013
 */

function cms_prefilter($row) {

    $desc = '';
    if($row['price']){ $desc .= " Цена от " . $row['price'] . " грн.";}
    
    $row['description'] = $desc . $row['description'];
    return $row;
}

$query = "
	SELECT  id,
		name,
		description,
                discount,  price,  
                IF(is_accumulative=1, 'Да',  'Нет') as is_accumulative,
                IF(is_registration=1, 'Да',  'Нет') as is_registration,                
                active
	FROM auth_discount_group 
	ORDER BY is_registration, price ASC
";
$cmsTable = new cmsShowView($DB, $query, 200);
$cmsTable->setParam('prefilter', 'cms_prefilter');
$cmsTable->addColumn('name', '20%');
$cmsTable->addColumn('discount', '10%', 'center');
$cmsTable->addColumn('is_accumulative', '10%', 'center');
$cmsTable->addColumn('is_registration', '10%', 'center');
$cmsTable->addColumn('description', '40%');
$cmsTable->addColumn('active', '5%');
$cmsTable->setColumnParam('active', 'editable', true);

$table_info = $cmsTable->getTableInfo();
echo $cmsTable->display();


?>