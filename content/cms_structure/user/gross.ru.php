<?php
/**
 * Скидочные группы
 * @package USER
 * @subpackage Content_Admin
 * @author Naumenko A.
 * @copyright c-format, 2013
 */

function cms_prefilter($row) {
    
    return $row;
}

$query = "
	SELECT  tb_group.id,
		CONCAT('<a href=\"./relation/?group_id=', tb_group.id, '\" title=\"Проставить скидку\">', tb_group.name, '</a>') as name,
		tb_group.description,                        
                tb_group.active,
                CONCAT('(', COUNT(tb_user.id), ')') as count_user
	FROM auth_gross_group as tb_group
        LEFT JOIN auth_user AS tb_user ON tb_user.gross_id = tb_group.id
        GROUP BY tb_group.id
	ORDER BY tb_group.name ASC
";
$cmsTable = new cmsShowView($DB, $query, 200);
//$cmsTable->setParam('prefilter', 'cms_prefilter');
$cmsTable->addColumn('name', '20%');
$cmsTable->addColumn('count_user', '10%', 'center', 'К-ство чел.');
$cmsTable->addColumn('description', '30%', 'center');
$cmsTable->addColumn('active', '5%');
$cmsTable->setColumnParam('active', 'editable', true);

$table_info = $cmsTable->getTableInfo();
echo $cmsTable->display();


?>