<?php
/**
 * Группы пользователей
 * @package CMS
 * @subpackage Content_Admin
 * @author Rudenko Ilya <rudenko@delta-x.com.ua>
 * @copyright Delta-X, ltd. 2005
 */

function remove_wrong_tables($type) {
    global $DB;
    $query = "
        select tb_update.*
        from auth_action_table_$type as tb_update
        inner join cms_table as tb_table on tb_table.id=tb_update.table_id
        inner join auth_action as tb_action on tb_action.id=tb_update.action_id
        where tb_table.module_id != tb_action.module_id
    ";
    $data = $DB->query($query);
    reset($data);
    while (list(,$row) = each($data)) {
        $query = "delete from auth_action_table_$type where table_id='$row[table_id]' and action_id='$row[action_id]'";
        $DB->delete($query);
    }
}

$group_id = globalVar($_GET['group_id'], 0);
$TmplContent->set('group_id', $group_id);

// Определяем название группы
$query = "select name from auth_group where id='$group_id'";
$group = $DB->result($query);
$TmplContent->set('group', $group);

//module_id
$module_id = globalVar($_GET['module_id'], 0);
$TmplContent->set('module_id', $module_id);

if ( !$module_id )
{
    // Если таблицу прописать сначала в одном модуле, а потом изменить её принадлежность модулю, 
    // то она пропадет из списка ошибок, но и во втором модуле привязана не будет,
    // надо убирать такие связи
    remove_wrong_tables('select');
    remove_wrong_tables('update');

    //Выбираем все экшены пользователя
    $actions = $DB->fetch_column("SELECT `action_id` FROM `auth_group_action` WHERE `group_id`='{$group_id}'");
    //$actions = $DB->fetch_column("SELECT `action_id` FROM `auth_group_action` WHERE `action_id` NOT IN ('" . implode("','",$actions) . "') GROUP BY action_id");
    if ( !count($actions) ) $actions[] = 0;
    
    /*$query ="                    select tb_structure.*,tb_view.*
                    from cms_structure as tb_structure
                    left join auth_action_view as tb_view on tb_view.structure_id=tb_structure.id and tb_view.action_id in ('" . implode("','",$actions) . "')
                    left join auth_action as tb_action on tb_action.id=tb_view.action_id and tb_structure.module_id=tb_action.module_id
                        
                            where tb_structure.module_id='50' and tb_action.id is null
                    /*where tb_action.id is null*///";
    //x( $query );
    //x( $DB->query($query) );
    
    //x_debug( $actions );
    
    $query = "
            CREATE TEMPORARY TABLE tmp_error
            (
                    /* Определяем модули, в которых есть непроставленные права доступа к просмотру таблиц */
                    SELECT tb_table.module_id
                    FROM cms_table tb_table
                    LEFT JOIN auth_action_table_select tb_relation ON (tb_table.id = tb_relation.table_id 
                        AND tb_relation.action_id IN ('" . implode("','",$actions) . "') )
                    WHERE 
                            tb_table.is_disabled = 0
                            AND tb_relation.action_id is null
                            and tb_table._table_type not in ('function', 'procedure', 'view')
                    GROUP BY tb_table.module_id
            ) UNION (
                    /* Определяем модули, в которых есть непроставленные права доступа к изменению таблиц */
                    SELECT tb_table.module_id
                    FROM cms_table tb_table
                    LEFT JOIN auth_action_table_update tb_relation ON (tb_table.id = tb_relation.table_id
                        AND tb_relation.action_id IN ('" . implode("','",$actions) . "') )
                    WHERE 
                            tb_table.is_disabled = 0
                            AND tb_relation.action_id is null
                            and tb_table._table_type not in ('function', 'procedure', 'view')
                    GROUP BY tb_table.module_id
            ) UNION (
                    /* Модули, в которых есть непривязанные события */
                    SELECT tb_event.module_id
                    FROM cms_event tb_event
                    LEFT JOIN auth_action_event AS tb_relation ON (tb_event.id = tb_relation.event_id
                        AND tb_relation.action_id IN ('" . implode("','",$actions) . "') )
                    WHERE tb_relation.event_id is null
                    GROUP BY tb_event.module_id
            ) UNION (
                    /* Модули, в которых не привязаны разделы админ интерфейса */
                    select distinct tb_structure.module_id
                    from cms_structure as tb_structure
                    left join auth_action_view as tb_view on tb_view.structure_id=tb_structure.id
                        and tb_view.action_id in ('" . implode("','",$actions) . "')
                    left join auth_action as tb_action on tb_action.id=tb_view.action_id and tb_structure.module_id=tb_action.module_id
                    where tb_action.id is null
            )
    ";
    $DB->insert($query);
    
    function cms_filter($row) {
            global $group_id;
            if ( $row["error"] ) $row['name'] = "<a style='color:red;font-weight:bold' href='./?module_id=" . $row['id'] . "&group_id=" . $group_id . "'>" . $row["name"] ."</a>";
            else $row['name'] = "<a href='./?module_id=" . $row['id'] . "&group_id=$group_id'>" . $row["name"] ."</a>";
            return $row;
    }

    $query = "
            select 
                    tb_module.id,
                    tb_module.name,
                    tmp_error.module_id AS module_id,
                    IF(tmp_error.module_id IS NULL, 0, 1) AS error,
                    tb_module.description_".LANGUAGE_CURRENT." AS description,
                    (select if(count(*)=0, '-', count(*)) from auth_action where module_id=tb_module.id) as count
            from cms_module as tb_module
            left join tmp_error on tb_module.id=tmp_error.module_id
            group by tb_module.id
            order by tb_module.name asc
    ";
    $cmsTable = new cmsShowView($DB, $query, 200);
    
    $path = array();
    $path[0]["name"] = "Роли";
    $path[0]["url"] = "./../";
    
    $table_name =  cmsTable::getInfoByAlias( $DB->db_alias, "cms_module" );
    
    $cmsTable->setParam('prefilter', 'cms_filter');
    $cmsTable->setParam('add', false);
    $cmsTable->setParam('delete', false);
    $cmsTable->setParam('edit', false);
    $cmsTable->setParam('show_filter', false);
    $cmsTable->setParam('show_parent_link', true);
    $cmsTable->setParam('parent_link', '/admin/user/groups/?');
    $cmsTable->setParam('path', $path);
    $cmsTable->setParam('path_current', $table_name["title"]);
    $cmsTable->addColumn('name', '30%', 'center', 'Модуль');
    $cmsTable->addColumn('description', '60%', 'center', 'Описание');
    echo $cmsTable->display();
    unset($cmsTable);
}
else {
    $action_id = $DB->result( "
        SELECT `id` FROM auth_action
        INNER JOIN `auth_group_action` ON `auth_group_action`.`action_id`=`auth_action`.`id` AND `auth_group_action`.`group_id`='{$group_id}'
        WHERE auth_action.module_id='$module_id'
    " );
    if ( !$action_id ) $action_id = 0;
    
    // Таблицы, к которым есть доступ
    $checked_update = $DB->fetch_column("select table_id from auth_action_table_update where action_id='$action_id'", 'table_id', 'table_id');
    $checked_select = $DB->fetch_column("select table_id from auth_action_table_select where action_id='$action_id'", 'table_id', 'table_id');


    // Определяем перечень таблиц
    $data = $DB->query("
            select
                    tb_table.id,
                    concat(tb_table.title_".LANGUAGE_CURRENT.", ' [', tb_table.name, ']') as name,
                    tb_db.alias as db_alias,
                    group_concat(distinct if(tb_select_action.id='$action_id', concat('<font color=green>', tb_select_action.title_".LANGUAGE_CURRENT.", '</font>'), tb_select_action.title_".LANGUAGE_CURRENT.")  order by tb_select_action.title_".LANGUAGE_CURRENT." separator ', ') as select_actions,
                    group_concat(distinct if(tb_update_action.id='$action_id', concat('<font color=green>', tb_update_action.title_".LANGUAGE_CURRENT.", '</font>'), tb_update_action.title_".LANGUAGE_CURRENT.") order by tb_update_action.title_".LANGUAGE_CURRENT." separator ', ') as update_actions
            from cms_table as tb_table
            inner join cms_db as tb_db on tb_db.id=tb_table.db_id
            left join auth_action_table_select as tb_select on tb_select.table_id=tb_table.id
            left join auth_action as tb_select_action on tb_select_action.id=tb_select.action_id
            left join auth_action_table_update as tb_update on tb_update.table_id=tb_table.id
            left join auth_action as tb_update_action on tb_update_action.id=tb_update.action_id
            where tb_table.module_id='$module_id'
                    and tb_table.is_disabled=0
                    and tb_table._table_type not in ('procedure', 'function')
            group by tb_table.id
            order by tb_db.alias, tb_table.name
    ");
    
    
    $TmplContent->set('show_change', $DB->rows);
    $prev_db = '';

    reset($data); 
    while (list(,$row) = each($data)) {
            $row['db_name'] = db_config_constant("name", $row['db_alias']); 

            if ($prev_db == $row['db_name']) {
                    $row['db_name'] = '';
            } else {
                    $prev_db = $row['db_name'];
            }

            $row['checked_select'] = (isset($checked_select[ $row['id'] ])) ? 'checked' : '';
            $row['checked_update'] = (isset($checked_update[ $row['id'] ])) ? 'checked' : '';

            if (empty($row['select_actions']) || empty($row['update_actions'])) {
                    $row['name'] = "<font color=red>$row[name]</font>";
            }

            $TmplContent->iterate('/table/', null, $row);
    }
    
    // События к которым есть доступ
    $checked = $DB->fetch_column("select event_id from auth_action_event where action_id='$action_id'", 'event_id', 'event_id');

    // Определяем перечень событий
    $query = "
            select
                    tb_event.id,
                    concat(tb_event.description_".LANGUAGE_CURRENT.", ' [', tb_event.name, ']') as name,
                    group_concat(distinct if(tb_action.id='$action_id', concat('<font color=green>', tb_action.title_".LANGUAGE_CURRENT.", '</font>'), tb_action.title_".LANGUAGE_CURRENT.") order by tb_action.title_".LANGUAGE_CURRENT." separator ', ') as actions
            from cms_event as tb_event
            left join auth_action_event as tb_relation on tb_event.id=tb_relation.event_id
            left join auth_action as tb_action on tb_action.id=tb_relation.action_id
            where tb_event.module_id = '$module_id'
            group by tb_event.id
            order by tb_event.name
    ";
    $data = $DB->query($query);
    $TmplContent->set('show_event', $DB->rows);
    reset($data); 
    while (list(,$row) = each($data)) {
            $row['checked'] = (isset($checked[ $row['id'] ])) ? 'checked' : '';
    //	if (!isset($checked[ $row['id'] ])) {
            if (empty($row['actions'])) {
                    $row['name'] = "<font color=red>$row[name]</font>";
            }
            $TmplContent->iterate('/event/', null, $row);
    }



    $checked = $DB->fetch_column("select structure_id, structure_id as id from auth_action_view where action_id='$action_id'");

    // Определяем перечень разделов в админ части
    $query = "
            select 
                    tb_structure.url,
                    tb_structure.id,
                    tb_structure.id as real_id,
                    tb_structure.structure_id as parent,
                    tb_view.action_id,
                    tb_structure.name_".LANGUAGE_CURRENT." as name,
                    group_concat(distinct if(tb_action.id='$action_id', concat('<font color=green>', tb_action.title_".LANGUAGE_CURRENT.", '</font>'), tb_action.title_".LANGUAGE_CURRENT.") order by tb_action.title_".LANGUAGE_CURRENT." separator ', ') as actions
            from cms_structure as tb_structure
            left join auth_action_view as tb_view on tb_view.structure_id=tb_structure.id
            left join auth_action as tb_action on tb_action.id=tb_view.action_id
            where tb_structure.module_id='$module_id'
            group by tb_structure.id
    ";
    $data = $DB->query($query, 'id');
    reset($data);
    while(list($index,$row) = each($data)) {
            $state = (isset($checked[ $row['id'] ])) ? 'checked' : '';
            $row['name'] = "<input $state type=checkbox class=delta-checkbox name=view[] value=$row[id] id=structure_$row[id]>&nbsp;<label for=structure_$row[id]>$row[name]</label>";
            if (empty($row['actions'])) {
                    $data[$index]['name'] = "<font color=red>$row[name]</font>";
            } else {
                    $data[$index]['name'] = "$row[name]";
            }
    }
    $TmplContent->set('show_view', $DB->rows);
    $Tree = new Tree($data, $checked, 'list');
    $TmplContent->set('view', $Tree->build());
    reset($Tree->used);
    while (list(,$row) = each($Tree->used)) {
            unset($data[$row]);
    }

    reset($data);
    while (list(,$row) = each($data)) {
            $TmplContent->iterate('/structure/', null, $row);
    }
    
    $path = array();
    $path[0]["name"] = "Роли";
    $path[0]["url"] = "./../";
    
    $table_name =  cmsTable::getInfoByAlias( $DB->db_alias, "cms_module" );
    
    $path[1]["name"] = $table_name["title"];
    $path[1]["url"] = "./?group_id=$group_id";    
    
    $TmplContent->iterateArray( '/path/', null, $path );
    
    $group_name  = $DB->result("SELECT uniq_name FROM auth_group WHERE id='{$group_id}'");
    $module_name = $DB->result("SELECT name FROM cms_module WHERE id='{$module_id}'");
    $TmplContent->set( 'path_current', "Привилегии $group_name. Модуль $module_name" );
    
}


?>
