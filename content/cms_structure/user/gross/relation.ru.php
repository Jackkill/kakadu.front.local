<?php

/* 
 * Скидки к группам товаров
 * @package Pilot 
 * @subpackage Shop 
 * @author Naumenko A.
 * @copyright C-format, 2014
 */

$group_id = globalVar($_GET['group_id'], 0);
$product_id = globalVar($_GET['product_id'], 0);
$type = globalVar($_GET['type'], 'group');

if(!empty($group_id)){
    $gross = $DB->result( "SELECT name FROM auth_gross_group WHERE id='$group_id'" );

    if(!empty($gross)){
        $TmplContent->set('type', $type);
        $TmplContent->set("group_id", $group_id);
        $TmplContent->set("headline", $gross);

        if($type == 'group') { 
        
            $gross = $DB->result( "SELECT name FROM auth_gross_group WHERE id='$group_id'" );

            // Группы
            $query = "
                    SELECT 
                            tb_group.id,
                            tb_group.group_id,                    
                            tb_group.name_".LANGUAGE_CURRENT." as name,                    
                            tb_group.active,
                            IFNULL(tb_relation.percent, 0) as percent
                    FROM shop_group tb_group
                    LEFT JOIN auth_gross_group_relation as tb_relation on tb_relation.object_id=tb_group.id 
                        and tb_relation.table_name='shop_group' and tb_relation.group_id='$group_id'
                    WHERE tb_group.group_id='1'
                    ORDER BY tb_group.priority ASC
            ";
            $data = $DB->query($query, 'id');

            reset($data);
            while(list($subgroup_id, $row)=each($data)){
                 // Группы
                $query = "
                    SELECT 
                            tb_group.id,
                            tb_group.group_id,                    
                            tb_group.name_".LANGUAGE_CURRENT." as name,                    
                            tb_group.active,
                           IFNULL(tb_relation.percent, 0) as percent
                    FROM shop_group tb_group
                    LEFT JOIN auth_gross_group_relation as tb_relation on tb_relation.object_id=tb_group.id 
                        and tb_relation.table_name='shop_group' and tb_relation.group_id='$group_id'
                    WHERE tb_group.group_id='$subgroup_id'
                    ORDER BY tb_group.priority ASC
            ";
            $subgroup = $DB->query($query);
            $fp = $TmplContent->iterate('/groups/', null, $row);
            $TmplContent->iterateArray('/groups/sub/', $fp, $subgroup);
        }
    }
    elseif ($type == 'product'){
        
        /* товар со скидкой */
        $query = "
		select 
			tb_relation.id,
			tb_relation.object_id,
			tb_relation.percent,
			IFNULL(CONCAT(tb_parent.name_".LANGUAGE_SITE_DEFAULT.", ' / ', tb_group.name_".LANGUAGE_SITE_DEFAULT."), tb_group.name_".LANGUAGE_SITE_DEFAULT.") as group_name,			
			CONCAT('<a href=\'/admin/shop/groups/info/?product_id=',tb_product.id, '\'>', tb_product.name_".LANGUAGE_SITE_DEFAULT.", '</a>') as name
		from auth_gross_group_relation as tb_relation
		inner join shop_product as tb_product on tb_product.id=tb_relation.object_id
		inner join shop_group as tb_group on tb_group.id=tb_product.group_id				
                left join shop_group as tb_parent on tb_parent.id=tb_group.group_id	
		where tb_relation.group_id='$group_id' and tb_relation.table_name='shop_product'
		order by tb_product.priority
	";
	$cmsTable = new cmsShowView($DB, $query);
        $cmsTable->setParam('add', false);
        $cmsTable->setParam('edit', false);
        $cmsTable->setParam('show_filter', false);
        $cmsTable->setParam('title', '');
	$cmsTable->addColumn('object_id', '5%', 'center', 'ID');
	$cmsTable->addColumn('name', '30%', 'left', 'Наименование');
	$cmsTable->addColumn('group_name', '30%', 'left', 'Группа');
	$cmsTable->addColumn('percent', '10%', 'center', 'Скидка');
	$cmsTable->setColumnParam('percent', 'editable', true);
	$TmplContent->set('products', $cmsTable->display());
	unset($cmsTable);
                
        $tmpl_search = TemplateUDF::search_products(array('table_name'=>'auth_gross_group_relation'));
        $TmplContent->set('search_form', $tmpl_search);
    }
    
}

}
elseif (!empty($product_id)){
    $TmplContent->set("product_id", $product_id);
    $product = $DB->result("select name_".LANGUAGE_SITE_DEFAULT." as name FROM shop_product WHERE id='{$product_id}' ");
    
    $TmplContent->set('headline', $product);
     //Добавляем в таблицу связей товара и группы оптовиков
    $query = "
        INSERT IGNORE INTO  auth_gross_group_relation (`group_id`,`table_name`, `object_id`, `percent`)
            (select `id` as group_id, 'shop_product' as table_name, '$product_id' as object_id, '0' as percent "
        . " from auth_gross_group )" ;
    $DB->query($query);
    
    //выборка данных
    $query = "
		select 
			tb_relation.id,
			tb_relation.object_id,
			tb_relation.percent,		
			tb_group.name
		from auth_gross_group_relation as tb_relation		
		inner join auth_gross_group as tb_group on tb_group.id=tb_relation.group_id
		where tb_relation.object_id='$product_id' and tb_relation.table_name='shop_product'
	
	";
	$cmsTable = new cmsShowView($DB, $query);
        $cmsTable->setParam('add', false);
        //$cmsTable->setParam('edit', false);
        $cmsTable->setParam('show_filter', false);
        $cmsTable->setParam('title', '');
        
	$cmsTable->addColumn('name', '30%', 'left', 'Группа');	
	$cmsTable->addColumn('percent', '10%', 'center', 'Скидка');
	$cmsTable->setColumnParam('percent', 'editable', true);
	$TmplContent->set('products', $cmsTable->display());
	unset($cmsTable);
}