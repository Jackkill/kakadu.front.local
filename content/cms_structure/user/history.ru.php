<?php
/** 
 * Лог входа в систему
 * @package DeltaCMS
 * @subpackage Auth 
 * @copyright c-format, 2016
 */ 

$type = globalVar($_GET['type'], '');
$TmplContent->set('type', $type);

if($type == 'edits'){
     function loge_filter($row){
         global $DB;
        
       //  $field_row = $DB->query_row("select * from `{$row['table_name']}` where id = '{$row['field_id']}' " );
         
         $row['event'] = trim($row['event'], '/');
         $row['event'] = ($row['event'] == 'add' && empty($row['field_id'])) ? 'insert' : 'update';
//         
//         if( !empty($field_row) ){
//            $row['name'] = (isset($field_row['name'."_".LANGUAGE_SITE_DEFAULT]) ) 
//                        ? $field_row['name'."_".LANGUAGE_SITE_DEFAULT]
//                        : ( ( isset($field_row['name']) ) ? $field_row['name'] : '');
//         }
//         
      
         return $row;
     }
         $query = "
            SELECT		
                    tb_log.id,
                    tb_log.table_id,
                    tb_log.ip,
                    tb_log.field_id,
                    tb_log.event,
                    REPLACE(tb_log.event, 'cms/table_', '') as event,
                    tb_user.login,                    	
                    tb_table.title_".LANGUAGE_ADMIN_DEFAULT." as table_title,
                    tb_table.name as table_name,
                    tb_log.content,	
                    date_format(tb_log.dtime, '".LANGUAGE_DATE_SQL." %H:%i') as date
            from `cms_log_event` as tb_log	
            left join auth_user tb_user on tb_user.id=tb_log.admin_id
            left join cms_table tb_table on tb_table.id=tb_log.table_id
            order by tb_log.dtime DESC
    ";
    $cmsTable = new cmsShowView($DB, $query);
    $cmsTable->setParam('prefilter', 'loge_filter');
  if(!IS_DEVELOPER){  $cmsTable->setParam('delete', false); }
    $cmsTable->setParam('edit', false);
    $cmsTable->setParam('add', false);    
    $cmsTable->addColumn('login', '15%', 'left', 'Админ');
    $cmsTable->addColumn('event', '10%', 'left', 'Событие');
    $cmsTable->addColumn('table_title', '15%', 'left', 'Раздел');
  //  $cmsTable->addColumn('name', '20%', 'left', 'Страница');
    $cmsTable->addColumn('content', '10%', 'left', 'Контент');
    $cmsTable->addColumn('date', '15%', 'center', 'Дата');
    $TmplContent->set('cms_table', $cmsTable->display());
    unset($cmsTable);
    
}elseif($type == 'contents') {
     function log_filter($row){
         global $DB;
        
         $field_row = $DB->query_row("select * from `{$row['table_name']}` where id = '{$row['edit_id']}' " );
         
         if( !empty($field_row) ){
            $row['name'] = (isset($field_row['name'."_".LANGUAGE_SITE_DEFAULT]) ) 
                        ? $field_row['name'."_".LANGUAGE_SITE_DEFAULT]
                        : ( ( isset($field_row['name']) ) ? $field_row['name'] : '');
         }
         
         if( !empty($row['content']) ){
            $row['html'] = '<center><a href="javascript:void(0);" onclick="$.fn.custombox({url:$(\'#rowf_'.$row['id'].'\').html(),title: \'Контент\', overlay: false});return false;" class="aicons info">&nbsp;</a></center>';
            $row['html'] .= '<div id="rowf_'.$row['id'].'" class="uprising" style="display:none;">'.$row['content'].'</div>';
         }
         return $row;
     }
     $query = "
            SELECT		
                    tb_log.id,
                    tb_log.table_name,
                    tb_log.edit_id,
                    tb_user.login,                    	
                    tb_table.title_".LANGUAGE_ADMIN_DEFAULT." as table_title,
                    tb_log.content,	
                    date_format(tb_log.dtime, '".LANGUAGE_DATE_SQL." %H:%i') as date
            from `cvs_log` as tb_log	
            left join auth_user tb_user on tb_user.id=tb_log.admin_id
            left join cms_table tb_table on tb_table.name=tb_log.table_name
            order by tb_log.dtime DESC
    ";
    $cmsTable = new cmsShowView($DB, $query);
    $cmsTable->setParam('prefilter', 'log_filter');
    $cmsTable->setParam('delete', false);
    $cmsTable->setParam('edit', false);
    $cmsTable->setParam('add', false);    
    $cmsTable->addColumn('login', '15%', 'left', 'Админ');
    $cmsTable->addColumn('table_title', '15%', 'left', 'Раздел');
    $cmsTable->addColumn('name', '20%', 'left', 'Страница');
    $cmsTable->addColumn('html', '10%', 'left', 'Контент');
    $cmsTable->addColumn('date', '15%', 'center', 'Дата');
    $TmplContent->set('cms_table', $cmsTable->display());
    unset($cmsTable);
}else{
    $TmplContent->set('type', '');
    $query = "
            SELECT		
                    tb_log.user_id,
                    tb_log.login,
                    tb_log.ip,		
                    date_format(tb_log.login_dtime, '".LANGUAGE_DATE_SQL." %H:%i') as date,
                    date_format(tb_log.logout_dtime, '".LANGUAGE_DATE_SQL." %H:%i') as date_to
            from auth_log as tb_log	
            order by tb_log.login_dtime DESC
    ";
    $cmsTable = new cmsShowView($DB, $query);
    $cmsTable->setParam('delete', false);
    $cmsTable->setParam('edit', false);
    $cmsTable->setParam('add', false);
    $cmsTable->addColumn('user_id', '16%', 'left', "ID");
    $cmsTable->addColumn('login', '25%', 'left');
    $cmsTable->addColumn('ip', '16%', 'left');
    $cmsTable->addColumn('date', '16%', 'center', 'Дата входа');
    $cmsTable->addColumn('date_to', '16%', 'center', 'Дата выхода');
    $TmplContent->set('cms_table', $cmsTable->display());
    unset($cmsTable);
}