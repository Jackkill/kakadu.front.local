<?php
/**
 * Формирование розписания задач
 * @package Pilot
 * @subpackage CMS
 * @author Markovskiy Dima<dima@delta-x.ua>
 * @copyright Delta-X, ltd. 2010
 */

$crontab_urls = $DB->fetch_column("SELECT url FROM cms_crontab");

$files = Filesystem::getAllSubdirsContent(SITE_ROOT.'system/crontab/', true);

reset($files);
while (list(, $file) = each($files)) {
    if (substr($file, -4) != '.php') {
        continue;
    }
    $info = array(
	'path' => Uploads::getURL($file),
	'author' => '',
	'description' => '',
	'time' => '',
	'author' => '',
	'copyright' => ''
    );
    $info["path"] = substr($info["path"], 1, strlen($info["path"]));
    $content = file_get_contents($file);
    
    preg_match('/^<\?php(.+)\*\//ismU', $content, $matches);
    if (empty($matches[1])) {
	continue;
    }
    
    preg_match('/(?:@author)\s+(.+)$/imsU', $matches[1], $author);
    preg_match('/(?:@description)\s+(.+)$/imsU', $matches[1], $description);
    preg_match('/(?:@time)\s+(.+)$/imsU', $matches[1], $time);
    preg_match('/(?:@copyright)\s+(.+)$/imsU', $matches[1], $copyright);
    
    $author      = globalVar( $author[1], "" );
    $description = globalVar( $description[1], "" );
    $time        = globalVar( $time[1], "" );
    $copyright   = globalVar( $copyright[1], "" );
    
    if ( in_array( $info["path"] , $crontab_urls) )
        continue;
    
    $DB->insert("INSERT IGNORE INTO cms_crontab SET url = '{$info["path"]}', name = '$description', time_desc = '$time'");    
    
}

function cms_filter($row){
    $url = '/' . $row["url"];
//    $url = substr($url,  strpos($url, "/"));
//    $url = substr($url,  0, strpos($url, "."));
    
    $row["name"] = "<a href='/admin/logs/crontablog/?crontab_id={$row["id"]}'>" . $row["name"] . "</a>";
    $row["exec"] = '<a href="'.$url.'" target="_blank" >запустить</a>';
    return $row;
}

$query = "
    SELECT
        id,
        name,
        url,
        time_desc,
	DATE_FORMAT(start_dtime, '%d.%m.%Y %H:%i:%s') as dtime,
	DATE_FORMAT(end_dtime, '%d.%m.%Y %H:%i:%s') as last_dtime,
	TIMESTAMPDIFF(SECOND, start_dtime, end_dtime) as time,
	case
            when status = 'failed' then '<span style=\'color:red;font-size:10px;\'>неудачно</span>'
            when status = 'blocked' then '<span style=\'color:#777;font-size:10px;\'>заблокирован</span>' 
            else '<span style=\'color:green;font-size:10px;\'>успешно</span>'
	end as status,
        priority
    FROM cms_crontab  
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('prefilter', 'cms_filter');
$cmsTable->setParam('add', false);
$cmsTable->setParam('excel', false);
$cmsTable->setParam('delete', true);
$cmsTable->addColumn('name', '15%', 'center');
$cmsTable->addColumn('url', '15%', 'center');
$cmsTable->addColumn('time_desc', '15%', 'center');
$cmsTable->addColumn('exec', '15%', 'center', 'Выполнить');
$cmsTable->addColumn('last_dtime', '15%', 'center', 'Последний запуск');
echo $cmsTable->display();
unset($cmsTable);

?>