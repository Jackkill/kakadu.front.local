<?php

/**
 * Организаторы и партнеры
 * @package DeltaCMS
 * @subpackage Site
 * @author Naumenko A.
 * @copyright c-format, 2016
 */ 

    function cms_prefilter($row) {
        $file = Uploads::getIsFile('site_parthner', 'image', $row['id'], $row['image']);
        $row['image'] = Uploads::fancyImage($file, $row['name'], 50);
        return $row;
    }
    $query = "
            select 
                    tb_parthner.id,
                    tb_parthner.name_".LANGUAGE_CURRENT." as name,                 
                    tb_parthner.link,
                    tb_parthner.image,
                    tb_parthner.is_organizator,
                    tb_parthner.active,
                    tb_parthner.priority
            from site_parthner tb_parthner            
            order by tb_parthner.priority
    ";

    $cmsTable = new cmsShowView($DB, $query);    
    $cmsTable->setParam('prefilter', 'cms_prefilter');
    $cmsTable->addColumn('name', '15%');
    $cmsTable->addColumn('image', '10%','center');
    $cmsTable->addColumn('link', '40%', 'left');  
    $cmsTable->addColumn('is_organizator', '5%', 'center');
    $cmsTable->setColumnParam('is_organizator', 'editable', true);   
    $cmsTable->addColumn('active', '5%', 'center');
    $cmsTable->setColumnParam('active', 'editable', true);    
    echo $cmsTable->display();
    unset($cmsTable);