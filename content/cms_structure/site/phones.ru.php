<?php

/**
 * Список магазинов 
 * @package Site
 * @author Naumenko A. 
 * @copyright c-format, 2014
 */

    $query = "
            select 
                    tb_phone.id,
                    tb_phone.name_".LANGUAGE_CURRENT." as name,
                    tb_phone.phone,
                    tb_phone.active,
                    tb_phone.priority
            from site_phone tb_phone            
            order by tb_phone.priority
    ";

    $cmsTable = new cmsShowView($DB, $query);    
    $cmsTable->addColumn('name', '15%','left');
    $cmsTable->addColumn('phone', '40%', 'left');    
    $cmsTable->addColumn('active', '5%', 'center');
    $cmsTable->setColumnParam('active', 'editable', true);    
    echo $cmsTable->display();
    unset($cmsTable);