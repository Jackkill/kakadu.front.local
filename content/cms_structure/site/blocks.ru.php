<?php
/** 
 * Текстовые блоки на сайте
 * @package Pilot 
 * @subpackage CMS
 * @author Miha Barin <barin@delta-x.com.ua> 
 * @copyright Delta-X, ltd. 2010
 */ 

$query = "
	SELECT 
		id,
		uniq_name,
		LEFT( content_".LANGUAGE_CURRENT.", 400) as content,
		title
	FROM block
	ORDER BY title
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->addColumn('title', '30%');
//$cmsTable->addColumn('content', '40%');
$cmsTable->addColumn('uniq_name', '20%');
echo $cmsTable->display();
unset($cmsTable);
