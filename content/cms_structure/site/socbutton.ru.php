<?php

/**
 * Список ссылок на социальные сети
 * @package DeltaCMS
 * @subpackage Site
 * @author Naumenko A.
 * @copyright c-format, 2015
 */ 

    $query = "
            select 
                    tb_button.id,
                    tb_button.name_".LANGUAGE_CURRENT." as name,                 
                    tb_button.uniq_name,                 
                    tb_button.link,
                    tb_button.active,
                    tb_button.priority
            from site_socbutton tb_button            
            order by tb_button.priority
    ";

    $cmsTable = new cmsShowView($DB, $query);    
    $cmsTable->addColumn('name', '15%','center');
    $cmsTable->addColumn('uniq_name', '10%','center');
    $cmsTable->addColumn('link', '40%', 'left');    
    $cmsTable->addColumn('active', '5%', 'center');
    $cmsTable->setColumnParam('active', 'editable', true);    
    echo $cmsTable->display();
    unset($cmsTable);