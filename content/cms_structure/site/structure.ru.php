<?php
/**
 * @package DeltaCMS
 * @subpackage Site
 * @author Naumenko A.
 * @copyright c-format, 2015
 */ 

//$TmplDesign->iterate('/onload/', null, array('function' => "$('#structure_copy').jqm();"));

$pattern = "/^(\+38)?\(0\d{2}\)\s{1}\d{3}-\d{2}-\d{2}$/";
/*
$phones = array('+38(097)802-45-04', '+38(497) 802-45-04', '+38(097) 802-45-04', '(097) 802-45-04', '(097) 80-245-04', '0978024504');
foreach($phones as $p){
    if( preg_match($pattern, $p, $matches) ){
        echo "IS true " . $p . "<br/>";
    }
    else{
        echo "IS false " . $p . "<br/>";
    }
}*/

$structure_id = globalVar($_GET['structure_id'], 0);
$TmplContent->set('structure_id', $structure_id);

// Копирование раздела
/*$query = "select concat(name_".LANGUAGE_CURRENT.", ' (', url, ')') from site_structure where id='$structure_id'";
$current = $DB->result($query);
$TmplContent->set('current', $current);
$query = "select id, title from site_template order by priority asc";
$template = $DB->fetch_column($query);
$TmplContent->set('template', $template);
*/
/*
$query = "select id, id as real_id, structure_id as parent, name_ru as name from site_structure";
$data = $DB->query($query, 'id');
$Tree = new Tree($data, array());
$tree = $Tree->build(0);
unset($Tree);
$TmplContent->set('tree', $tree);
*/


$enum = cmsTable::getEnumFields('site_structure');

function cms_prefilter($row) {
	global $enum;
        
        if( $row['substitute_url'] ){
            $row['url'] = "<small>Переадресация</small>";
        }
        elseif( $row['active'] ){
            $row['url'] = '<a href="http://'.$row['url'].'/" target="_blank" title="Перейти на страницу «'.$row['name'].'»">'
                        . ' <i class="aicons views">&nbsp;</i></a>';
        }
        else{
            $row['url'] = '';
        }
//	$row['name'] = '<img src="/design/cms/img/button/edit_text.gif" align="bottom" width="15" height="14" border="0"> <a href="#" onclick="cw('.$row['id'].');return false;">'.$row['name'].'</a>';
	$row['name'] = $row['edit'] .' ' . $row['name'];
	
	// Определяем размер файла с контентом
	$row['name'] .= ($row['len'] > 0) ?
		"<br><span class=comment>[Размер: ".number_format($row['len'], 0, '.', ' ')." байт]</span>":
		"<br><span class=comment>[Размер: нет]</span>";
	
	// группы доступа
	if (is_module('user')) {
		$access_level = preg_split("/,/", $row['access_level'], -1, PREG_SPLIT_NO_EMPTY);
		$access_level = array_intersect(array_flip($enum['access_level']), $access_level);
		$row['name'] .= '<span class=comment>['.implode(", ", array_flip($access_level)).': '.$row['group_access'].']</span>';
	}
	
	// В каком меню показывать
	$show_menu = preg_split("/,/", $row['show_menu'], -1, PREG_SPLIT_NO_EMPTY);
	$show_menu = array_intersect(array_flip($enum['show_menu']), $show_menu);
	$row['uniq_name'] = "<a href='?structure_id=$row[id]'>$row[uniq_name]</a><br><span class=comment>".implode(", ", array_flip($show_menu)).'</span>';
	
	return $row;
}

$query = "
	SELECT
		tb_structure.id,
		tb_structure.priority,
		tb_structure.access_level,
		tb_structure.show_menu,
		html_editor(tb_structure.id, 'site_structure', 'content_".LANGUAGE_SITE_DEFAULT."', '<span class=\"edit_text\"></span>') as edit,
		length(tb_structure.content_".LANGUAGE_SITE_DEFAULT.") as len,
		tb_structure.url,
		tb_structure.substitute_url,
		tb_structure.active,
		tb_structure.name_".LANGUAGE_SITE_DEFAULT." AS name,
		IFNULL(
			(
				SELECT GROUP_CONCAT(tb_group.name ORDER BY tb_group.name SEPARATOR ', ')
				FROM auth_group AS tb_group
				INNER JOIN site_group_relation AS tb_relation ON tb_relation.group_id=tb_group.id
				WHERE tb_relation.structure_id=tb_structure.id
			), 
			'все'
		) AS group_access,
		CONCAT(
			IF(
				(SELECT COUNT(id) FROM site_structure WHERE structure_id=tb_structure.id) > 0, 
				CONCAT(
						IF (tb_structure.active!='1',
					'<span class=\"folder grey\" alt=\"В разделе находится: ',
                                        '<span class=\"folder\" alt=\"В разделе находится: '
                                    ),
					(
						SELECT
							CASE
								WHEN COUNT(id)-1=1 THEN CONCAT(COUNT(id)-1, ' страница')
								WHEN COUNT(id)-1<5 THEN CONCAT(COUNT(id)-1, ' страницы')
								ELSE CONCAT(COUNT(id)-1, ' страниц')
							END
						FROM site_structure_relation 
						WHERE parent=tb_structure.id
					),
					'\"</span></span>'
				),

                                IF (tb_structure.active!='1',
                                    '<span class=\"ie_style grey\" alt=\"Страница:\"></span>', '<span class=\"ie_style\" alt=\"Страница:\"></span>'
                                )
				), 
			' ',
			IF (tb_structure.active!='1', CONCAT('<font color=silver>', tb_structure.uniq_name, '</font>'), tb_structure.uniq_name)
		) AS uniq_name
	FROM site_structure AS tb_structure
	WHERE tb_structure.structure_id='$structure_id' AND tb_structure.id <> '75898'
	ORDER BY tb_structure.priority ASC
";
$cmsTable = new cmsShowView($DB, $query, 200, 'site_structure');
$cmsTable->setParam('prefilter', 'cms_prefilter');
if ($structure_id > 0) {
	$cmsTable->addColumn('uniq_name', '15%');
} else {
	$cmsTable->addColumn('uniq_name', '15%', 'left', 'Сайт');
}

$cmsTable->addColumn('name', '40%');
$cmsTable->addColumn('url', '5%', 'center', 'Просмотр');
    $cmsTable->addColumn('active', '5%', 'center');
    $cmsTable->setColumnParam('active', 'editable', true);   

//$cmsTable->addEvent('copy', "javascript:$('#structure_copy').jqmShow();", false, true, true, '/design/cms/img/event/table/copy.gif', '/design/cms/img/event/table/copy_over.gif', 'Копировать', null, true);
//$cmsTable->addEvent('xml', "/action/admin/sdk/structure_xml_builder/", false, true, true, '/design/cms/img/event/table/xml.gif', '/design/cms/img/event/table/xml_over.gif', 'Скачать в формате xml', null, true);
$TmplContent->set('cms_view', $cmsTable->display());
unset($cmsTable);


/**
 * Class uiGallery

$is_promo = $DB->result("select id from site_structure_relation where parent='63147' and id='$structure_id'");
if ( $is_promo > 0 ) {
    $gallery = new uiGallery('site_structure', $structure_id);
    $gallery->setParam('field', 'photo');
    $gallery->setParam('headline', 'Фоновое изображение в шапку сайта');
    $TmplContent->set('cms_gallery', $gallery->display());    
    unset($gallery);
}
 */

/**
 * Выводим разделы, которыми ограничен пользователь
 */
$query = "select count(*) from auth_group_structure where group_id='".$_SESSION['auth']['group_id']."'";
$allow = $DB->result($query);
if ($allow) {
	/*$query = "
		SELECT
			tb_link.structure_id AS id,
			tb_parent.structure_id,
			CONVERT(GROUP_CONCAT(tb_structure.name_".LANGUAGE_SITE_DEFAULT." ORDER BY tb_relation.priority ASC SEPARATOR ' / ') USING ".CMS_CHARSET_DB.") COLLATE ".CMS_COLLATION." AS name
		FROM auth_group_structure AS tb_link
		INNER JOIN auth_user AS tb_user ON tb_user.group_id = tb_link.group_id
		INNER JOIN site_structure_relation AS tb_relation ON tb_relation.id=tb_link.structure_id
		INNER JOIN site_structure AS tb_structure ON tb_structure.id = tb_relation.parent
		INNER JOIN site_structure AS tb_parent ON tb_parent.id = tb_link.structure_id
		WHERE tb_user.id='".$_SESSION['auth']['id']."'
		GROUP BY tb_link.group_id, tb_link.structure_id
		ORDER BY tb_link.structure_id ASC, tb_relation.priority ASC 
	";
	$links = $DB->query($query);
	$TmplContent->set('links', $DB->rows);
	reset($links);
	while(list(,$row) = each($links)) {
		$TmplContent->iterate('/links/', null, $row);
	}
	unset($links);*/
}




?>