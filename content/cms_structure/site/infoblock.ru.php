<?php
/**
 * Разделы. Стилизированные блоки
 * 
 * @package DeltaCMS 
 * @subpackage Site
 * @author Naumenko A.
 * @copyright c-format, 2015
 */


$group_id = globalVar($_GET['group_id'], 0);
$infoblock = $DB->query_row("SELECT * FROM site_infoblock WHERE id='$group_id' ");

if( empty($group_id) ) {
    function cms_filter($row) {
        $row['name'] = "<a href='./?group_id={$row['id']}'>".$row['name'].'</a>';
        //$previews = Uploads::getIsFile('site_infoblock', 'previews', $row['id'], $row['previews']);
        //if($previews){
        //    $row['name'] .= "  <a class='fancy' href='".$previews."' title='Отображение блока на сайте'><i class='aicons inline info'>&nbsp;</i></a>";
        //}
        if($row['comment']){
            $row['name'] .= "<br/><span class='comment'>" . $row['comment'] ."</span>";
        }
        $row['code_paste'] = "{infoblock name=&quot;".$row['id']."&quot;}";
        
           // Определяем размер файла с контентом
	 //$row['textbefore'] .= ($row['lentextbefore'] > 0) ? "<br><span class=comment>[Размер: ".num2str($row['lentextbefore'])." байт]</span>": "";
	// $row['textafter'] .= ($row['lentextafter'] > 0) ? "<br><span class=comment>[Размер: ".num2str($row['lentextafter'])." байт]</span>": "";
         
        //$row['textedit'] = "<div style='margin:5px'>" . $row['textbefore'] . "</div>" . $row['textafter'];
        return $row;
    }

    // Разделы
    $query = "
            select 
                  tb_group.id, 
                  tb_group.template_id, 
                  tb_group.comment,             
                  CONCAT('<small>', tb_template.title, '</small>') as title,
                  tb_group.name_".LANGUAGE_CURRENT." as name,
                  tb_group.priority,
                  tb_group.active,
                  (
                        select count(distinct t_message.id) 
                        from site_infoblock_message as t_message WHERE t_message.group_id=tb_group.id
                  ) as count_news
            from site_infoblock as tb_group
            left join site_infoblock_template as tb_template on tb_template.id=tb_group.template_id           
            order by tb_group.priority
    ";
    $cmsTable = new cmsShowView($DB, $query);
    $cmsTable->setParam('title', 'Информационные блоки ');   
    $cmsTable->setParam('prefilter', 'cms_filter');
  
    $cmsTable->addColumn('name', '25%');
//    $cmsTable->addColumn('textedit', '15%', 'center', 'Тексты');
    
    $cmsTable->addColumn('title', '15%', 'left', 'Шаблон');
    $cmsTable->addColumn('count_news', '10%', 'center', 'К-ство');
    $cmsTable->addColumn('code_paste', '20%', 'center', 'Код вставки');
    $cmsTable->addColumn('active', '5%', 'center');
    $cmsTable->setColumnParam('active', 'editable', true);
    echo $cmsTable->display();
    unset($cmsTable);
}
else{
     // Сообщения
     function cms_filter($row) {
        //$row['icon_id'] = SvgIcons::quickPaste($row['icon_id']);
        return $row;
    }
    $query = "
            select 
                tb_message.id,          
                IF(tb_message.active='0', CONCAT('<font style=\"color:gray\">', tb_message.name_".LANGUAGE_CURRENT.", '</font>'), tb_message.name_".LANGUAGE_CURRENT.") AS name,                                
                IFNULL(REPLACE(tb_message.description_".LANGUAGE_CURRENT.", '\n', '<br>'), '') AS description,                
                tb_message.subtitle_".LANGUAGE_CURRENT." as subtitle,
                tb_message.active,
                tb_message.priority
            from site_infoblock_message tb_message            
            where tb_message.group_id = '$group_id'
            order by tb_message.priority
    ";
    $cmsTable = new cmsShowView($DB, $query);    
    $cmsTable->setParam('prefilter', 'cms_filter');
    $cmsTable->setParam('parent_link', '/admin/site/infoblock/?');      
   
    $cmsTable->addColumn('name', '20%');    
    $cmsTable->addColumn('subtitle', '15%');    
    $cmsTable->addColumn('description', '40%');
    $cmsTable->addColumn('active', '5%', 'center');
    $cmsTable->setColumnParam('active', 'editable', true);
    echo $cmsTable->display();
    unset($cmsTable);
}

if(IS_DEVELOPER && empty($group_id) ){
     // Сообщения
    $query = "
            select 
                tb_template.id,
                tb_template.title,
                text_editor(tb_template.id, 'site_infoblock_template', 'content', 'edit') as content,
                tb_template.priority
            from site_infoblock_template tb_template           
            order by tb_template.priority
    ";
    $cmsTable = new cmsShowView($DB, $query);
   // $cmsTable->setParam('title', 'Список сообщений ');   
    //$cmsTable->setParam('prefilter', 'cms_filter');
   // $cmsTable->setParam('parent_link', '/admin/site/infoblock/?');
    $cmsTable->addColumn('title', '30%');
    $cmsTable->addColumn('content', '30%', 'center', 'Шаблон');

    echo $cmsTable->display();
    unset($cmsTable);
}