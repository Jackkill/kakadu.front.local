<?php
/** 
 * Формы на сайте
 * @package Pilot 
 * @subpackage Form
 * @author Naumenko A.
 * @copyright (c) 2014, c-format
 */ 

function cms_filter($row) {
	$row['title'] = "<a href='./fields/?form_id=$row[id]'>$row[title]</a>";
        $row['resulttext'] .= "<a href='#' class='aicons info inline' onclick='$.fn.custombox({url:$(\"#info_text\").html(),title: \"Сообщения\",overlay: false});return false;'>&nbsp;</span>";
        $row['autoreply'] .= "<a href='#' class='aicons info inline' onclick='$.fn.custombox({url:$(\"#info_avto\").html(),title: \"Автоответ\",overlay: false});return false;'>&nbsp;</span>";
	return $row;
}

$query = "
	SELECT 
		id,
		uniq_name,priority,
		html_editor(id, 'form', 'resulttext_".LANGUAGE_CURRENT."', 'Edit') as resulttext,
		html_editor(id, 'form', 'autoreply_".LANGUAGE_CURRENT."', 'Edit') as autoreply,
		html_editor(id, 'form', 'description_".LANGUAGE_CURRENT."', 'Edit') as description,
		title_".LANGUAGE_CURRENT." AS title
	FROM form
	ORDER BY priority
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('prefilter', 'cms_filter');
$cmsTable->addColumn('title', '30%');
$cmsTable->addColumn('uniq_name', '10%');
$cmsTable->addColumn('description', '10%', 'center');
$cmsTable->addColumn('resulttext', '10%', 'center');
$cmsTable->addColumn('autoreply', '10%', 'center');
echo $cmsTable->display();
unset($cmsTable);
?>

<div id='info_text' style='display:none'>
        <span >После успешной отправки данных формы вместо формы будет отображаться введенный текст.</span>
</div>
<div id='info_avto' style='display:none'>
        <span >Автоматический ответ пользователю</span>
</div>
