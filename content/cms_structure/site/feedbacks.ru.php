<?php
/** 
 * Обратная связь
 * @package Pilot 
 * @subpackage Site
 * @author Naumenko A.
 * @copyright c-format, 2014
 */ 

$feedback_id = globalVar($_GET['id'], 0);
$form_id = globalVar($_GET['form_id'], 0);

$forms = $DB->query("select tb_form.id, tb_form.title_".LANGUAGE_CURRENT." as title, "
        . " IF (tb_form.id='$form_id', 'selected', 'none') as class_name "
        . " FROM form as tb_form"
        . " INNER JOIN site_form_feedback as tb_feedback on tb_feedback.form_id=tb_form.id "
        . " GROUP BY tb_form.id");
$TmplContent->iterateArray('/forms/', null, $forms);
$TmplContent->set('form_id', $form_id);

if(empty($feedback_id)){

function cms_filter($row) {
	global $DB, $form_id;
	if($row['status'] == "new"){
		$row['status'] = "<span style=\"color:green\">новый</span>";
	} elseif($row['status'] == "in_process"){
		$row['status'] = "в обработке";
	} elseif($row['status'] == "done"){
		$row['status'] = "<span style=\"color:grey\">выполнен<br/></span>";
	}
        if(!empty($row['last_admin'])){
            $row['status'] .= "<br/>({$row['last_admin']})";
        }
        
        //$row['params'] = preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $row['params']);
        /*$row['params'] = preg_replace_callback(
                                '/s:(\d+):"(.*?)";/',
                                function( $m ) { return 's:'.strlen($m[2]).':"'.$m[2].'";' ; },
                                $row['params']
                            );*/
                                
        $url = (!empty($form_id)) ? "&form_id={$form_id}" : "";
        $row['form'] = "<a href='/admin/site/feedbacks/?id={$row['id']}".$url."' title='Подробнее'>".$row['form'].'</a>';
          
        $params = $DB->fetch_column("SELECT CONCAT( '<b>', name, ': </b>',  value) "
                . " FROM site_form_feedback_value WHERE feedback_id='{$row['id']}' ORDER BY priority LIMIT 5 ");        
        if ( count($params) ) {
            $row['params'] = "<div style='padding:5px;font-size:11px;'>" . implode('<br/>', $params) . "</div>";
        }    
	return $row;
} 

$query = "
	select 
		tb_feedback.id, 
                tb_feedback.status,	
                tb_feedback.name, 
                tb_feedback.phone, 
                tb_feedback.email, 
                DATE_FORMAT(tb_feedback.tstamp, '%d.%m.%Y  %H:%i') as tstamp,      
                tb_form.title_".LANGUAGE_CURRENT." as form,
		if(tb_admin.name, tb_admin.name, tb_admin.email) as last_admin
	from site_form_feedback as tb_feedback
	LEFT JOIN auth_user as tb_admin ON tb_admin.id = tb_feedback.last_admin	
	LEFT JOIN form as tb_form on tb_form.id=tb_feedback.form_id
        WHERE 1 ".  where_clause('tb_feedback.form_id', $form_id) ."
	order by  tb_feedback.tstamp desc
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('add', false);
$cmsTable->setParam('show_filter', false);
//$cmsTable->setParam('edit', false);
//$cmsTable->addColumn('last_admin', '7%', 'left');

$cmsTable->addColumn('form', '10%', 'left', 'Заявка');
$cmsTable->addColumn('name', '15%', 'left', 'ФИО');
$cmsTable->addColumn('email', '7%');
$cmsTable->addColumn('phone', '15%');

//$cmsTable->addColumn('theme', '15%');
$cmsTable->addColumn('params', '30%', 'left', "Параметры");
$cmsTable->addColumn('tstamp', '7%');
$cmsTable->setParam('prefilter', 'cms_filter');
$cmsTable->addColumn('status', '12%');
$TmplContent->set('cms_table', $cmsTable->display());
unset($cmsTable);

}
else{
    
    function cms_filter($row) {
        if($row['type'] == 'file'){
            $filename = Uploads::getIsFile('site_form_feedback_value', 'value', $row['id'], $row['value']);
            if ( !empty($filename) ) {
                //$filename = Uploads::getUploadsURL($filename);
                $row['value'] = "<a href='{$filename}'>Скачать файл</a>";
            }
            else{$row['value'] = '';}
        }
        return $row;
    }
    
    $data = $DB->query_row("SELECT * FROM `site_form_feedback` WHERE id='$feedback_id' ");
    $TmplContent->set('base_data', $data);
  
    $query = "
	select 
		tb_feedback.id, 			
                tb_feedback.name, 
                tb_feedback.value,
                tb_feedback.type
	from site_form_feedback_value as tb_feedback      
	WHERE tb_feedback.feedback_id = '$feedback_id' 	
	order by  tb_feedback.priority
    ";
    $cmsTable = new cmsShowView($DB, $query);
    
    $cmsTable->setParam('prefilter', 'cms_filter');
    $cmsTable->setParam('title', 'Дополнительные сведения');
    $cmsTable->setParam('add', false);
    $cmsTable->setParam('show_filter', false);
    $cmsTable->setParam('edit', false);
    $cmsTable->setParam('priority', false);
    $cmsTable->setParam('delete', false);
    
    $cmsTable->addColumn('name', '30%', 'right');
    $cmsTable->addColumn('value', '30%', 'left');
    $TmplContent->set('additional_table', $cmsTable->display());
    unset($cmsTable);
}