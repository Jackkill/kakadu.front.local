<?php
/**
 * @package DeltaCMS
 * @subpackage Site
 * @author Naumenko A.
 * @copyright c-format, 2015
 */ 
$type_id = globalVar($_REQUEST['type_id'], 0);

// Рубрики
$query = "
        select 
                tb_type.id,
                html_editor(id, 'news_type', 'content_".LANGUAGE_SITE_DEFAULT."', 'Описание') as about,
                concat('<a href=\"./?type_id=', id, '\">', tb_type.name_".LANGUAGE_CURRENT.", '</a>') as name,
                tb_type.active,    
                tb_type.uniq_name,
                (
                        select count(distinct t_message.id) 
                        from news_type_relation as t_relation
                        inner join news_message as t_message on t_message.type_id=t_relation.id
                        where t_relation.parent=tb_type.id
                ) as count_news,
                tb_type.priority
        from news_type as tb_type
        where type_id='$type_id'
        order by tb_type.priority
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->addColumn('name', '20%');
$cmsTable->addColumn('uniq_name', '10%');
$cmsTable->addColumn('about', '10%', 'center', 'Описание');
$cmsTable->addColumn('count_news', '10%', 'right', 'Новостей');
$cmsTable->addColumn('active', '5%');
$cmsTable->setColumnParam('active', 'editable', true);
echo $cmsTable->display();
unset($cmsTable);


// Новости
if ( !empty($type_id) ) {
    
    function cms_prefilter($row) {
        $image = Uploads::getIsFile('news_message', 'image', $row['id'], $row['image']);
        $row['image'] = Uploads::fancyImage( $image, $row['title'], 200 );
        return $row;
    }
    $sortby = $DB->result("select IF( `sortby` = 'date', 'date desc', `sortby`) as sortby from news_type where id='$type_id'");
    if(empty($sortby)){
            Header("Location: /admin/site/news/");
            exit;
    }
    $query = "
            SELECT 
                    tb_message.id,
                    tb_message.image,
                    tb_message.name_".LANGUAGE_CURRENT." as title,
                    CONCAT(html_editor(tb_message.id, 'news_message', 'content_".LANGUAGE_CURRENT."', tb_message.name_".LANGUAGE_CURRENT."),
                            '<br><span class=comment>', 
                            IFNULL(REPLACE(tb_message.announcement_".LANGUAGE_CURRENT.", '\n', '<br>'), ''),
                            '</span>'
                    ) AS name,
                    case
                            when date <> date_to and date_to <> '0000-00-00' and date_to is not null then concat(date_format(date, '".LANGUAGE_DATE_SQL."'),'-',date_format(date_to, '".LANGUAGE_DATE_SQL."'))
                            else date_format(date, '".LANGUAGE_DATE_SQL."')
                    end as date_period,
                    date_format(tb_message.date, '".LANGUAGE_DATE_SQL."') as date_txt,                    
                    tb_message.active,
                    tb_message.priority
            FROM news_message as tb_message
            WHERE tb_message.type_id='$type_id'
            ORDER BY tb_message.$sortby
    ";
    $cmsTable = new cmsShowView($DB, $query);
    $cmsTable->setParam('prefilter', 'cms_prefilter');
    if($sortby !== 'priority'){
        $cmsTable->setParam('priority', false);
    }
    //$cmsTable->addColumn('date_period', '15%', 'center', 'Период');
    $cmsTable->addColumn('date_txt', '15%', 'center', 'Дата публикации');    
    $cmsTable->addColumn('name', '40%');
    $cmsTable->addColumn('image', '25%', 'center');
    $cmsTable->addColumn('active', '10%', 'center');

    $cmsTable->setColumnParam('active', 'editable', true);

    echo $cmsTable->display();
    unset($cmsTable);
}
