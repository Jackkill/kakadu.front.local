<?php
/**
* Группы баннеров
*
* @package Pilot
* @subpackage Banner
* @version 2.1
* @author Naumenko A.
* @copyright (c) 2014, c-format
*/


function cms_filter($row) {
	
        if($row['active'] == '0'){
            $row['uniq_name'] = "<a href='./banner/?group_id=$row[id]' style=\"color:grey\">$row[uniq_name]</a>"
                    . " <a href=\"javascript:void(0);\" onclick=\"EditWindow( '".$row['profile_id']."', 'banner_profile', '', '".CURRENT_URL_LINK."', '".LANGUAGE_CURRENT."', '');\" style=\"color:red\">(Выключен)</a>";        
                    
            $row['date_from'] = "<span style=\"color:grey\">$row[date_from]</span>";        
            $row['date_to'] = "<span style=\"color:grey\">$row[date_to]</span>";        
            $row['name'] = "<span style=\"color:grey\">$row[name]</span>";        
        }
        else{
            $row['name'] = "<a href='./banner/?group_id=$row[id]'>$row[name]</a>";        
        }
	return $row;
}

$query = "
	SELECT 
		tb_group.id,
		tb_group.uniq_name,
		tb_group.name,
		tb_group.profile_id,
                COUNT(tb_banner.id) as count_banner,
                DATE_FORMAT(tb_profile.date_from, '".LANGUAGE_DATE_SQL."') AS date_from,
		DATE_FORMAT(tb_profile.date_to, '".LANGUAGE_DATE_SQL."') AS date_to,
                if (tb_profile.date_from <= current_date() && current_date() <= tb_profile.date_to, '1', '0'  ) as active
	FROM banner_group as tb_group
        INNER JOIN banner_profile as tb_profile on tb_profile.id=tb_group.profile_id
        left JOIN banner_banner as tb_banner on tb_banner.group_id=tb_group.id and tb_banner.active='1'
        group by tb_group.id
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('prefilter', 'cms_filter');
$cmsTable->addColumn('name', '30%');
$cmsTable->addColumn('uniq_name', '20%');

$cmsTable->addColumn('count_banner', '10%','center', "К-ство активных");
$cmsTable->addColumn('date_from', '15%', 'center', 'Дата с');
$cmsTable->addColumn('date_to', '15%', 'center', 'Дата по');
echo $cmsTable->display();
unset($cmsTable);
?>