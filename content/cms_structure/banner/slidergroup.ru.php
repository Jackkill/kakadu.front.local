<?php
/**
* Группы слайдеров
*
* @package Pilot
* @subpackage Banner
* @version 1.0
* @author Naumenko A.
* @copyright Copyright 2013, c-format.
*/

function cms_filter($row) {
	$row['name'] = "<a href='./slider/?group_id=$row[id]'>$row[name]</a>";
	return $row;
}

$query = "
	SELECT 
		tb_group.id,
		tb_group.uniq_name,
		tb_group.name,
		tb_group.amount,
                COUNT(tb_banner.id) as count_banner
	FROM banner_slidergroup tb_group
        left JOIN banner_slider as tb_banner on tb_banner.group_id=tb_group.id and tb_banner.active='1'
        group by tb_group.id
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('prefilter', 'cms_filter');

$cmsTable->addColumn('name', '30%');
$cmsTable->addColumn('uniq_name', '20%');
$cmsTable->addColumn('amount', '10%','center', "К-ство на страницу");
$cmsTable->addColumn('count_banner', '10%','center', "К-ство активных");
echo $cmsTable->display();
unset($cmsTable);
?>