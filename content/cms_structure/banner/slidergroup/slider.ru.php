<?php
/**
* Список слайдов группы
*
* @package Pilot
* @subpackage Banner
* @version 3.0
* @author Naumenko A.
* @copyright c-format, 2014
*/

$group_id = globalVar($_GET['group_id'], 0);

function row_filter($row) {
    
	$filename = Uploads::getIsFile('banner_slider', 'image', $row['id'], $row['image']);        
	$row['image'] = Uploads::fancyImage($filename, $row['title'], 300);    
	return $row;
}


$query = "
	SELECT 
		tb_slider.id,
		tb_slider.active,
		tb_slider.title_".LANGUAGE_CURRENT." AS title,
                CONCAT(html_editor(tb_slider.id, 'banner_slider', 'description_".LANGUAGE_CURRENT."', tb_slider.title_".LANGUAGE_CURRENT."),
                            '<br><span class=comment>', 
                            IFNULL(tb_slider.description_".LANGUAGE_CURRENT.", ''),
                            '</span>'
                ) AS name,    		
		tb_slider.image,
                tb_slider.priority
	FROM banner_slider as tb_slider
	WHERE tb_slider.group_id='$group_id'
        GROUP BY tb_slider.id    
";
$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('prefilter', 'row_filter'); 
$cmsTable->addColumn('name', '20%', 'left', 'Текст на слайде' );
$cmsTable->addColumn('image', '20%', 'center');
$cmsTable->addColumn('active', '5%', 'center', 'Активен');
$cmsTable->setColumnParam('active', 'editable', true);
echo $cmsTable->display();
unset($cmsTable);
?>