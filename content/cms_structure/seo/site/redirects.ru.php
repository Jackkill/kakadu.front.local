<?php

/** 
 * Вывод 404 ошибок
 * @package Pilot 
 * @subpackage Seo 
 * @author Naumenko A.
 * @copyright c-format, 2014
 */ 

    function cms_prefilter($row) {     
            $row['url_old'] = str_replace(CMS_HOST, '', $row['url_old']);
            $row['url_new'] = str_replace(CMS_HOST, '', $row['url_new']);
            $row['views'] = "<a href='http://".$row['link']."' target=\"_blank\"  title=\"Проверить\"><i class=\"aicons views\">&nbsp;</i></a>";
            return $row;
    }
    
    $query = "
            SELECT 
                    tb_redirect.`id`,                                         
                    IF(
                        LENGTH(tb_redirect.`url_new`) > 50,
                        LEFT(tb_redirect.`url_new`, 50),
                        tb_redirect.`url_new`
                    ) as url_new,
                    IF(
                        LENGTH(tb_redirect.`url_old`) > 50,
                        LEFT(tb_redirect.`url_old`, 50),
                        tb_redirect.`url_old`
                    ) as url_old,
                    tb_redirect.operation,                    
                    DATE_FORMAT(tb_redirect.`dtime`, '%d.%m.%Y %H:%i') as date,
                    tb_admin.name as admin,                                              
                    tb_redirect.`url_old` as link
            FROM `site_structure_redirect` as tb_redirect
            LEFT JOIN auth_user as tb_admin on tb_admin.id=tb_redirect.admin_id            
            GROUP BY tb_redirect.id
            ORDER BY tb_redirect.`dtime` DESC  
    ";
    $cmsTable = new cmsShowView($DB, $query);
    $cmsTable->setParam('prefilter', 'cms_prefilter');
    //$cmsTable->setParam('add', false);
    //$cmsTable->setParam('edit', false);
    //$cmsTable->setParam('delete', false);
   
    $cmsTable->addEvent('import', "javascript:import_equip();", true, true, true, '', '', 'Загрузить страницы', null, true);
    $cmsTable->filterSkipTable('auth_user');

    $cmsTable->addColumn('url_old', '30%', 'left');
    $cmsTable->addColumn('url_new', '30%', 'left');
     $cmsTable->addColumn('views', '5%', 'center', 'Проверить');
    $cmsTable->addColumn('admin', '10%', 'left', 'Администратор');
    $cmsTable->addColumn('date', '10%', 'center', 'Дата');
    $cmsTable->addColumn('operation', '5%', 'left');
   
    //$cmsTable->addColumn('total_count', '10%', 'right', 'Кол-во');

    $TmplContent->set('cms_table', $cmsTable->display());
    unset($cmsTable);

?>
