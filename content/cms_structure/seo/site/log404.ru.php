<?php

/** 
 * Вывод 404 ошибок
 * @package Pilot 
 * @subpackage Seo 
 * @author Naumenko A.
 * @copyright c-format, 2014
 */ 

$url = globalVar($_GET['url'], "");
$url = str_replace("[AND]", "&", $url);

if(empty($url)){
    function cms_prefilter($row) {

            $link = $row['url'];
            $row['url'] = "<a href='$link' target='_blank'>".substr($link, 0, 50)."</a>"; 

            if($row['url_counter'] > 1){
                    $row['url'] = $row['url']."&nbsp; <div style=\"float:right;\" > <a href='./?url=".urlencode($link)."' title=\"статистика\" class='aicons statistics'>&nbsp;</a> </div>";    
            }

            return $row;
    }

    $query = "
            SELECT 
                    `id`, 
                    `url`, 
                    date,
                    ip,
                    LEFT(referer, 100) as referer,
                    user_agent,
                    count(`id`) as url_counter, 
                    sum(`count`) as `total_count`
            FROM `seo_log_404`
            GROUP BY `url`
            ORDER BY `total_count` DESC  
    ";

    $cmsTable = new cmsShowView($DB, $query);
    $cmsTable->setParam('prefilter', 'cms_prefilter');
    $cmsTable->setParam('add', false);
    $cmsTable->setParam('edit', false);
    //$cmsTable->setParam('delete', false);
    $cmsTable->setParam('excel', false);

    $cmsTable->addColumn('url', '40%', 'left');
    $cmsTable->addColumn('referer', '20%', 'left');
    $cmsTable->addColumn('user_agent', '30%', 'left');
    $cmsTable->addColumn('total_count', '10%', 'right', 'Кол-во');

    echo $cmsTable->display();
    unset($cmsTable);
}
else{
    
    function cms_prefilter($row) {
            $row['referer'] = (strlen($row['referer']) > 100) ?
                    '<a href="'.$row['referer'].'">'.substr($row['referer'], 0, 100).'...</a>':
                    '<a href="'.$row['referer'].'">'.$row['referer'].'</a>';
            return $row;
    }

    $query = "
            SELECT
                    id,
                    DATE_FORMAT(date, '%d.%m.%Y %H:%i') as date,
                    url,
                    ip,
                    referer,
                    user_agent,
                    count
            FROM seo_log_404
            WHERE url='$url'
            ORDER BY count DESC  
    "; 
    $cmsTable = new cmsShowView($DB, $query); 
    $cmsTable->setParam('prefilter', 'cms_prefilter');
    $cmsTable->setParam('title', "Страница \"".$url."\"");
    $cmsTable->setParam('add', false);
    $cmsTable->setParam('edit', false);
    //$cmsTable->setParam('delete', false);
    $cmsTable->setParam('excel', false);
    $cmsTable->setParam('parent_link', '/admin/seo/site/log404/?');
    $cmsTable->setParam('show_parent_link', true);

    $cmsTable->addColumn('date', '5%', 'center');
    $cmsTable->addColumn('ip', '5%', 'center');
    $cmsTable->addColumn('referer', '40%', 'left');
    $cmsTable->addColumn('user_agent', '30%', 'left');
    $cmsTable->addColumn('count', '10%', 'right', 'Кол-во');
    echo $cmsTable->display();
    unset($cmsTable);
}
?>

