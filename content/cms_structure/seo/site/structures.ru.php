<?php
/**
 * Вывод структуры сайта
 * @package SEO
 * @subpackage Content_Admin
 * @author Naumenko A.
 * @copyright c-format, 2014
 */


$group_id = globalVar($_GET['group_id'], 0);
$type = globalVar($_GET['type'], 'structure');

$TmplContent->set('group_id', $group_id);
$TmplContent->set('type', $type);



if(IS_DEVELOPER){
    echo '<a class="btn btn-grey" style="position:absolute;top:15px;left:50%;margin-left:-20px;" href="/action/admin/seo/auto_structure/?_return_path=/admin/seo/site/structures/" title="Генерация страниц"><span class="aicons reload">&nbsp;</span> Генерация страниц</a>';
}

function cms_prefilter($row) {
        global $type;
        
        $row['url'] = (!empty($row['link'])) ? LANGUAGE_URL.substr($row['link'], strpos($row['link'], '/', 1) ) : $row['link'];   
        $row['url'] = trim($row['url'], '/');
        if($row['active'] == 0){
            $row['url'] = "<font color=silver>$row[url]</font>";
            $row['name'] = "<font color=silver>$row[name]</font>";
            $row['view_page'] = "";
        }
        else{
            $row['view_page'] = "<a href='http://$row[link]/' target=\"_blank\"  title=\"Перейти на страницу\"><i class=\"aicons views\">&nbsp;</i></a>";
        }
	$row['url'] = "<a href='?type=$type&group_id=$row[id]'>$row[image] $row[url]</a>";
        
	
	return $row;
}


$title = $DB->result("SELECT count(*) FROM seo_structure WHERE 1 ");

$where = '';
if( $type == 'structure' ){
    $where .=  " and `table_id`<>'2807' and table_id <> '2796'  ";
}
elseif ($type == 'shop' ){
    if(empty($group_id)){$group_id=1;}
    $where .=  " and (`table_id` = '2807' or table_id = '2796' ) ";
}


$parent_group_id = $DB->result("select group_id from seo_structure where id='$group_id'");

//structure site, avtopage
$query = "
	SELECT
		tb_structure.id,
		tb_structure.priority,
                html_editor(tb_structure.id, 'seo_structure', 'content_".LANGUAGE_SITE_DEFAULT."', '<span class=\"aicons add\">&nbsp;</span>') as content,	
                tb_structure.url as link,    
                IF(
                    (SELECT COUNT(id) FROM seo_structure WHERE `group_id`=tb_structure.id) > 0, 
                    '<span class=\"folder\">&nbsp;</span>',
                    '<span class=\"ie_style\">&nbsp;</span>'
                ) AS image,
		tb_structure.is_index, tb_structure.is_sitemap, 
                tb_structure.page_priority,
                tb_structure.change_frequency,
		tb_structure.name_".LANGUAGE_SITE_DEFAULT." AS name,
                tb_structure.active    
	FROM seo_structure AS tb_structure
	WHERE tb_structure.group_id = '$group_id' $where  
	ORDER BY tb_structure.priority ASC
";

$cmsTable = new cmsShowView($DB, $query);

$cmsTable->setParam('prefilter', 'cms_prefilter');
$cmsTable->setParam('title', 'Всего страниц '.$title);
$cmsTable->setParam('parent_link', "/admin/seo/site/structures/?type=$type&group_id=$parent_group_id");
$cmsTable->addColumn('url', '20%');
$cmsTable->addColumn('view_page', '5%', 'center', 'View');
$cmsTable->addColumn('name', '50%', 'left', 'Заголовок');
$cmsTable->addColumn('content', '15%', 'center');


$cmsTable->addColumn('page_priority', '5%', 'center');

$cmsTable->addColumn('is_index', '5%', 'center');
$cmsTable->setColumnParam('is_index', 'editable', true);

$cmsTable->addColumn('is_sitemap', '5%', 'center');
$cmsTable->setColumnParam('is_sitemap', 'editable', true);

$TmplContent->set('cms_site', $cmsTable->display());
unset($cmsTable);

?>