<?php
/**
* SEO модуль, шаблоны заголовков
*
* @package Pilot
* @subpackage Seo
* @version 1.0
* @author Naumenk A.
* @copyright Copyright 2013, c-format.
*/

//Seo::generateMetaTag(0, 202);



$query = "
	SELECT 
		tb_seo.id,
		tb_seo.name,		
		tb_seo.uniq_name,
		tb_seo.content_".LANGUAGE_CURRENT." as content,
		tb_structure.name_".LANGUAGE_CURRENT." as structure,
		tb_seo.active,
                tb_type.name as type_page,
               IF(tb_type.table_id <> '2797',
                    CONCAT('<a href=\"/action/admin/seo/auto_metatag/?_return_path=/admin/seo/seotemplate/&id=', tb_seo.id ,'\">[применить]</a>'),
                    ''
              ) as generate
	FROM seo_template as tb_seo
        left join seo_type as tb_type on tb_type.id=tb_seo.type_id	
        left join seo_structure as tb_structure on tb_structure.id=tb_seo.structure_id
        ORDER BY tb_structure.id, tb_seo.type_id
";

$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('subtitle', 'structure');
$cmsTable->addColumn('id', '5%', 'center', 'ID');
$cmsTable->addColumn('name', '30%', 'left', 'Название');
//$cmsTable->addColumn('structure', '20%', 'left', 'Раздел');
$cmsTable->addColumn('type_page', '30%', 'left', 'Тип страницы');
$cmsTable->addColumn('uniq_name', '10%', 'center');
$cmsTable->addColumn('content', '30%', 'left');
$cmsTable->addColumn('generate', '10%', 'center', 'Сгенерировать');
$cmsTable->addColumn('active', '5%');
$cmsTable->setColumnParam('active', 'editable', true);


$TmplContent->set('cms_table', $cmsTable->display());
unset($cmsTable);

$data = $DB->query("
		SELECT 
			tb_seo.name_".LANGUAGE_CURRENT." as name, tb_group.uniq_name, tb_group.name as title
		FROM seo_words as tb_seo
		INNER JOIN seo_words_group as tb_group ON tb_group.id = tb_seo.group_id		
		GROUP BY tb_group.id		
		ORDER BY tb_group.priority
		
	");

$TmplContent->iterateArray('/params/', null, $data);