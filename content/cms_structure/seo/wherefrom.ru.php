<?php
/** 
 * Откуда пришел пользователь
 * @package Pilot 
 * @subpackage Seo
 * @author Naumenko A.
 * @copyright c-format, 2014
 */ 


$query = "
	SELECT 
		tb_m.id,
		tb_m.uniq_name,
		tb_m.company,
		tb_m.clicks,
		tb_m.comment
	FROM seo_wherefrom as tb_m
        where tb_m.`uniq_name`<>'default'
	ORDER BY tb_m.uniq_name
";
//x($query);

$cmsTable = new cmsShowView($DB, $query);

$cmsTable->setParam('add', false);
//$cmsTable->setParam('excel', false);
$cmsTable->setParam('subtitle', 'uniq_name');
$cmsTable->addColumn('uniq_name', '20%');
$cmsTable->addColumn('company', '20%');
$cmsTable->addColumn('comment', '30%');
$cmsTable->addColumn('clicks', '10%', 'center', 'К-ство заходов');
//$cmsTable->addColumn('r_num', '10%', 'center', 'Повторные заходы');
echo $cmsTable->display();
unset($cmsTable);
