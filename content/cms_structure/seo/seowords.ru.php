<?php
/**
* SEO модуль, словосочетания для генерации заголовков
*
* @package Pilot
* @subpackage Seo
* @version 1.0
* @author Naumenk A.
* @copyright Copyright 2013, c-format.
*/

$group_id = globalVar($_GET['group_id'], 0);

	
if($group_id) {
	
	$query = "
		SELECT 
			tb_seo.id,
			tb_seo.name_".LANGUAGE_CURRENT." as name,				
			tb_seo.priority,
			tb_seo.active
		FROM seo_words as tb_seo
		WHERE tb_seo.group_id='$group_id'
		ORDER BY tb_seo.priority
		
	";
	
	$cmsTable = new cmsShowView($DB, $query);
        
	$cmsTable->setParam('show_parent_link', true);
	$cmsTable->setParam('parent_link', '/admin/seo/seowords/?');
        
	$cmsTable->addColumn('name', '80%');	
	$cmsTable->addColumn('active', '5%');
	$cmsTable->setColumnParam('active', 'editable', true);
	
        $TmplContent->set('cms_table', $cmsTable->display());	
	unset($cmsTable);

}
else {
    
    function cms_filter($row) {
                global $DB;
                
		$row['name'] = "<a href='?group_id=$row[id]'>$row[name]</a>";
                
                $data = $DB->fetch_column("SELECT id, name_".LANGUAGE_CURRENT." as name FROM seo_words WHERE group_id='{$row['id']}' ORDER BY priority");
                
                $row['content'] = implode(', ', $data);
                
		return $row;
	}	

	$query = "
		SELECT 
			tb_seo.id,
			tb_seo.name,			
			tb_seo.uniq_name,			
			tb_seo.priority,
			tb_seo.active
		FROM seo_words_group as tb_seo		
		ORDER BY tb_seo.priority
		
	";
	
	$cmsTable = new cmsShowView($DB, $query);
	$cmsTable->setParam('prefilter', 'cms_filter');
        
	$cmsTable->addColumn('name', '30%');
	$cmsTable->addColumn('uniq_name', '20%');	
	$cmsTable->addColumn('content', '20%', 'center', 'Словосочетания');	
	$cmsTable->addColumn('active', '5%');
	$cmsTable->setColumnParam('active', 'editable', true);
	
	
	$TmplContent->set('cms_table', $cmsTable->display());	
	unset($cmsTable);
    
}