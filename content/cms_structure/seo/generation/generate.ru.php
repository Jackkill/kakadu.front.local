<?php
/**
 * Правила формирования страниц
 * @package SEO
 * @version 1.0
 * @author Naumenko A.
 * @copyright c-format, 2014
 */


echo '<a class="btn btn-grey" style="position:absolute;top:15px;left:50%;margin-left:-20px;" href="/action/admin/seo/auto_structure/?_return_path=/admin/seo/site/structures/" title="Генерация страниц"><span class="aicons reload">&nbsp;</span> Генерация страниц</a>';
/*
function cms_prefilter($row) {
        global $types;
    
        if(!$row['active']){
            $row['url'] = "<span style='color:grey;'>$row[url]</span>";
        }
	return $row;
}

$query = "
	SELECT 
            tb_seo.id, tb_seo.structure_id, tb_seo.active, tb_site.url, tb_seo.name, 
            tb_seo.priority,
            tb_seo.is_index, tb_seo.is_sitemap, tb_seo.page_priority
	FROM seo_type as tb_seo        
        left JOIN site_structure as tb_site on tb_site.id=tb_seo.structure_id
	ORDER BY tb_seo.priority
";


$cmsTable = new cmsShowView($DB, $query);

$cmsTable->setParam('prefilter', 'cms_prefilter');

$cmsTable->addColumn('name', '40%', 'left');
$cmsTable->addColumn('url', '30%', 'left', 'Прикреплен к странице');
$cmsTable->addColumn('is_index', '5%', 'center');
$cmsTable->setColumnParam('is_index', 'editable', true);

$cmsTable->addColumn('is_sitemap', '5%', 'center');
$cmsTable->setColumnParam('is_sitemap', 'editable', true);

$cmsTable->addColumn('page_priority', '5%', 'center');

$cmsTable->addColumn('active', '5%', 'center');
$cmsTable->setColumnParam('active', 'editable', true);

echo $cmsTable->display();
unset($cmsTable);

*/
?>