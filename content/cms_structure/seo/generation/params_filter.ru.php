<?php

/* 
 * Настройка параметров фільтра
 * @package Pilot
* @subpackage Seo
* @version 1.0
* @author Naumenkо A.
* @copyright Copyright 2014, c-format.
 */

echo '<a class="btn btn-grey" style="position:absolute;top:15px;left:50%;margin-left:-20px;" href="/action/admin/seo/auto_filter/?_return_path=/admin/seo/site/structures/"> <span class="aicons reload">&nbsp;</span> Генерация страниц фильтра</a>';

        $query = "
		SELECT 
			tb_param.id,
			tb_param.name_".LANGUAGE_CURRENT." as name,
			tb_group.name_".LANGUAGE_CURRENT." as group_name,
			tb_param.is_index
		FROM shop_group_param as tb_param
                INNER JOIN shop_group as tb_group on tb_group.id=tb_param.group_id
                WHERE tb_param.is_filter = '1'
		ORDER BY tb_group.priority
		
	";
	
	$cmsTable = new cmsShowView($DB, $query);
	//$cmsTable->setParam('prefilter', 'cms_filter');
        
        $cmsTable->setParam('priority', false);
        $cmsTable->setParam('subtitle', 'group_name');
        $cmsTable->setParam('add', false);
        $cmsTable->setParam('delete', false);
        $cmsTable->setParam('edit', false);
        $cmsTable->setParam('excel', false);
        
        
	$cmsTable->addColumn('group_name', '10%', 'left', 'Каталог');
	$cmsTable->addColumn('name', '30%', 'left', 'Фильтр');
	//$cmsTable->addColumn('uniq_name', '20%');	
	//$cmsTable->addColumn('content', '20%', 'center', 'Словосочетания');	
	$cmsTable->addColumn('is_index', '5%');
	//$cmsTable->addColumn('type_filter', '5%');
	$cmsTable->setColumnParam('is_index', 'editable', true);
	echo $cmsTable->display();
	unset($cmsTable);