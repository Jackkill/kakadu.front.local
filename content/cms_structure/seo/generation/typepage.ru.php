<?php
/**
 * Типы страниц
 * @package SEO
 * @version 1.0
 * @author Naumenko A.
 * @copyright c-format, 2014
 */

$query = "
	SELECT 
            tb_seo.id, tb_seo.name,tb_seo.priority, tb_table.name as table_name
	FROM seo_type as tb_seo
        left join cms_table as tb_table on tb_table.id=tb_seo.table_id
	ORDER BY tb_seo.priority
";


$cmsTable = new cmsShowView($DB, $query);
$cmsTable->setParam('show_filter', false);
$cmsTable->addColumn('name', '30%', 'left');
$cmsTable->addColumn('table_name', '30%', 'left', 'Таблица');

echo $cmsTable->display();
unset($cmsTable);