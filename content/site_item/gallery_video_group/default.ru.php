<?php
/**
 * Страница ВидеоГалереи
 * @package Pilot
 * @subpackage Gallery
 * @author Naumenko A.
 * @copyright c-format, 2015
 */

$page_start = globalVar($_GET['_page'], 0);
$limit = GALLERY_VIDEO_PAGE_COUNT;

$group_id = $Site->item_object_id;

$gallery = new Gallery( 'gallery_video_group', $group_id );
    
$video = $gallery->getVideos(true, ' AND show_item = 1 ', $page_start, $limit);

if( $gallery->total == 1 ){ 
    $Template = new Template('gallery/video');    
    $Template->set('video', $video[0]);
    $TmplContent->set('videoTmpl', $Template->display());
}
elseif ( $gallery->total > 1 ) {
    
    $TmplContent->iterateArray('/video/', null, $video); 
    
    $data = $gallery->getGroups( 1 );    
    if ( count($data) > 1 ){
        // Список всех групп, если групп больше одной
        $TmplContent->set('show_groups', true);
        $TmplContent->setGlobal('currentGroup', $Site->item_object_id );
        $TmplContent->iterateArray('/groups/', null, $data);
    }
    
    //список видео файлов     
    $misc_url = str_replace('//', '/', '/'.$_GET['_REWRITE_URL'].'/page-{$offset}/');
    $TmplContent->set('pages_list', Misc::pages($gallery->total, $limit, 10, $misc_url, false, true, true)); 
}    

