<?php

/**
 * Страница галереи
 * @package Pilot
 * @subpackage Gallery
 * @author Naumenko A.
 * @copyright c-format, 2014
 */
  
$gallery_id = $Site->item_object_id;

$gallery = new Gallery('gallery_group', $gallery_id);
$data = $gallery->getGroups( );

if ( count($data) > 0 ){

	// Список всех групп	
	$TmplContent->set('show_groups', true);

        //$data = array_merge($data, $data, $data, $data, $data);
        //размеры
        $styles = array(1, 3, 2, 3, 2, 4, 2, 3, 1, 3,  4, 2);
        if(count($data) == 8 ) {$styles[6] = 4;$styles[7] = 2;}
        
        reset($data);
        while( list($key, $row)=each($data) ){
            $index = $key % count($styles);
            $row['items'] = $styles[$index];
            $row['url'] = '/'. LANGUAGE_URL . 'gallery/'.$row['uniq_name'].'/';

            $TmplContent->iterate('/gallery/', null, $row);
        }
	
        $misc_url = str_replace('//', '/', '/'.$_GET['_REWRITE_URL'].'/page-{$offset}/');
        $TmplContent->set('pages_list', Misc::pages($gallery->total, 10, 10, $misc_url)); 
}
else{

	$type = $gallery->getGroupInfo();
	$photos = $gallery->getPhotos(100, 0);	
	//x($photos);
        
        $TmplContent->set( $type );
        //$TmplContent->iterateArray('/gallery/', null, $photos );
        
         //размеры
        $styles = array(5, 3, 2, 5, 3, 2, 3, 5, 3, 5);
        reset($photos);
        while( list($key, $row)=each($photos) ){
            $index = $key % count($styles);
            $row['items'] = $styles[$index];
            
            $TmplContent->iterate('/gallery/', null, $row);
        }
	
	//изображения галлереи
//	$Template = new Template('gallery/gallery');		
//	$Template->set( $type );
//	$Template->iterateArray('/gallery/', null, $photos );
//	$TmplContent->set('gallery', $Template->display() ) ;
//
//	//информация о группе галереи		
//	$TmplDesign->set('headline', $type['headline']);
//	$TmplDesign->set('title', $type['title']);
//	$TmplDesign->set('keywords', $type['keywords']);
//	$TmplDesign->set('description', $type['description']);
//	
//	//радительские группы
//	$data = $gallery->getGroups( $type['group_id']);
	
} 

