<?php	

/*
 * Страница новостей по умолчанию
 */

$id = $Site->item_object_id;
$page_start = globalVar($_GET['_page'], 0);
    
$TmplDesign->set('show_rightside', false);

$News = new News();
$type = $News->getType($id);
$TmplContent->set( $type );

//list groups
//$child_types = $News->getChildTypes( $id, true, true );
//$TmplContent->set('show_types', count($child_types));    
//foreach ( $child_types as $row){
//    if ($id == $row['id']) {$row['active'] = true; }
//    $TmplContent->iterate('/types/', null, $row);    
//}

$limit = NEWS_PAGE_COUNT;

//list messages
$data = $News->getHeadlines($limit, $id);
$TmplContent->iterateArray('/news/', null, $data);


//if($News->total > $limit){
//    $count = ($News->total > $limit*2 ) ? $limit : $News->total - $limit;
//    $TmplContent->set('show_more', $count); 
//    $TmplContent->set('type_id', $id); 
//}

$misc_url = str_replace('//', '/', '/'.$_GET['_REWRITE_URL'].'/page-{$offset}/');
$TmplContent->set('pages_list', Misc::pages($News->total, $limit, 10, $misc_url, false, true)); 

?>