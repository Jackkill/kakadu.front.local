<?php
/**
 * Страница ВидеоГалереи
 * @package Pilot
 * @subpackage Gallery
 * @author Naumenko A.
 * @copyright c-format, 2014
 */
  
$video_id = $Site->item_object_id;

    
$video = Gallery::getVideoFile($video_id);

$Template = new Template('gallery/video');    
$Template->set('video', $video[0]);
$TmplContent->set('videoTmpl', $Template->display());

if(Auth::isAdmin()){
        
        $Adminbar->addButton('cms_edit', $Site->item_table_name, $Site->item_object_id, "Параметры", 'edit.gif');   
        $Adminbar->addButton('editor', $Site->item_table_name, $Site->item_object_id, 'Редактирование', 'word.gif');    
}
