<?php
/** 
 * Обработчик шаблона для сайта, используемый по умолчанию.
 * Данный шаблон используется как "скелет" для нового дизайна сайта.
 * require_once этого щаблона - запрещено!
 * 
 * @package Pilot
 * @subpackage Site
 * @author Naumenko A.
 * @copyright c-format, 2014
 */ 


header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Expires: -1");
header("Last-Modified: " . gmdate("D, d M Y H:i:s")." GMT");
header('Cache-Control: no-store, no-cache, must-revalidate');
header("Cache-Control: post-check=0,pre-check=0", false);
header("Cache-Control: max-age=0", false);
header('Pragma: no-cache');


if (!defined('DESIGN_URL')){
    define('DESIGN_URL', '/design/kakadu/');
}

$isMainPage = $Site->isMainPage();

/* Путь к странице */
$TmplDesign->set("breacrumbs",   $Site->getTemplatePath());
$TmplDesign->set("isMain",       $isMainPage );
$TmplDesign->set("structure_id", $Site->structure_id );


/* -----Блок из меню ------*/
$topMenu = $Site->getMenu(-1, 'top_menu', 2);
$TmplDesign->iterateArray('/topMenu/', null, $topMenu);


if ( AwardsSeason::isLogining() ) {
    
    //вход и регистрация
    if ( Auth::isLoggedIn() ) {
            $cabinet_data = array(
                0 => array('url' => '/'.LANGUAGE_URL.'user/works/', 'name'=>cmsMessage::get('USER', 'MSG_USER_CABINET')),
                1 => array('url' => '/'.LANGUAGE_URL.'action/cms/logout/', 'name'=>cmsMessage::get('USER', 'MSG_USER_LOGOUT'))
            );
    } else {
            $cabinet_data = array(
                0 => array('url' => '/'.LANGUAGE_URL.'user/login/', 'name'=>cmsMessage::get('USER', 'MSG_USER_ENTER_PRIVATE'))
            );
            if ( AwardsSeason::isTake() ) {
                $cabinet_data[1] = array('url' => '/'.LANGUAGE_URL.'user/register/', 'name'=>cmsMessage::get('USER', 'MSG_USER_REGISTRATION'));
            }
    }   

    $TmplDesign->iterateArray('/login_menu/', null, $cabinet_data);
}

