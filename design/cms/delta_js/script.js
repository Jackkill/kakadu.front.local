$(function() {
    Hotkey.Init();
    FormFocus();

    for(var i = 0; i < document.links.length; i++){
            document.links.hidefocus = true;
    }
				
    $('.cms_view').tableDnD({
            dragHandle: '.sort',
            onDragClass: 'dragClass',
            onDrop: function (table, row) {
                    AjaxRequest.send($(table).parents('form.cms_view_form:first').attr('id'), '/action/admin/cms/table_sort/', 'Сохранение', true, {})
            }
    });
    cmsView.init();
				
    $(".chzn-select").chosen({
            allow_single_deselect:true, 
            placeholder_text_multiple: 'Выберите несколько значений', 
            placeholder_text_multiple: 'Выберите одно из значений',
            no_results_text: 'Ничего не найдено'
    }).change(function(){ $(this).trigger('click');}); 
				
    $(".nav-select").chosen({disable_search_threshold: 30});
    
    toastr.options = { "closeButton": true, "timeOut": "3000" }
				
    $('input[type=checkbox].delta-checkbox').iCheck({
            checkboxClass: 'icheckbox_futurico',
            radioClass: 'iradio_futurico',
            increaseArea: '20%'
    });
				
    $(document).ready(function(){     
        
        var cookie_left_menu = "left_menu";
        $("body").on("click", ".mainBlocksCover", function(){
            $(this).find("a").trigger("click");
            return false;

        });
        
        $("body").on("click", ".innerBlocks .nolink", function(){
            $(this).parent().find('.innerBlocksValue').slideToggle();
            $(this).parent().find('.square').toggleClass('changed');
            return false;
        });
        
        $("body").on("click", ".mainBlocksCover>a, .innerBlocksValue>a", function(e){
            var elem_id = $(this).attr("elem_id");
            if ( elem_id ){
                var cookie_value = getCookie(cookie_left_menu);
                var num = "^" + String(elem_id) + "^";
                
                if (!cookie_value) cookie_value = "";
                
                var ind = cookie_value.indexOf(num);
                if(ind == -1) {
                    cookie_value += num;
                    cookie_value = cookie_value.replace("^^", "^");	                    
                }
                else {
                    cookie_value = cookie_value.replace(num, "^"); //если такая кука уже есть 
                    if ( $(this).hasClass("mainBlocksText") ) var a_lvl = $(this).parent().parent();
                    else var a_lvl = $(this).parent();
                    a_lvl.find("a").each(function(){
                        elem_id = $(this).attr("elem_id");                       
                        if ( elem_id ) cookie_value = cookie_value.replace("^" + String(elem_id) + "^", "^");
                    })
                    if ( cookie_value == "^" ) cookie_value = "";
                }
                setCookie(cookie_left_menu,cookie_value,0,"/");
            }
            
            //if first lvl
            if ( $(this).parent().hasClass("mainBlocksCover") ){
                $(this).parents(".mainBlocks").find('.innerBlocks').slideToggle(500);
                $(this).parent().find('.mainBlocksIco').toggleClass('changed');                
            }
            
            if ( $(this).attr("href") != "javascript:void(0); " ){
                location.href = $(this).attr("href");
            }
            
            e.preventDefault();
            return false;
        })
        
        var cookie_value = getCookie(cookie_left_menu);
        $(".mainBlocksCover>a, .innerBlocksValue>a").each(function(){
            elem_id = $(this).attr("elem_id");
            if ( elem_id ){
                var num = "^" + String(elem_id) + "^";
                if (!cookie_value) cookie_value = "";
                if ( cookie_value.indexOf(num)!=-1 ) {
                    if ( $(this).parent().hasClass("hasChildren") ) {
                        $(this).find(".square").addClass("changed");
                        $(this).parent().children().show();
                    }
                    else {
                      $(this).parent().find(".mainBlocksIco").addClass("changed");
                      $(this).parents(".mainBlocks").find(".innerBlocks").show();  
                    }
                }
            }
        });

    });
    
        if( $("a.fancy, a[rel='lightbox-content'], .ui_gallery, a.fancyvideo").length ) {
		//fancy
		loadStyle("/css/extras/fancy/jquery.fancybox.css", function(){});		
		loadScript("/js/jquery/jquery.fancybox.js", function(){	 
			$("a.fancy, a[rel='lightbox-content']").fancybox();	
		});
		
	}
        
});



/* загрузка скриптов */
function loadScript(url, callback){

    var script = document.createElement("script")
    script.type = "text/javascript";

    if (script.readyState){  //IE
        script.onreadystatechange = function(){
            if (script.readyState == "loaded" ||
                    script.readyState == "complete"){
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {  //Others
        script.onload = function(){
            callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

/* загрузка стилей */
function loadStyle(url, callback){

    var link = document.createElement("link")
    link.type = "text/css";
    link.rel = "stylesheet";

    if (link.readyState){  //IE
        link.onreadystatechange = function(){
            if (link.readyState == "loaded" ||
                    link.readyState == "complete"){
                link.onreadystatechange = null;
                callback();
            }
        };
    } else {  //Others
        link.onload = function(){
            callback();
        };
    }

    link.href = url;
    document.getElementsByTagName("head")[0].appendChild(link);
}