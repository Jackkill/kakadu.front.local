function alert_open( msg ){
    $('div.alert-danger > p').html(msg);
    $('div.alert-danger:not(.alert-fade)').show();
    return;
}

function choosen_updateAll(){
    $("#params .chosen-select").chosen({no_results_text: "Упс, не найдено."});
}

function alf_table( city_from, country, city, date, nights, tour ){
    $('#alf_table tr').remove();
    $('#tour_form').jqmShow();
    $('#tour_form .modal-title').append('<i class="icon-spinner icon-spin"></i>');
    AjaxRequest.send('', '/actions_admin.php', '', true, { 
        _event: 'maillist/alf_tours', date: date, city_from: city_from, country: country, city: city, nights: nights, tour: tour } );
}

$(document).ready(function(){ 
  
  $(".modal").show();
  $(".chosen-select").chosen({no_results_text: "Упс, не найдено."});
  $(".modal").hide();
  
  $('#choose').live('click',function(){
      alf_table( $('#city_from').val(), $('#country').val(), $('#city').val(), $('#date').val(), $('#nights').val(), $('#tour').val()  );
      //CKEDITOR.instances['mail_tinymce'].getData()
  })
  
  $('#send').live('click',function(){
      $('#final_message').val(CKEDITOR.instances['mail_tinymce'].getData());
	  
	  $('#mail_html').html( $('#final_message').val() );
	  
	  $('#mail_html').append('\
	  <map name="head">\
	  <area shape="rect" alt="Написать письмо" title="Написать письмо" coords="458,28,572,67" href="http://alf-ua.com/send_mail/" target="_blank" />\
	  <area shape="rect" alt="Наши консультанты" title="Наши консультанты" coords="458,74,571,110" href="http://alf-ua.com/contacts/" target="_blank" />');
	  

	  $('#mail_html').append('\
	  <map name="foot">\
	  <area shape="rect" alt="Отписаться" title="Отписаться" coords="118,95,231,104" href="http://alf-ua.com/action/maillist/unsubscribe/" target="_blank" />');
	  	  
	  $('#mail_html img:first').attr("usemap","#head");
	  $('#mail_html img:last').attr("usemap","#foot");
	  $('#final_message').val( $('#mail_html').html() );	  
	  
      $('#final_categories').find('option').remove();
      $('#categories option:selected').each(function(){
          $('#final_categories').append( $(this).clone() );
      });
	  $('#test').val(0);
      //$('#final_categories').find('option').attr('selected',true);
      AjaxRequest.form('mail_default', '', {});
  });
  
  $('#test_send').live('click',function(){
      $('#final_message').val(CKEDITOR.instances['mail_tinymce'].getData());
	  
	  $('#mail_html').html( $('#final_message').val() );
          
	  $('#mail_html').append('\
	  <map name="head">\
	  <area shape="rect" alt="Написать письмо" title="Написать письмо" coords="458,28,572,67" href="http://alf-ua.com/send_mail/" target="_blank" />\
	  <area shape="rect" alt="Наши консультанты" title="Наши консультанты" coords="458,74,571,110" href="http://alf-ua.com/contacts/" target="_blank" />');
	  

	  $('#mail_html').append('\
	  <map name="foot">\
	  <area shape="rect" alt="Отписаться" title="Отписаться" coords="118,95,231,104" href="http://alf-ua.com/action/maillist/unsubscribe/" target="_blank" />');
	  	  
	  $('#mail_html img:first').attr("usemap","#head");
	  $('#mail_html img:last').attr("usemap","#foot");
          //$('#mail_html>div')
          
          
	  $('#final_message').val( $('#mail_html').html() );
	  
      $('#final_categories').find('option').remove();
      $('#categories option:selected').each(function(){
          $('#final_categories').append( $(this).clone() );
      });
      $('#test').val(1);
      //console.log( $('#final_message').val() );
      AjaxRequest.form('mail_default', '', {});
  });  
  
  $('#table_choose_countries').live('click',function(){
      //$('#mail_tinymce').html(CKEDITOR.instances['mail_tinymce'].getData());
      
      $('#mail_html').html(CKEDITOR.instances['mail_tinymce'].getData());
      
      var t = '';
      $('#alf_table input:checked').each(function(){
          var input_id = $(this).attr('id');
          
          t += "<div style='color: grey; font-weight: normal;'>" + $('label[for="' + input_id + '"]').html() + "</div>";
      })
      
	  var city_from_orfo = $('#city_from').find('option:selected').html();
	  if ( city_from_orfo == 'Одесса' ) city_from_orfo = 'Одессы';
	  else if ( city_from_orfo == 'Запорожье' ) city_from_orfo = 'Запорожья';
	  else city_from_orfo += 'а';
	  
	  t_add = '';
	  if ( $("#mail_html #countries p").length ) t_add = 'border-top: 1px solid #748bbf;';
	  
	  var t_wrap ='<div style="' + t_add + 'border-bottom: 1px solid #748bbf;padding: 10px 0px;">'+
		'<div style="color: #214c83; font-weight: bold;font-family: tahoma;font-size:16px;">'+
		$('#country').find('option:selected').html()+
		', ' +  $('#city').find('option:selected').html()+ ' из ' +
		city_from_orfo +
		'</div>'+
		'<div style="color:gray;font-weight: bold;"><b style="text-decoration: underline;">Наши консультанты:</b></div>'+
                '<div style="font-size: 14px; color:#214c83;font-weight: bold; text-decoration: underline; margin-top: 10px; margin-bottom: 10px;"> Вылет на ' + $('#date').val() + 
                '. Количество ночей: ' + $('#nights').val() + '</div>'+
		t +
		'</div>';
                
		
	  
	  if ( $("#mail_html #countries p").length ) $("#mail_html #countries").html( t_wrap );
      else $("#mail_html #countries").append( t_wrap );
	  
	  AjaxRequest.send('', '/actions_admin.php', '', true, { 
        _event: 'maillist/params/alf_manager', country: $('#country').val() } );	  
      
      //console.log( $("#mail_html #countries").html() );
      
      CKEDITOR.instances['mail_tinymce'].setData( $('#mail_html').html() );
      
      $('#tour_form').jqmHide();
  })
  
  $('#all').click(function(){  
      if ( $('ul.list_emails input:checkbox').length > $('ul.list_emails input:checkbox:checked').length )
          $('ul.list_emails input:checkbox').attr('checked',true);
      else $('ul.list_emails input:checkbox').attr('checked',false);  
  });
  
  $('#mail').click(function(){
      if ( !$('ul.list_emails input:checked').length ){
          alert_open('Вы не ввели e-mail адреса.');
          return false;
      };
      
      $(".alert-fade").hide();
      
      $('div.users').html('');
      $('ul.list_emails input:checked').each(function(){
          $('div.users').append( $(this).clone() );
      })
      $('div.users input').attr("checked", true);
      
      $('#mail_form').jqmShow();
      document.getElementById("form_mail").reset();
      $('select').trigger("chosen:updated");
  });
  
  $('#stop').click(function(){
      if ( !$('ul.list_emails input:checked').length ){
          alert_open('Вы не ввели e-mail адреса.');
          return false;
      };
      
      $(".alert-fade").hide();
      
      $('div.users').html('');
      $('ul.list_emails input:checked').each(function(){
          $('div.users').append( $(this).clone() );
      })
      $('div.users input').attr("checked", true);
      
      $('#unmail_form').jqmShow();
      document.getElementById("form_unmail").reset();
      $('select').trigger("chosen:updated");      
      
      //$("#form_mailstop").submit();
  });  
  
  $('button[data-dismiss="modal"]').live('click',function(){
    $(this).parents('.modal').jqmHide();
  });
  
  $('button[data-dismiss="alert"]').live('click',function(){
    $(this).parents("div.alert").hide();
  });  
  
  $( 'textarea#mail_tinymce' ).ckeditor({
    toolbar: null,
    toolbarGroups : null,
    height: '600px'
  });
  
  $('#mail_params').change(function( eventData ){
      
      var object_id = eventData.target.id;
      $('#' + object_id).prevAll('b').first().append('<i class="icon-spinner icon-spin"></i>');
      
      //clear all next select
      $('#' + object_id).nextAll('select').prop('selectedIndex',0);
      
      AjaxRequest.form('mail_params', '', {});
  })

})