function checked_reg_form(form) {
    var error = true;
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,6})$/;
    var reg1 = /[а-яА-Я ]/;  
    $(".all_text").show();
    $(".form_error").hide();
    //$("#form_error").html('Заполните, пожалуйста, все поля');
    form.find('input.required').each(function(){
        $(this).parent().find('.error').removeClass('error');
        if($(this).val() == '') {
            $(this).addClass('error');
            $(this).parent().find('label').addClass('error');
            error = false;
        }
    });
    
    var email = form.find('.user_email').val();
    if(reg.test(email) == false) {
        form.find('.user_email').addClass("error");
        form.find('.user_email').parent().find('label').addClass("error");
        error = false;
        
    }
  
    return error;
}/*
function check_reg_form() {
  var error = '';
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  var reg1 = /[а-яА-Я ]/;
  $("#all_text").show();
  $("#form_error").hide();
  $("#form_error").html('Заполните, пожалуйста, все поля');
  fields = ['email', 'pass', 'repass', 'phone', 'organization_name', 'city',  'address', 'person_reger'];
  for(elem in fields) {
    $("#" + fields[elem]).removeClass('error');
    $("#" + fields[elem]).parent().find('label').removeClass('error');
    if($("#" + fields[elem]).val() == '') {
      $("#" + fields[elem]).addClass('error');
      $("#" + fields[elem]).parent().find('label').addClass('error');
      error = 1;
    }
  }
  $("#email").removeClass('error');
  $("#email").parent().find('label').removeClass('error');
  if(reg.test($("#email").val()) == false) {
    $("#email").addClass("error");
    $("#email").parent().find('label').addClass("error");
    error = 1;
  }
  $("#repass").removeClass('error');
  $("#repass").parent().find('label').removeClass('error');
  if($("#repass").val() != $("#pass").val()) {
    error = 2;
    $("#repass").addClass('error');
    $("#repass").parent().find('label').addClass('error');
  }
  if($("#repass").val() == '') {
    error = 1;
    $("#repass").addClass("error");
    $("#repass").parent().find('label').addClass("error");
  }

  if(error == 0) {
    return true;
  } else {
    $("#all_text").hide();
    if(error == 2) {
      $("#form_error").html('Пароли не совпадают');
    }
    $("#form_error").show();
    return false;
  }
}
*/
function check_edit_profile(form){
    error = 0;
    $("#form_good").hide();
    $("#form_error").html('Заполните, пожалуйста, все поля');
  
   form.find('input.required').each(function(){
        $(this).parent().find('.error').removeClass('error');
        if($(this).val() == '') {
            $(this).addClass('error');
            $(this).parent().find('label').addClass('error');
            error = 1;
        }
    });
    /*
  fields = ['phone', 'organization_name', 'city', 'address', 'person_reger'];
  for(elem in fields) {
    $("#" + fields[elem]).removeClass('error');
    $("#" + fields[elem]).parent().find('label').removeClass('error');
    if($("#" + fields[elem]).val() == '') {
      $("#" + fields[elem]).addClass('error');
      $("#" + fields[elem]).parent().find('label').addClass('error');
      error = 1;
    }
  }*/
  
/*  if(type == '1'){
    $("#jur_address").removeClass('error');
    $("#jur_address").parent().find('label').removeClass('error');
    if($("#jur_address").val() == ''){
      error = 1;
      $("#jur_address").addClass('error');
      $("#jur_address").parent().find('label').addClass('error');
    }
  }
  
  $("#repass").removeClass('error');
  $("#repass").parent().find('label').removeClass('error');
  if($("#repass").val() != $("#pass").val()) {
    error = 2;
     $("#repass").addClass('error');
     $("#repass").parent().find('label').addClass('error');
  }*/


  if(error == 0) {
    return true;
  } else {
    if(error == 2) {
      $("#form_error").html('Пароли не совпадают');
    }
    $("#form_error").show();
    return false;
  }

}
/*
function check_reg_form_p() {
  var error = '';
  $("#all_text_p").hide();
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  var reg1 = /[а-яА-Я ]/;
  $("#form_error_p").hide();
  $("#form_error_p").html('Заполните, пожалуйста, все поля');
  fields = ['email_p', 'pass_p', 'repass_p', 'phone_p', 'name_p', 'city_p', 'address_p', 'person_reger_p'];
  for(elem in fields) {
    $("#" + fields[elem]).removeClass('error');
    $("#" + fields[elem]).parent().find('label').removeClass('error');
    if($("#" + fields[elem]).val() == '') {
      $("#" + fields[elem]).addClass('error');
      $("#" + fields[elem]).parent().find('label').addClass('error');
      error = 1;
    }
  }
  if(reg.test($("#email_p").val()) == false) {
    $("#email_p").addClass("error");
    $("#email_p").parent().find('label').addClass("error");
    error = 1;
  }
  if($("#repass_p").val() != $("#pass_p").val()) {
    error = 2;
    $("#repass_p").addClass("error");
    $("#repass_p").parent().find('label').addClass("error");
  }
  if($("#repass_p").val() == '') {
    error = 1;
    $("#repass_p").addClass("error");
    $("#repass_p").parent().find('label').addClass("error");
  }

  if(error == 0) {
    return true;
  } else {
    $("#all_text_p").hide();
    if(error == 2) {
      $("#form_error_p").html('Пароли не совпадают');
    }
    $("#form_error_p").show();
    return false;
  }
}*/

function change_category_ajax(group_id){
    AjaxRequest.send(null, '/action/kakadu/change_category/', '', true, { 'group_id': group_id });	
    return false;
}

function delete_bill(bill_id) {
    AjaxRequest.send(null, '/action/kakadu/delete_bill/', '', true, {'bill_id': bill_id });	
    return false;
}

function change_category(){
  cat = $("#cat").val();

  $("#form_error").hide();
  $("#description").removeClass('error');
  $("#description").parent().find('label').removeClass('error');
  fields = ['product', 'name', 'participiant_name', 'creative_head', 'idea_author', 'reklamodatel'];
  for(elem in fields) {
    $("#" + fields[elem]).removeClass('error');
    $("#" + fields[elem]).parent().find('label').removeClass('error');
  }

  $("#image_audio_file").hide();
  $("#video_file").hide();
  $("#link_name").html("Ссылка (опционально)");
  $("#video_name").html("загрузить видео (опционально)");
  //$("#file_link").hide();
  $("#label_image_mp3").html("загрузить изображение");
  $("#video_file").show();
  switch(cat){
      case '1':
      case '7':
        $("#video_name").html("загрузить видео");
        $("#video_file").show();
      break;

      case '2':
      case '3':
      case '5':
      case '8':
      case '9':
      case '11':
      case '13':
      case '14':
      case '15':
      case '17':
      case '18':
        $("#image_audio_file").show();
      break;

      case '4':
      case '10':
      case '16':
        $("#label_image_mp3").html("загрузить аудиролик");
        $("#image_audio_file").show();
      break;

      case '6':
      case '12':
      $("#link_name").html("Ссылка");
        $("#image_audio_file").show();
        $("#video_file").show();
       // $("#file_link").show();
      break;
  };
}

function check_add_work(){
    var form = $('#form_add_work');
    var error = true;
    
    $('#form_error').html('').hide();
    form.find('.required').each(function(){
        $(this).parent().find('.error').removeClass('error');
        if($(this).val() == '') {
            $(this).addClass('error');
            $(this).parent().find('label').addClass('error');
            
            error = false;
            //console.log(error);
        }
    });
    
    //files 
    form.find('.required_file').each(function(){
        $(this).find('.error').removeClass('.error');
        if ( $(this).find('.inputFile').val() == ''){
            $(this).find('label').addClass('error');
            $(this).find('input[type=text]').addClass('error');
            error = false;
        }
    });
    
    if ( $('#file_image').length > 0 ){
        var ident = $('#file_image');
        if(ident.val() != '' && !is_image(ident.val())){
            ident.parent().parent().find('input,label').addClass('error');
            error = false;
            $("#form_error").html("Допустимі тільки файли jpg!");
        }
    }
    
    if ( $('#file_audios').length > 0 ){
        var ident = $('#file_audios');
        if(ident.val() != '' && !is_music(ident.val())){
            ident.parent().parent().find('input,label').addClass('error');
            error = false;
            $("#form_error").html("Допустимі тільки файли mp3!");
        }
    }
    
    if ( $('#file_video').length > 0 ){
        var ident = $('#file_video').find('input');
        if(ident.val() != '' && !is_youtube(ident.val())){
            ident.parent().parent().find('input,label').addClass('error');
            error = false;
            $("#form_error").html("Допустимі посилання тільки на сервіс youtube!<br/>(Приклад: https://www.youtube.com/watch?v=TD4h9Fdn3-o)");
        }
    }
    
    if (!error){
        $('#form_error').show();
    } else {
        form.find("input[type=submit]").hide();
        $('#form_error').text('Збереження').show();
    }
    return error;
    
}

function check_add_work_OLD(typer){
  var error = 0;
  $("#form_error").hide();
  $("#description").removeClass('error');
  $("#description").parent().find('label').removeClass('error');
  $("#form_error").html('Заполните, пожалуйста, все поля');
  fields = ['product', 'name', 'participiant_name', 'creative_head', 'idea_author', 'reklamodatel'];
  for(elem in fields) {
    $("#" + fields[elem]).removeClass('error');
    $("#" + fields[elem]).parent().find('label').removeClass('error');
    if($("#" + fields[elem]).val() == '') {
      $("#" + fields[elem]).addClass('error');
      $("#" + fields[elem]).parent().find('label').addClass('error');
      error = 1;
    }
  }

  if($("#description").val() == ''){
    error = 1;
    $("#description").parent().find('label').addClass('error');
    $("#description").addClass('error');
  }
  cat = $("#cat").val();

  switch(cat){
      case '1':
      case '7':

        $("#fileName_video").removeClass('error');
        $("#fileName_video").parent().parent().find('label').removeClass('error');
        if($("#work_video_file").val() == '' && $('.input_video').length == 0){
          $("#fileName_video").addClass('error');
          $("#fileName_video").parent().parent().find('label').addClass('error');
          error = 1;
        }else if($("#work_video_file").val() != '' && !is_video($("#work_video_file").val()) ){
          $("#fileName_video").addClass('error');
          $("#fileName_video").parent().parent().find('label').addClass('error');
          error = 2;
          $("#form_error").html("Допустимы только файлы видео!");
        }

      break;

      case '2':
      case '3':
      case '5':
      case '8':
      case '9':
      case '11':
      case '13':
      case '14':
      case '15':
      case '17':
      case '18':
        $("#fileName_image").removeClass('error');
        $("#fileName_image").parent().parent().find('label').removeClass('error');
        if($("#work_file").val() == '' && $('.input_image').length == 0){
          $("#fileName_image").addClass('error');
          $("#fileName_image").parent().parent().find('label').addClass('error');
          error = 1;
        }else if($("#work_file").val() != '' && !is_image($("#work_file").val())){
          $("#fileName_image").addClass('error');
          $("#fileName_image").parent().parent().find('label').addClass('error');
          error = 2;
          //console.log(123);
          $("#form_error").html("Допустимы только файлы картинки!");
        }
      break;

      case '4':
      case '10':
      case '16':
        $("#fileName_image").removeClass('error');
        $("#fileName_image").parent().parent().find('label').removeClass('error');
        if($("#work_file").val() == '' && $('.input_image').length == 0){
          $("#fileName_image").addClass('error');
          $("#fileName_image").parent().parent().find('label').addClass('error');
          error = 1;
        }else if(!is_music($("#work_file").val())){
          $("#fileName_image").addClass('error');
          $("#fileName_image").parent().parent().find('label').addClass('error');
          error = 2;
          $("#form_error").html("Допустимы только файлы mp3!");
        }
      break;

      case '6':
      case '12':
       $("#link").removeClass('error');
       $("#link").parent().find('label').removeClass('error');
       if($("#link").val() == ''){
         error = 1;
         $("#link").parent().find('label').addClass('error');
         $("#link").addClass('error');
       }

       /*$("#fileName_video").removeClass('error');
        $("#fileName_video").parent().parent().find('label').removeClass('error');
        if($("#work_video_file").val() == ''){
          $("#fileName_video").addClass('error');
          $("#fileName_video").parent().parent().find('label').addClass('error');
          error = 1;
        }else if(!is_video($("#work_video_file").val())){
          $("#fileName_video").addClass('error');
          $("#fileName_video").parent().parent().find('label').addClass('error');
          error = 2;
          $("#form_error").html("Допустимы только файлы видео!");
        }
        $("#fileName_image").removeClass('error');
        $("#fileName_image").parent().parent().find('label').removeClass('error');
        if($("#work_file").val() == ''){
          $("#fileName_image").addClass('error');
          $("#fileName_image").parent().parent().find('label').addClass('error');
          error = 1;
        }else if(!is_image($("#work_file").val())){
          $("#fileName_image").addClass('error');
          $("#fileName_image").parent().parent().find('label').addClass('error');
          error = 2;
          $("#form_error").html("Допустимы только файлы картинки!");
        }*/
      break;
  };

  if(error == 0) {

    return true;
    setTimeout(function(){$("input[type='submit']").hide();},500);
  } else {
    $("#form_error").show();
    return false;
  }
}


function is_youtube(lnk){
   // var l = 'https://www.youtube.com/watch?v=28MgjQRRLOM&feature=youtu.be';
    var reg = /^(https:\/\/)?www\.youtube\.com\/watch\?v=([A-Za-z0-9\-&=\.\_])+$/;
	var reg_newtype = /^(https:\/\/)?youtu\.be\/([A-Za-z0-9\-&=\.\_])+$/;
    if(reg.test(lnk) == false && reg_newtype.test(lnk) == false) {
        return false;
    }
    return true;
}

function is_video(file_name){
  file_ext = file_name.split('.');
    ext_temp = file_ext[file_ext.length-1].split('?');
    ext = ext_temp[0];

    if(ext.toLowerCase() == 'mp4' || ext.toLowerCase() == 'avi' || ext.toLowerCase() == 'wmv' || ext.toLowerCase() == 'mkv' || ext.toLowerCase() == 'mpg' || ext.toLowerCase() == 'mov' || ext.toLowerCase() == 'vob' || ext.toLowerCase() == 'flv'){
    return true;
  }else{
    return false;
  }
}

function is_image(file_name){
    file_ext = file_name.split('.');
    ext_temp = file_ext[file_ext.length-1].split('?');
    ext = ext_temp[0];
    if(ext.toLowerCase() == 'jpg' || ext.toLowerCase() == 'jpeg'){
    return true;
  }else{
    return false;
  }
}

function is_music(file_name){
    file_ext = file_name.split('.');
    ext_temp = file_ext[file_ext.length-1].split('?');
    ext = ext_temp[0];
    if(ext.toLowerCase() == 'mp3'){
    return true;
  }else{
    return false;
  }
}
function clone_work(work_id){
  $("#work_id").val(work_id);
  $("#clone_work").css('visibility','visible');
}
$(document).ready(function(){
  change_category();
  if($("#goto_addwork")){
    $("#goto_addwork").bind('click',function(){
      document.location.href='/add_work';
    });
  }
});
