/* 
 * Функции и не только
 */
$(document).ready(function(){
     //fancybox
    if( $('.fancy-box').length > 0) {
        loadStyle("/design/santech/css/jquery.fancybox.css", function(){});
        loadScript("/js/jquery/jquery.fancybox.js", function(){	 
            if( $('.fancy-box').hasClass('video') )  { 
                $(".video").click(function() {                                
                        $.fancybox({
                                'padding'		: 0,
                                'autoScale'		: false,
                                'transitionIn'	: 'none',
                                'transitionOut'	: 'none',
                                'title'			: this.title,
                                'width'			: 1400,
                                'height'		:900,
                                'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
                                'type'			: 'swf',
                                'swf'			: {
                                'wmode'				: 'transparent',
                                'allowfullscreen'	: 'true'
                                }
                        });
                        return false;
                }); 
            }   
        });
    } 
});


 
$(window).load(function(){
    
    $('.setBlock__link').click(function(){
        
        if( $(this).hasClass('disabled') ){ return false; }
        
        var price = parseInt( $(this).attr('data-price') );
        var coeff = 1;
        
        if( $(this).hasClass('active')  ){
            coeff = -1;
        }
        
        $(this).toggleClass('active');
        
        var total = parseInt( $('#setPrice').text() ) + price * coeff;
        $('#setPrice').html(total);
        
        return false;        
    });
    
});

//добавление комплекта в корзину
function addSetToCart(){
    var products = [];
     $('.setBlock__link.active').each(function(){
            products.push( $(this).attr('data-id') );
     });
    //console.log(products);
    AjaxRequest.send('', '/action/shoporder/addset_to_cart/', '', true, {'products': products});
    return false;
}

//загрузка формы
function loadForm(form_name, product_id){    
    AjaxRequest.send('', '/action/form/load_form/', '', true, {'form_name': form_name, 'product_id': product_id});
    return false;
}

//удаление из просмотренных
function delRecently( product_id ){    
    AjaxRequest.send('', '/action/shop/del_from_recently/', '', true, {'product_id': product_id});
    
    return false;
}

function userInfo(){
    if( $('#userInform .my_hiiden').length > 0 ){
       $('#userInform .my_hiiden').show();$('body').addClass('fixed'); 
    }
    else {
        AjaxRequest.send('', '/action/user/userinfo/', '', true);
    }    
    return false;
}

function showHistory(order_id){
    AjaxRequest.send('', '/action/user/load_history/', '', true, {'order_id': order_id});
    return false;
}

function goodHistory(){
	var owl = $('.goodHistory'); owl.owlCarousel({items:3,loop:false,nav:true,autoplay:false});
}

function loadCollect(gallery_id){
    AjaxRequest.send('', '/action/shop/load_collection/', '', true, {'gallery_id': gallery_id});
    return false;
}
function formUpdate(  ){    
    // placeholder	
     $('.placedefault').each(function() {
          var default_value = this.value;
          $(this).focus(function() {
           if(this.value == default_value) {
                this.value = '';
           }
          });
          $(this).blur(function() {
           if(this.value == '') {
                this.value = default_value;    
           }
          });
    });
    
    //mask click
    $('.phone').click(function(){
        $(this).mask("+38(999) 999-99-99");
        return false;
    });
                
    if( $('.formLight select').length ) { 
        $('.formLight select').chosen( {  disable_search_threshold: 10,allow_single_deselect:true });
    } 
    
    //iCheck
    if ( $('.icheck').length > 0 ){
        if( ( $('input[type=radio]').length > 0 ) ){
            $('.icheck').iCheck({
                    checkboxClass: 'icheckbox_futurico',
                    radioClass: 'iradio_futurico',
                    increaseArea: '20%'
                });
        }
        else{
            loadScript("/js/jquery/icheck.js", function(){	 
                $('.icheck').iCheck({
                    checkboxClass: 'icheckbox_futurico',
                    radioClass: 'iradio_futurico',
                    increaseArea: '20%'
                });
            });
        }
    }
    
    $('form').submit(function(){  
        if ( $(this).attr('delta-ajax')=='on' ){ AjaxRequest.send( $(this) ); return false; }
    });
}
