var frameNavArr = [];

$(document).ready(function(){
    /* bx slider init */
    if($('.bxslider').length){
		$('.bxslider').bxSlider({
			pager: false,
			minSlides: 3,
			maxSlides: 3,
			slideWidth: 220,
			moveSlides: 1,
			slideMargin: 20,
            autoStart: false,
			auto: false,

		});
	}
    /* bx slider init end*/
    /* bx slider init */
	if($('.header-slider').length){
		$('.header-slider').bxSlider({
		    mode: 'fade',
		    auto: true,
            controls: false
		});
	}
    /* bx slider init end*/
    /* replace select with custom html */
	if($(".select").length){
	    $(".select").selectbox();
	}
    /* replace select with custom html end */
    /* header tabs */
    // Default action
    $(".tab_content").hide(); //Скрыть весь контент
	$("ul.tabs li:first").addClass("active").show(); //Активировать первую вкладку
	$(".tab_content:first").show(); //Показать контент первой вкладки
    // Handling an onclick event
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active"); //Удалить любой "active" класс
		$(this).addClass("active"); //Добавить "active" класс к выбранной вкладке
		$(".tab_content").hide(); //Скрыть контент всех вкладок
		var activeTab = $(this).find("a").attr("href"); //Найти rel атрибут для определения активной вкладки+контента
		$(activeTab).fadeIn(); //Проявление активного содержимого
		return false;
	});
    /* header tabs end */
    /* slide year list menu */
    $(".year-list").click(function() {
        if(! $(".year-list").hasClass("active") ) {
            $(this).addClass("active");
        } else {
            $(this).removeClass("active");
        }
    });
    /* slide year list menu end */
    /* open hamburger menu */
    $('.hamburger').click(function (e) {
        // e.preventDefault();
        $(".tabs.active").removeClass('active');
        if ($(this).hasClass('convert')) {
            $(this).removeClass('convert');
            $('.menu').removeClass('active_menu');
        } else {
            $(this).addClass('convert');
            $('.menu').addClass('active_menu');
        }
        if ($(".kabinet-menu ul.active").length >= 1 ) {
            $(".kabinet-menu ul").removeClass("active");
            categoryHide();
        }
        if ( $(".year-list.active").length >= 1) {
            $(".year-list").removeClass("active");
        }
    });
    /* open hamburger menu end */
    /* close menu after click */
    if ($(window).width() <= 768) {
        $('.headerWrap #menu > li > a').click(function (e) {

            $('.hamburger').removeClass('convert');
            // $('#menu').fadeOut(300);
            $('.headerWrap #menu').removeClass('active_menu');
            $(".tabs.active").removeClass('active');
        });
    }
    /* close menu after click end */
    /* replace video with pictures */
    loadVideo();
    /* replace video with pictures end */
    /* create floating arrow */
    $('.wrapper').append('<div class="floatingArrow"></div>');
    $(".floatingArrow").on( "click", function() {
        $('body, html').animate({scrollTop: 0}, 500);
        if ( $(".year-list.active").length >= 1) {
            $(".year-list").removeClass("active");
        }
        if ( $(".kabinet-menu ul.active").length >= 1) {
            $(".kabinet-menu ul.active").removeClass("active");
            categoryHide();
        }
    });
    $(window).scroll(function () { // scroll event
        // draw arrow - back to top
        drawFloatingArrow();
    });
    /* create floating arrow end */
}); // document redy end

$(window).load(function(){

    var $allContentH = 1;

     $('.enter_log').click(function(){
       $('.enter_form').show();
       $('.enter_passw').hide();
       $('.post_message').hide();
       $('#myModal').reveal({
       animation: 'fade',                   //fade, fadeAndPop, none
       animationspeed: 300,                       //how fast animtions are
       closeonbackgroundclick: true,              //if you click background will modal close?
       dismissmodalclass: 'close-reveal-modal'    //the class of a button or element that will close an open modal
      });
     });

    categoryHide();

    // make adaptive
    if (window.innerWidth <= 1023) {
        // adaptive tables on first load
        adaptiveTables()
        // create all adaptive magic on first load
        makeAdaptive();
        // recreate all adaptive magic on resize
        $(window).on('resize', function () {
            makeAdaptive();
            adaptiveTables();
            categoryHide();
        });

    } else {
        // clear inline styles
        $('.top-block .sub-menu ul').attr("style","");
    }

    function makeAdaptive() {
        // hide "category menu" on resize
        // $(".kabinet-menu ul li").removeClass("active");
        // set all content height using height of first active tab
        $allContentH = $(".tab_container").height();
        // change content height when active tab changing
        $(".kabinet-menu ul li").on('click', function(e) {
            // hide menu
            $(".kabinet-menu ul li").removeClass("active");

            var n = $(".kabinet-menu ul li").index(this);
            $allContentH = $(".tab_content:eq("+ n +")").height();
            //console.log($allContentH);
            // restart sidebar with new content height
            makeSideBar();
            //console.log("sidebar");
        });


    };

    $(".kabinet-menu ul").on('click', function(e) {
        console.log("ul cliked")
        if ( $(".kabinet-menu ul").hasClass("active") ) {
            $(".kabinet-menu ul").removeClass("active");
            categoryHide();

            console.log("hide")
        } else {
            $(".kabinet-menu ul").addClass("active");
            $(".kabinet-menu ul").css({"top": "0"});

            console.log("show")
        }
        if ( $(".year-list.active").length >= 1) {
            $(".year-list").removeClass("active");
        }
    });
    makeSideBar();

    function adaptiveTables() {
        if ($("table").length >= 1 && $('.responsive').length <= 1) {
            $('table').wrap('<div class="responsive"></div>');
            $('.responsive').wrap('<div class="responsiveWrap"></div>');
            drawSwipeHand();
        }
    }

    function drawSwipeHand() {
        $('.responsiveWrap .responsive table').each(function (index, value) {
            // console.log( $(value) );
            // show hand if width of table is lower than page width
            if ($('.swipeHand').offset() === undefined && $(value).width() > $(".responsiveWrap").width()) {
                $(value).parent().parent().addClass("swipeHand");
            } else {
                $(value).parent().parent().removeClass("swipeHand");
            }
        });
    }

    function makeSideBar() {
        var $sticky = $('.year-list'),
            $stickyH = $('.year-list').height(),
            $stickyStopper = $('.b-contact');

        // make sure content height bigger than menu height
        //console.log( $allContentH + ">=" + $stickyH * 1.5 );
        if( ( $allContentH >= $stickyH * 1.5) ) {


            // make sure ".sticky" element exists
            if ($sticky.offset()) {
                //console.log('side menu exist');
                var
                    $stickyTop = $sticky.offset().top,
                    $stickyStopperPos = $stickyStopper.offset().top,
                    $stickOffset = 10,
                    $stickyMenuBottom = window.innerHeight - $stickOffset - $stickyH,
                    $stickyStopperPosition = $stickyStopperPos - $stickOffset - $stickyH ,
                    $bottomMenuPosition = $stickyStopperPos - $stickyH - $stickyMenuBottom ;

                $(window).scroll(function () { // scroll event

                    var $windowTop = $(window).scrollTop();

                    // var windowTop = $(window).scrollTop(); // returns number
                    //console.log($stickyStopperPosition + "<" + $windowTop);

                        // bottom of the document
                    if ($bottomMenuPosition + $stickyMenuBottom < $windowTop) {
                        $sticky.css({position: 'absolute', top: $stickyStopperPosition, 'margin-left': '0px'});

                        // middle of the document
                    } else if ($stickyTop < $windowTop + $stickOffset) {
                        $sticky.css({position: 'fixed', top: $stickOffset, 'margin-left': '0'});

                        // start of the document
                    } else { // first load
                        $sticky.css({position: 'absolute', top: 80, 'margin-left': '0px'});
                    }
                });
            }
        } else {
            //console.log("there is no content");
            $sticky.css({position: 'absolute', top: 10, 'margin-left': '0px'});
            $(window).scroll(function () {
                $sticky.css({position: 'absolute', top: 10, 'margin-left': '0px'});
            });
        }
    }; // make sidebar end
});
/**
* floating arrow
* */
// draw arrow - back to top
function drawFloatingArrow() {
    /* slide page on top */

    if ( $(".floatingArrow").length ) {

        var topPos = $(".floatingArrow").offset().top;
        var windHeight = $( window ).height();

        if ( topPos >= windHeight ) {
            $(".floatingArrow").css({"opacity":"0.7", "right":"10px"});
            // console.log("show");
        } else {
            $(".floatingArrow").css({"opacity":"0", "right":"-50px"});
            // console.log("hide");
        }
        /* slide page on top end */
    }
}
/**
 * select all iframes add replace it with pictures
 **/
function loadVideo() {
    var youtube = document.querySelectorAll( ".jobs-box iframe" );
    for (var i = 0; i < youtube.length; i++) {
        var currentElement = youtube[i],
            mainWrapper = currentElement.parentElement,
            address = youtube[i].getAttribute("src"),
            videoCode = address.split('/embed/'),
            codeNumber = videoCode[1],
            source = "https://img.youtube.com/vi/"+ codeNumber +"/0.jpg",
            largeBlock = '<div class="youtubeWrapper"><div class="youtube" data-posNumber="'+ i +'" data-embed="'+ codeNumber +'"><div class="play-button"></div></div></div>',
            image = document.createElement("img");
        // create new block with image and play button
        mainWrapper.insertAdjacentHTML('afterbegin', largeBlock);
        // make cover image from first slide in a video
        image.src = source;
        image.addEventListener( "load", function() {
            $(".youtube")[i].appendChild( image );
        }( i ) );
        // remove old YOUTUBE iframes to prevent loading players
        youtube[i].remove();

        frameNavArr.push(codeNumber);

        $(".youtube")[i].addEventListener( "click", function() {
            var that = this;
            createVideo(that);
        });
    }
};
/**
 * create one youtube iframe on click
 * part of loadVideo script
 **/
function createVideo(that) {
    // console.log('click');
    var iframe = document.createElement( "iframe" ),
        curItem = parseInt(that.getAttribute('data-posnumber'));

    iframe.setAttribute( "frameborder", "0" );
    iframe.setAttribute( "allowfullscreen", "" );
    iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ frameNavArr[curItem] +"?rel=0&showinfo=0&autoplay=1" );
    that.innerHTML = "";
    that.appendChild( iframe );
};
/**
 *
 **/
function categoryHide() {
    if ($(window).width() <= 600) {
        var val = $(".kabinet-menu ul").innerHeight() - 30;
        $(".kabinet-menu ul").css({"top": "-"+ val + "px"})
    }
}