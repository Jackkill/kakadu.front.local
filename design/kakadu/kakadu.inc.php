<?php
/**
 * @package Pilot
 * @subpackage Site
 * @author Naumenko A.
 * @copyright (c) 2016, c-format
 */

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Expires: -1");
header("Last-Modified: " . gmdate("D, d M Y H:i:s")." GMT");
header('Cache-Control: no-store, no-cache, must-revalidate');
header("Cache-Control: post-check=0,pre-check=0", false);
header("Cache-Control: max-age=0", false);
header('Pragma: no-cache');

/*
if (!IS_DEVELOPER){
    if (!IP_ADMINS && HTTP_IP != '93.72.236.28'){
        exit();
    }
}*/

define('DESIGN_URL', '/design/kakadu/');

$isMainPage = $Site->isMainPage();

/* Путь к странице */
$TmplDesign->set("breacrumbs",   $Site->getTemplatePath());
$TmplDesign->set("isMain",       $isMainPage );
$TmplDesign->set("structure_id", $Site->structure_id );


//$phones = $Site->getSitePhone();
//$TmplDesign->iterate("/main_phones/", null, array_shift($phones) ); //телефонные номера
//$TmplDesign->iterateArray("/phones/", null, $phones ); //телефонные номера
//$TmplDesign->iterateArray("/socbuttons/", null, $Site->getSocButton() ); //ссылки на социальные сети
//$TmplDesign->iterateArray("/language/", null, $Site->getMenuLanguage() ); //ссылки на языковые версии


/* -----Блок из меню ------*/
$topMenu = $Site->getMenu(-1, 'top_menu', 2);
$TmplDesign->iterateArray('/topMenu/', null, $topMenu);
if (AwardsSeason::isPoll()) {
    //&& Auth::isLoggedIn()){    
    $poll_url = Site::getPageUrl(75923 ,'');
    $poll_url['name'] = 'Голосование';
    $TmplDesign->iterate('/topMenu/', null, $poll_url);
}

//разбиваем меню на две колонки
//$bottomMenu = $Site->getMenu($Site->structure_id, 'left_menu');
//x($bottomMenu);
//$TmplDesign->iterateArray('/tabmenu/', null, $bottomMenu);
//$TmplDesign->set('show_tabmenu', count($bottomMenu));

//урл предриятий по умолчанию
//$url_term_for_use = Site::getPageUrl( 75893 );
//$TmplDesign->set('url_term_for_use', $url_term_for_use);


//вход и регистрация
if ( Auth::isLoggedIn() ) {
        $cabinet_data = array(
            0 => array('url' => '/'.LANGUAGE_URL.'user/works/', 'name'=>cmsMessage::get('USER', 'MSG_USER_CABINET')),
            1 => array('url' => '/'.LANGUAGE_URL.'action/cms/logout/', 'name'=>cmsMessage::get('USER', 'MSG_USER_LOGOUT'))
        );
} else {
        $cabinet_data = array(
            0 => array('url' => '/'.LANGUAGE_URL.'user/login/', 'name'=>cmsMessage::get('USER', 'MSG_USER_ENTER_PRIVATE'))
        );
        if ( AwardsSeason::isTake() ) {
            $cabinet_data[1] = array('url' => '/'.LANGUAGE_URL.'user/register/', 'name'=>cmsMessage::get('USER', 'MSG_USER_REGISTRATION'));
        }
}   
if ( AwardsSeason::isLogining() ) {
    $TmplDesign->iterateArray('/login_menu/', null, $cabinet_data);
}


$parents = $Site->parents;
if(!$isMainPage && isset($parents[2]) ){
    if ($parents[2] == 75906 && !Auth::isLoggedIn()) { }
    else {
        $leftMenu = $Site->getMenu($parents[2], 'left_menu', 1, false); 
        if (!AwardsSeason::isTake() ){
            if (isset($leftMenu[75917])) unset($leftMenu[75917]);
            //if (isset($leftMenu[75918])) unset($leftMenu[75918]);
        }
        $TmplDesign->iterateArray('/tabmenu/', null, $leftMenu);
        $TmplDesign->set('show_tabmenu', count($leftMenu));
    }
}

/* -------- end block menu ------ */


// с левой колонкой
//define("DIV_CONTENT_START", '<div class="container"><div><div>');
//define("DIV_CONTENT_END", '</div></div></div>');


//Отслеживания откуда пришел
#Seo::setWhereFrom();
